#!/bin/sh
# called by .gitlab-ci.yml during deploy.

cd /srv/$HOSTNAME
npm install
npm run migrations
pm2 delete 0
npm run build
pm2 --name Xpeedt start npm -- start
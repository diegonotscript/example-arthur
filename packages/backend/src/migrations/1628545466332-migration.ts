import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1628545466332 implements MigrationInterface {
    name = 'migration1628545466332'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` ADD `qrCode` varchar(255) NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `qrCode`");
    }

}

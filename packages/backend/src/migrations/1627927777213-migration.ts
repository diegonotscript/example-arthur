import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1627927777213 implements MigrationInterface {
    name = 'migration1627927777213'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` ADD `receiverName` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `order` ADD `receiverPhone` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `order` ADD `receiverEmail` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `order` ADD `description` varchar(255) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `description`");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `receiverEmail`");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `receiverPhone`");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `receiverName`");
    }

}

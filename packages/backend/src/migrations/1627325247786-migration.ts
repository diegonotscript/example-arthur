import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1627325247786 implements MigrationInterface {
    name = 'migration1627325247786'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_8cbf856839ddca842f21b804a91`");
        await queryRunner.query("ALTER TABLE `order` CHANGE `driverId` `driverId` int NULL");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_8cbf856839ddca842f21b804a91` FOREIGN KEY (`driverId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_8cbf856839ddca842f21b804a91`");
        await queryRunner.query("ALTER TABLE `order` CHANGE `driverId` `driverId` int NOT NULL");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_8cbf856839ddca842f21b804a91` FOREIGN KEY (`driverId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

}

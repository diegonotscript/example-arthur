import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1627324815427 implements MigrationInterface {
    name = 'migration1627324815427'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `phoneNumber`");
        await queryRunner.query("ALTER TABLE `user` ADD `phoneNumber` varchar(255) NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `phoneNumber`");
        await queryRunner.query("ALTER TABLE `user` ADD `phoneNumber` int NOT NULL");
    }

}

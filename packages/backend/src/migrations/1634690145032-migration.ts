import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1634690145032 implements MigrationInterface {
    name = 'migration1634690145032'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_5eaeccd127221dd1d74e0e08d4e`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `contactName`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `companyWebsite`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `showWebsite`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `showLogo`");
        await queryRunner.query("ALTER TABLE `package` DROP COLUMN `price`");
        await queryRunner.query("ALTER TABLE `package` ADD `price` int NOT NULL");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `totalPrice`");
        await queryRunner.query("ALTER TABLE `order` ADD `totalPrice` int NOT NULL");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `extraPackages`");
        await queryRunner.query("ALTER TABLE `order` ADD `extraPackages` int NULL");
        await queryRunner.query("ALTER TABLE `order` CHANGE `orderPackageId` `orderPackageId` int NULL");
        await queryRunner.query("ALTER TABLE `extra_package` DROP COLUMN `price`");
        await queryRunner.query("ALTER TABLE `extra_package` ADD `price` int NOT NULL");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_124456e637cca7a415897dce659` FOREIGN KEY (`customerId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_75f552e5cfa40402ac25050c3e3` FOREIGN KEY (`orderPackageId`) REFERENCES `package`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_75f552e5cfa40402ac25050c3e3`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_124456e637cca7a415897dce659`");
        await queryRunner.query("ALTER TABLE `extra_package` DROP COLUMN `price`");
        await queryRunner.query("ALTER TABLE `extra_package` ADD `price` decimal NULL");
        await queryRunner.query("ALTER TABLE `order` CHANGE `orderPackageId` `orderPackageId` int NOT NULL");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `extraPackages`");
        await queryRunner.query("ALTER TABLE `order` ADD `extraPackages` tinyint NULL");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `totalPrice`");
        await queryRunner.query("ALTER TABLE `order` ADD `totalPrice` decimal NOT NULL");
        await queryRunner.query("ALTER TABLE `package` DROP COLUMN `price`");
        await queryRunner.query("ALTER TABLE `package` ADD `price` decimal NOT NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `showLogo` tinyint NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `showWebsite` tinyint NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `companyWebsite` varchar(100) NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `contactName` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_5eaeccd127221dd1d74e0e08d4e` FOREIGN KEY (`customerId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

}

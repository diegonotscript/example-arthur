import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1627324478853 implements MigrationInterface {
    name = 'migration1627324478853'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `package` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `price` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `order_status` (`id` int NOT NULL AUTO_INCREMENT, `description` varchar(255) NOT NULL, `collectionStatus` tinyint NOT NULL, `shipmentStatus` tinyint NOT NULL, `internalStatus` tinyint NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `order` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `costumerId` int NULL, `driverId` int NOT NULL, `collectionAddressId` int NULL, `orderStatusId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `address` (`id` int NOT NULL AUTO_INCREMENT, `postCode` varchar(255) NOT NULL, `address` varchar(255) NOT NULL, `city` varchar(255) NOT NULL, `country` varchar(255) NOT NULL, `isDefault` tinyint NOT NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` int NOT NULL AUTO_INCREMENT, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `companyName` varchar(255) NOT NULL, `photoUrl` varchar(255) NULL, `phoneNumber` int NOT NULL, `name` varchar(255) NOT NULL, `typeUser` int NOT NULL, UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `order_packages_package` (`orderId` int NOT NULL, `packageId` int NOT NULL, INDEX `IDX_b8fbda3de3e6a30caee9fe5c62` (`orderId`), INDEX `IDX_a616b93361b7dc18fe21880add` (`packageId`), PRIMARY KEY (`orderId`, `packageId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_5eaeccd127221dd1d74e0e08d4e` FOREIGN KEY (`costumerId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_8cbf856839ddca842f21b804a91` FOREIGN KEY (`driverId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_440cdcc3eb5eee4deef52f327ce` FOREIGN KEY (`collectionAddressId`) REFERENCES `address`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_9529e529c43a0e47aee39fd0a7a` FOREIGN KEY (`orderStatusId`) REFERENCES `order_status`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `address` ADD CONSTRAINT `FK_d25f1ea79e282cc8a42bd616aa3` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order_packages_package` ADD CONSTRAINT `FK_b8fbda3de3e6a30caee9fe5c623` FOREIGN KEY (`orderId`) REFERENCES `order`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order_packages_package` ADD CONSTRAINT `FK_a616b93361b7dc18fe21880addf` FOREIGN KEY (`packageId`) REFERENCES `package`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order_packages_package` DROP FOREIGN KEY `FK_a616b93361b7dc18fe21880addf`");
        await queryRunner.query("ALTER TABLE `order_packages_package` DROP FOREIGN KEY `FK_b8fbda3de3e6a30caee9fe5c623`");
        await queryRunner.query("ALTER TABLE `address` DROP FOREIGN KEY `FK_d25f1ea79e282cc8a42bd616aa3`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_9529e529c43a0e47aee39fd0a7a`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_440cdcc3eb5eee4deef52f327ce`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_8cbf856839ddca842f21b804a91`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_5eaeccd127221dd1d74e0e08d4e`");
        await queryRunner.query("DROP INDEX `IDX_a616b93361b7dc18fe21880add` ON `order_packages_package`");
        await queryRunner.query("DROP INDEX `IDX_b8fbda3de3e6a30caee9fe5c62` ON `order_packages_package`");
        await queryRunner.query("DROP TABLE `order_packages_package`");
        await queryRunner.query("DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP TABLE `address`");
        await queryRunner.query("DROP TABLE `order`");
        await queryRunner.query("DROP TABLE `order_status`");
        await queryRunner.query("DROP TABLE `package`");
    }

}

import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1635480951024 implements MigrationInterface {
    name = 'migration1635480951024'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `payment_method` DROP FOREIGN KEY `FK_PAYMENT_METHOD_USER`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_5eaeccd127221dd1d74e0e08d4e`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_ORDER_PAYMENT_METHOD`");
        await queryRunner.query("ALTER TABLE `configuration` CHANGE `extra_package_price` `extraPackagePrice` decimal NULL");
        await queryRunner.query("ALTER TABLE `package` DROP COLUMN `price`");
        await queryRunner.query("ALTER TABLE `package` ADD `price` int NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` DROP COLUMN `number`");
        await queryRunner.query("ALTER TABLE `payment_method` ADD `number` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` DROP COLUMN `expiration_date`");
        await queryRunner.query("ALTER TABLE `payment_method` ADD `expiration_date` datetime NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` DROP COLUMN `cvv`");
        await queryRunner.query("ALTER TABLE `payment_method` ADD `cvv` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` CHANGE `userId` `userId` int NULL");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `totalPrice`");
        await queryRunner.query("ALTER TABLE `order` ADD `totalPrice` int NOT NULL");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `extraPackages`");
        await queryRunner.query("ALTER TABLE `order` ADD `extraPackages` int NULL");
        await queryRunner.query("ALTER TABLE `order` CHANGE `orderPackageId` `orderPackageId` int NULL");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `companyWebsite`");
        await queryRunner.query("ALTER TABLE `user` ADD `companyWebsite` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `showWebsite` `showWebsite` tinyint NOT NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `showLogo` `showLogo` tinyint NOT NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `active` `active` tinyint NOT NULL");
        await queryRunner.query("ALTER TABLE `configuration` DROP COLUMN `extraPackagePrice`");
        await queryRunner.query("ALTER TABLE `configuration` ADD `extraPackagePrice` int NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` ADD CONSTRAINT `FK_34a4419ef2010224d7ff600659d` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_124456e637cca7a415897dce659` FOREIGN KEY (`customerId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_75f552e5cfa40402ac25050c3e3` FOREIGN KEY (`orderPackageId`) REFERENCES `package`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_89726ee65618314009b279e66e8` FOREIGN KEY (`paymentMethodId`) REFERENCES `payment_method`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_89726ee65618314009b279e66e8`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_75f552e5cfa40402ac25050c3e3`");
        await queryRunner.query("ALTER TABLE `order` DROP FOREIGN KEY `FK_124456e637cca7a415897dce659`");
        await queryRunner.query("ALTER TABLE `payment_method` DROP FOREIGN KEY `FK_34a4419ef2010224d7ff600659d`");
        await queryRunner.query("ALTER TABLE `configuration` DROP COLUMN `extraPackagePrice`");
        await queryRunner.query("ALTER TABLE `configuration` ADD `extraPackagePrice` decimal NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `active` `active` tinyint(1) NULL DEFAULT '0'");
        await queryRunner.query("ALTER TABLE `user` CHANGE `showLogo` `showLogo` tinyint(1) NULL DEFAULT '0'");
        await queryRunner.query("ALTER TABLE `user` CHANGE `showWebsite` `showWebsite` tinyint(1) NULL");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `companyWebsite`");
        await queryRunner.query("ALTER TABLE `user` ADD `companyWebsite` varchar(100) NULL");
        await queryRunner.query("ALTER TABLE `order` CHANGE `orderPackageId` `orderPackageId` int NOT NULL");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `extraPackages`");
        await queryRunner.query("ALTER TABLE `order` ADD `extraPackages` tinyint NULL");
        await queryRunner.query("ALTER TABLE `order` DROP COLUMN `totalPrice`");
        await queryRunner.query("ALTER TABLE `order` ADD `totalPrice` decimal NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` CHANGE `userId` `userId` int NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` DROP COLUMN `cvv`");
        await queryRunner.query("ALTER TABLE `payment_method` ADD `cvv` varchar(3) NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` DROP COLUMN `expiration_date`");
        await queryRunner.query("ALTER TABLE `payment_method` ADD `expiration_date` date NOT NULL");
        await queryRunner.query("ALTER TABLE `payment_method` DROP COLUMN `number`");
        await queryRunner.query("ALTER TABLE `payment_method` ADD `number` varchar(16) NOT NULL");
        await queryRunner.query("ALTER TABLE `package` DROP COLUMN `price`");
        await queryRunner.query("ALTER TABLE `package` ADD `price` decimal NOT NULL");
        await queryRunner.query("ALTER TABLE `configuration` CHANGE `extraPackagePrice` `extra_package_price` decimal NULL");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_ORDER_PAYMENT_METHOD` FOREIGN KEY (`paymentMethodId`) REFERENCES `payment_method`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `order` ADD CONSTRAINT `FK_5eaeccd127221dd1d74e0e08d4e` FOREIGN KEY (`customerId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `payment_method` ADD CONSTRAINT `FK_PAYMENT_METHOD_USER` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

}

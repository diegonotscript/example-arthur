import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { BaseBuilder } from "../builders/base-builder.interface";

@Entity()
export class Configuration extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'extra_package_price' })
  extraPackagePrice: number;
}

export namespace Configuration {
  export class Builder implements BaseBuilder<Configuration> {
    private readonly entity: Configuration;

    constructor(entity: Configuration) {
      this.entity = entity
    }

    static create() {
      return new Configuration.Builder(new Configuration());
    }

    static from(configuration: Configuration) {
      return new Configuration.Builder(configuration);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    extraPackagePrice(extraPackagePrice: number) {
      this.entity.extraPackagePrice = extraPackagePrice;
      return this;
    }

    build() {
      return this.entity;
    }
  }
}
import {
  BaseEntity, Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { Address } from './address.entity';
import { User } from './user.entity';
import { Package } from './package.entity';
import { OrderStatus } from './order-status.entity';
import { BaseBuilder } from '../builders/base-builder.interface';
import { PaymentMethod } from './payment-method.entity';

@Entity()
export class Order extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => User, orders => User)
  customer: User;

  @ManyToOne(type => User, orders => User, { nullable: true })
  driver: User | null;

  @ManyToOne(type => Address, orders => Address)
  collectionAddress: Address;

  @ManyToOne(type => Package, orders => Package)
  orderPackage: Package;

  @ManyToOne(type => OrderStatus, ordersStatus => OrderStatus, { nullable: true })
  orderStatus: OrderStatus | null;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column()
  shippingName: string;

  @Column()
  shippingPhone: string;

  @Column()
  shippingEmail: string;

  @Column()
  shippingAddress: string;

  @Column({ nullable: true })
  qrCode: string;

  @Column({ nullable: true })
  productDescription: string;

  @Column()
  totalPrice: number;

  @Column({ nullable: true })
  extraPackages: number;

  @Column({ nullable: true })
  productImageUrl: string;

  @Column({ nullable: true })
  salesNumber: string;

  @ManyToOne(type => PaymentMethod, orders => PaymentMethod, { nullable: true })
  paymentMethod: PaymentMethod | null;
}

export namespace Order {
  export class Builder implements BaseBuilder<Order> {
    private readonly entity: Order;

    constructor(entity: Order) {
      this.entity = entity
    }

    static create() {
      return new Order.Builder(new Order());
    }

    static from(order: Order) {
      return new Order.Builder(order);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    customer(customer: User) {
      this.entity.customer = customer;
      return this;
    }

    driver(driver: User | null) {
      this.entity.driver = driver;
      return this;
    }

    collectionAddress(address: Address) {
      this.entity.collectionAddress = address;
      return this;
    }

    orderPackage(orderPackage: Package) {
      this.entity.orderPackage = orderPackage;
      return this;
    }

    orderStatus(orderStatus: OrderStatus | null) {
      this.entity.orderStatus = orderStatus;
      return this;
    }

    shippingName(shippingName: string) {
      this.entity.shippingName = shippingName;
      return this;
    }

    shippingPhone(shippingPhone: string) {
      this.entity.shippingPhone = shippingPhone;
      return this;
    }

    shippingEmail(shippingEmail: string) {
      this.entity.shippingEmail = shippingEmail;
      return this;
    }

    shippingAddress(shippingAddress: string) {
      this.entity.shippingAddress = shippingAddress;
      return this;
    }

    productDescription(productDescription: string) {
      this.entity.productDescription = productDescription;
      return this;
    }

    qrCode(qrCode: string) {
      this.entity.qrCode = qrCode;
      return this;
    }

    totalPrice(totalPrice: number) {
      this.entity.totalPrice = totalPrice;
      return this;
    }

    extraPackages(extraPackages: number) {
      this.entity.extraPackages = extraPackages;
      return this;
    }

    productImageUrl(productImageUrl: string) {
      this.entity.productImageUrl = productImageUrl;
      return this;
    }

    salesNumber(salesNumber: string) {
      this.entity.salesNumber = salesNumber;
      return this;
    }

    paymentMethod(paymentMethod: PaymentMethod | null) {
      this.entity.paymentMethod = paymentMethod;
      return this;
    }

    build() {
      this.validate();
      return this.entity;
    }

    validate() {
      if (this.entity.collectionAddress.user.id !== this.entity.customer.id) {
        throw new Error(`Address entity ${this.entity.collectionAddress.id} not found`);
      }
    }
  }
}

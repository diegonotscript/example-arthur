import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '.';
import { BaseBuilder } from '../builders/base-builder.interface';

@Entity()
export class PaymentMethod extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  number: string;

  @Column({ name: "expiration_date" })
  expirationDate: Date;

  @Column()
  cvv: string;

  @ManyToOne(type => User, orders => User)
  user: User;

}

export namespace PaymentMethod {
  export class Builder implements BaseBuilder<PaymentMethod> {
    private readonly entity: PaymentMethod;

    constructor(entity: PaymentMethod) {
      this.entity = entity
    }

    static create() {
      return new PaymentMethod.Builder(new PaymentMethod());
    }

    static from(order: PaymentMethod) {
      return new PaymentMethod.Builder(order);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    name(name: string) {
      this.entity.name = name;
      return this;
    }

    number(number: string) {
      this.entity.number = number;
      return this;
    }

    expirationDate(expirationDate: Date) {
      this.entity.expirationDate = expirationDate;
      return this;
    }

    cvv(cvv: string) {
      this.entity.cvv = cvv;
      return this;
    }

    user(user: User) {
      this.entity.user = user;
      return this;
    }

    build() {
      return this.entity;
    }
  }
}

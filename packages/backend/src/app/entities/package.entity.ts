import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {BaseBuilder} from "../builders/base-builder.interface";

@Entity()
export class Package extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;
}

export namespace Package {
  export class Builder implements BaseBuilder<Package> {
    private readonly entity: Package;

    constructor(entity: Package) {
      this.entity = entity
    }

    static create() {
      return new Package.Builder(new Package());
    }

    static from(pack: Package) {
      return new Package.Builder(pack);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    name(name: string) {
      this.entity.name = name;
      return this;
    }

    price(price: number) {
      this.entity.price = price;
      return this;
    }

    build() {
      return this.entity;
    }
  }
}
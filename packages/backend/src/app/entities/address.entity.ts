import {BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {User} from './user.entity';
import {BaseBuilder} from '../builders/base-builder.interface';
import {Order} from "./order.entity";

@Entity()
export class Address extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    postCode: string;

    @Column()
    address: string;

    @Column()
    city: string;

    @Column()
    country: string;

    @Column()
    isDefault: boolean;

    @ManyToOne(type => User, user => user.addresses)
    user: User;

    @OneToMany(type => Order, collectionAddress => Address)
    orders: Order[];
}

export namespace Address {
    export class Builder implements BaseBuilder<Address> {
        private readonly entity: Address;

        constructor(entity: Address) {
            this.entity = entity
        }

        static create() {
            return new Address.Builder(new Address());
        }

        static from(address: Address) {
            return new Address.Builder(address);
        }

        id(id: number) {
            this.entity.id = id;
            return this;
        }

        postCode(postCode: string) {
            this.entity.postCode = postCode;
            return this;
        }

        address(address: string) {
            this.entity.address = address;
            return this;
        }

        city(city: string) {
            this.entity.city = city;
            return this;
        }

        country(country: string) {
            this.entity.country = country;
            return this;
        }

        isDefault(isDefault: boolean) {
            this.entity.isDefault = isDefault;
            return this;
        }

        user(user: User) {
            this.entity.user = user;
            return this;
        }

        build() {
            return this.entity;
        }
    }
}


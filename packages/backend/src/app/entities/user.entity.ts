import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Address } from './address.entity';
import { TypeUser } from '../enums/type-user.enum';
import { BaseBuilder } from '../builders/base-builder.interface';
import { Order } from "./order.entity";

@Entity()
export class User extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  companyName: string;

  @Column({
    nullable: true,
  })
  photoUrl: string;

  @Column()
  phoneNumber: string;

  @Column()
  name: string;

  @OneToMany(() => Address, addresses => addresses.user, {
    cascade: true,
  })
  addresses: Address[];

  @OneToMany(type => Order, customer => User)
  orders: Order[];

  @Column()
  typeUser: TypeUser;

  @Column()
  companyWebsite: string;

  @Column()
  showWebsite: boolean = false;

  @Column()
  showLogo: boolean = false;

  @Column()
  active: boolean = false;

}

export namespace User {
  export class Builder implements BaseBuilder<User> {
    private readonly entity: User;

    constructor(entity: User) {
      this.entity = entity
    }

    static create() {
      return new User.Builder(new User());
    }

    static from(user: User) {
      return new User.Builder(user);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    email(email: string) {
      this.entity.email = email;
      return this;
    }

    companyName(companyName: string) {
      this.entity.companyName = companyName;
      return this;
    }

    phoneNumber(phoneNumber: string) {
      this.entity.phoneNumber = phoneNumber;
      return this;
    }

    name(name: string) {
      this.entity.name = name;
      return this;
    }

    password(password: string) {
      this.entity.password = password;
      return this;
    }

    typeUser(typeUser: TypeUser) {
      this.entity.typeUser = typeUser;
      return this;
    }

    photoUrl(photoUrl: string) {
      this.entity.photoUrl = photoUrl;
      return this;
    }

    companyWebsite(companyWebsite: string) {
      this.entity.companyWebsite = companyWebsite;
      return this;
    }

    showWebsite(showWebsite: boolean) {
      this.entity.showWebsite = showWebsite;
      return this;
    }

    showLogo(showLogo: boolean) {
      this.entity.showLogo = showLogo;
      return this;
    }

    active(active: boolean) {
      this.entity.active = active;
      return this;
    }

    build() {
      return this.entity;
    }
  }
}
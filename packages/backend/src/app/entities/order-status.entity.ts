import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {BaseBuilder} from '../builders/base-builder.interface';
import {Order} from "./order.entity";

@Entity()
export class OrderStatus extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @Column()
  collectionStatus: boolean;

  @Column()
  shipmentStatus: boolean;

  @Column()
  internalStatus: boolean;

  @OneToMany(type => Order, orderStatus => OrderStatus)
  orders: Order[];
}

export namespace OrderStatus {
  export class Builder implements BaseBuilder<OrderStatus> {
    private readonly entity: OrderStatus;

    constructor(entity: OrderStatus) {
      this.entity = entity
    }

    static create() {
      return new OrderStatus.Builder(new OrderStatus());
    }

    static from(order: OrderStatus) {
      return new OrderStatus.Builder(order);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    description(description: string) {
      this.entity.description = description;
      return this;
    }

    collectionStatus(collectionStatus: boolean) {
      this.entity.collectionStatus = collectionStatus;
      return this;
    }

    shipmentStatus(shipmentStatus: boolean) {
      this.entity.shipmentStatus = shipmentStatus;
      return this;
    }

    internalStatus(internalStatus: boolean) {
      this.entity.internalStatus = internalStatus;
      return this;
    }

    build() {
      return this.entity;
    }
  }
}

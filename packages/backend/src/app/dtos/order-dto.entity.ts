import { BaseBuilder } from '../builders/base-builder.interface';
import { UserDto } from './user-dto.entity';
import { AddressDto } from './address-dto.entity';
import { OrderStatusDto } from './order-status-dto.entity';
import { Order } from '../entities/order.entity';
import { BaseRepresentationBuilderAsync } from '../builders/base-representation-builder-async.interface';
import { UserService } from '../services/user-service';
import { dependency } from '@foal/core';
import { PackageService } from "../services/package-service";
import { Package } from "../entities/package.entity";
import { AddressRepository } from "../repositories/address-repository";
import { OrderStatusRepository } from "../repositories/order-status-repository";
import { Address, User } from "../entities";
import { PackageDto } from "./package-dto.entity";
import { TypeUser } from "../enums/type-user.enum";
import { v4 } from "uuid";
import { ConfigurationService } from '../services/configuration-service';
import { PaymentMethodDto } from './payment-method-dto.entity';
import { PaymentMethodRepository } from '../repositories/payment-method-repository';
import { File } from '../utils/file';
import { AddressService } from '../services/address-service';

const QRCode = require('qrcode')

export class OrderDto {
  id: number;
  customer: UserDto;
  driver: UserDto | null;
  collectionAddress: AddressDto;
  orderPackage: PackageDto;
  orderStatus: OrderStatusDto | null;
  shippingName: string;
  shippingPhone: string;
  shippingEmail: string;
  shippingAddress: string;
  productDescription: string;
  qrCode: string;
  createdAt: Date;
  totalPrice: number;
  extraPackages: number;
  productImageUrl: string;
  salesNumber: string;
  paymentMethod: PaymentMethodDto | null;
}

export namespace OrderDto {
  export class Builder implements BaseBuilder<OrderDto> {
    private readonly entity: OrderDto;

    constructor(entity: OrderDto) {
      this.entity = entity
    }

    static create() {
      return new OrderDto.Builder(new OrderDto());
    }

    static from(orderDto: OrderDto) {
      return new OrderDto.Builder(orderDto);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    customer(customer: UserDto) {
      this.entity.customer = customer;
      return this;
    }

    driver(driver: UserDto | null) {
      this.entity.driver = driver;
      return this;
    }

    collectionAddress(collectionAddress: AddressDto) {
      this.entity.collectionAddress = collectionAddress;
      return this;
    }

    orderPackage(orderPackage: PackageDto) {
      this.entity.orderPackage = orderPackage;
      return this;
    }

    orderStatus(orderStatus: OrderStatusDto | null) {
      this.entity.orderStatus = orderStatus;
      return this;
    }

    shippingName(shippingName: string) {
      this.entity.shippingName = shippingName;
      return this;
    }

    shippingPhone(shippingPhone: string) {
      this.entity.shippingPhone = shippingPhone;
      return this;
    }

    shippingEmail(shippingEmail: string) {
      this.entity.shippingEmail = shippingEmail;
      return this;
    }

    shippingAddress(shippingAddress: string) {
      this.entity.shippingAddress = shippingAddress;
      return this;
    }

    productDescription(productDescription: string) {
      this.entity.productDescription = productDescription;
      return this;
    }

    qrCode(qrCode: string) {
      this.entity.qrCode = qrCode;
      return this;
    }

    createdAt(createdAt: Date) {
      this.entity.createdAt = createdAt;
      return this;
    }

    totalPrice(totalPrice: number) {
      this.entity.totalPrice = totalPrice;
      return this;
    }

    extraPackages(extraPackages: number) {
      this.entity.extraPackages = extraPackages;
      return this;
    }

    productImageUrl(productImageUrl: string) {
      this.entity.productImageUrl = productImageUrl;
      return this;
    }

    salesNumber(salesNumber: string) {
      this.entity.salesNumber = salesNumber;
      return this;
    }

    paymentMethod(paymentMethod: PaymentMethodDto | null) {
      this.entity.paymentMethod = paymentMethod;
      return this;
    }

    build() {
      return this.entity;
    }
  }

  export class RepresentationBuilder implements BaseRepresentationBuilderAsync<Order, OrderDto, Order.Builder> {

    @dependency
    private readonly _userService: UserService;

    @dependency
    private readonly _packageService: PackageService;

    @dependency
    private readonly _configurationService: ConfigurationService;

    @dependency
    private readonly _addressService: AddressService;

    @dependency
    private readonly _orderStatusRepository: OrderStatusRepository;

    @dependency
    private readonly _paymentMethodRepository: PaymentMethodRepository;

    @dependency
    private readonly _userRB: UserDto.RepresentationBuilder;

    @dependency
    private readonly _packageRB: PackageDto.RepresentationBuilder;

    @dependency
    private readonly _addressRB: AddressDto.RepresentationBuilder;

    @dependency
    private readonly _orderStatusRB: OrderStatusDto.RepresentationBuilder;

    @dependency
    private readonly _paymentMethodRB: PaymentMethodDto.RepresentationBuilder;

    async toRepresentation(entity: Order): Promise<OrderDto> {
      const qrCode = entity.qrCode ? await QRCode.toDataURL(entity.qrCode) : null;

      const builder = OrderDto.Builder.create()
        .id(entity.id)
        .createdAt(entity.createdAt)
        .shippingName(entity.shippingName)
        .shippingPhone(entity.shippingPhone)
        .shippingEmail(entity.shippingEmail)
        .shippingAddress(entity.shippingAddress)
        .customer(await this._userRB.toMinimalRepresentation(entity.customer))
        .collectionAddress(this._addressRB.toRepresentation(entity.collectionAddress))
        .orderPackage(this._packageRB.toRepresentation(entity.orderPackage))
        .totalPrice(await this.getTotalPrice(entity.orderPackage, entity.extraPackages))
        .extraPackages(entity.extraPackages)
        .productImageUrl(await new File().read('orders', entity.productImageUrl))
        .salesNumber(entity.salesNumber)
        .qrCode(qrCode)

      if (entity.driver != null) {
        builder.driver(await this._userRB.toMinimalRepresentation(entity.driver))
      }

      if (entity.orderStatus != null) {
        builder.orderStatus(this._orderStatusRB.toRepresentation(entity.orderStatus))
      }

      if (entity.paymentMethod != null) {
        builder.paymentMethod(await this._paymentMethodRB.toRepresentation(entity.paymentMethod))
      }

      return builder.build();
    }

    async fromRepresentation(representation: OrderDto, builder: Order.Builder): Promise<Order> {

      const customer: User = await this._userService.getByIdThrowable(representation.customer.id);
      const address: Address = await this._addressService.getByIdThrowable(representation.collectionAddress.id);

      if (address.user.id !== customer.id) {
        throw new Error(`Address entity ${address.id} not found`);
      }

      const orderPackage = await this._packageService.getById(representation.orderPackage.id);

      const orderBuilder = builder
        .customer(customer)
        .orderPackage(orderPackage)
        .collectionAddress(address)
        .shippingPhone(representation.shippingPhone)
        .shippingName(representation.shippingName)
        .shippingEmail(representation.shippingEmail)
        .shippingAddress(representation.shippingAddress)
        .productDescription(representation.productDescription)
        .totalPrice(await this.getTotalPrice(orderPackage, representation.extraPackages))
        .extraPackages(representation.extraPackages)
        .productImageUrl(representation.productImageUrl)
        .salesNumber(representation.salesNumber)
        .orderStatus(await this._orderStatusRepository.getById(representation.orderStatus?.id) || null)
        .driver(await this._userService.getById(representation.driver?.id) || null)
        .paymentMethod(await this._paymentMethodRepository.getById(representation.paymentMethod?.id) || null);

      return orderBuilder.build();
    }

    async fromRepresentationWithUser(representation: OrderDto, builder: Order.Builder, user: User): Promise<Order> {

      let customer: User;
      if (user.typeUser === TypeUser.Admin) {
        customer = await this._userService.getByIdThrowable(representation.customer.id);
      } else {
        customer = await this._userService.getByIdThrowable(user.id);
      }

      const orderPackage = await this._packageService.getById(representation.orderPackage.id);

      const orderBuilder = builder
        .customer(customer)
        .orderPackage(orderPackage)
        .collectionAddress(await this._addressService.getByIdThrowable(representation.collectionAddress.id))
        .shippingPhone(representation.shippingPhone)
        .shippingName(representation.shippingName)
        .shippingEmail(representation.shippingEmail)
        .shippingAddress(representation.shippingAddress)
        .qrCode(v4())
        .productDescription(representation.productDescription)
        .totalPrice(await this.getTotalPrice(orderPackage, representation.extraPackages))
        .extraPackages(representation.extraPackages)
        .productImageUrl(representation.productImageUrl)
        .salesNumber(representation.salesNumber);

      return orderBuilder.build();
    }

    async getTotalPrice(orderPackage: Package, extraPackages: number): Promise<number> {

      if (!extraPackages) {
        return orderPackage.price;
      }

      const configuration = await this._configurationService.getFirst();

      return Number(orderPackage.price) + (Number(configuration.extraPackagePrice) * Number(extraPackages));
    }

  }
}
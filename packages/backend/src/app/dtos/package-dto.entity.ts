import { BaseBuilder } from '../builders/base-builder.interface';
import { BaseRepresentationBuilder } from '../builders/base-representation-builder.interface';
import { Package } from "../entities/package.entity";

export class PackageDto {
  id: number;
  name: string;
  price: number;
}

export namespace PackageDto {
  export class Builder implements BaseBuilder<PackageDto> {
    private readonly entity: PackageDto;

    constructor(entity: PackageDto) {
      this.entity = entity
    }

    static create() {
      return new PackageDto.Builder(new PackageDto());
    }

    static from(orderStatus: PackageDto) {
      return new PackageDto.Builder(orderStatus);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    name(name: string) {
      this.entity.name = name;
      return this;
    }

    price(price: number) {
      this.entity.price = price;
      return this;
    }

    build() {
      return this.entity;
    }
  }

  export class RepresentationBuilder implements BaseRepresentationBuilder<Package, PackageDto, Package.Builder>{

    toRepresentation(entity: Package): PackageDto {
      return PackageDto.Builder.create()
        .id(entity.id)
        .name(entity.name)
        .price(entity.price)
        .build()
    }

    fromRepresentation(representation: PackageDto, builder: Package.Builder): Package {
      return builder
        .name(representation.name)
        .price(representation.price)
        .build()
    }
  }
}
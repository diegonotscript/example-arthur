import {Address, User} from '../entities';
import {BaseBuilder} from '../builders/base-builder.interface';
import {BaseRepresentationBuilder} from '../builders/base-representation-builder.interface';
import {UserDto} from './user-dto.entity';
import {dependency} from '@foal/core';

export class AddressDto {
    id: number;

    postCode: string;

    address: string;

    city: string;

    country: string;

    isDefault: boolean;

    user: UserDto;

    fullAddress: string;
}

export namespace AddressDto {
    export class Builder implements BaseBuilder<AddressDto> {
        private readonly entity: AddressDto;

        constructor(entity: AddressDto) {
            this.entity = entity
        }

        static create() {
            return new AddressDto.Builder(new AddressDto());
        }

        static from(addressDto: AddressDto) {
            return new AddressDto.Builder(addressDto);
        }

        id(id: number) {
            this.entity.id = id;
            return this;
        }

        postCode(postCode: string) {
            this.entity.postCode = postCode;
            return this;
        }

        address(address: string) {
            this.entity.address = address;
            return this;
        }

        city(city: string) {
            this.entity.city = city;
            return this;
        }

        country(country: string) {
            this.entity.country = country;
            return this;
        }

        isDefault(isDefault: boolean) {
            this.entity.isDefault = isDefault;
            return this;
        }

        user(user: UserDto) {
            this.entity.user = user;
            return this;
        }

        fullAddress(fullAddress: string) {
            this.entity.fullAddress = fullAddress;
            return this;
        }

        build() {
            return this.entity;
        }
    }

    export class RepresentationBuilder implements BaseRepresentationBuilder<Address, AddressDto, Address.Builder>{

        @dependency
        private readonly _userRepresentationBuilder: UserDto.RepresentationBuilder;

        toRepresentation(entity: Address): AddressDto {
            return AddressDto.Builder.create()
                .id(entity.id)
                .address(entity.address)
                .postCode(entity.postCode)
                .city(entity.city)
                .country(entity.country)
                .isDefault(entity.isDefault)
                .fullAddress(`${entity.address}, ${entity.city}, ${entity.country} - ${entity.postCode}`)
                .build()
        }

        fromRepresentation(representation: AddressDto, builder: Address.Builder): Address {
            return builder
                .address(representation.address)
                .postCode(representation.postCode)
                .city(representation.city)
                .country(representation.country)
                .isDefault(representation.isDefault)
                .build()
        }

        fromRepresentationWithUser(representation: AddressDto, builder: Address.Builder, user: User) {
            const address = this.fromRepresentation(representation, builder);
            address.user = user;

            return address;
        }
    }
}
import { dependency } from '@foal/core';
import { UserDto } from '.';
import { BaseBuilder } from '../builders/base-builder.interface';
import { BaseRepresentationBuilderAsync } from '../builders/base-representation-builder-async.interface';
import { PaymentMethod } from '../entities/payment-method.entity';
import { UserService } from '../services/user-service';

export class PaymentMethodDto {
  id: number;
  name: string;
  number: string;
  expirationDate: Date;
  cvv: string;
  user: UserDto;
}

export namespace PaymentMethodDto {
  export class Builder implements BaseBuilder<PaymentMethodDto> {
    private readonly entity: PaymentMethodDto;

    constructor(entity: PaymentMethodDto) {
      this.entity = entity
    }

    static create() {
      return new PaymentMethodDto.Builder(new PaymentMethodDto());
    }

    static from(orderStatus: PaymentMethodDto) {
      return new PaymentMethodDto.Builder(orderStatus);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    name(name: string) {
      this.entity.name = name;
      return this;
    }

    number(number: string) {
      this.entity.number = number;
      return this;
    }

    expirationDate(expirationDate: Date) {
      this.entity.expirationDate = expirationDate;
      return this;
    }

    cvv(cvv: string) {
      this.entity.cvv = cvv;
      return this;
    }

    user(user: UserDto) {
      this.entity.user = user;
      return this;
    }

    build() {
      return this.entity;
    }
  }

  export class RepresentationBuilder implements BaseRepresentationBuilderAsync<PaymentMethod, PaymentMethodDto, PaymentMethod.Builder>{

    @dependency
    private readonly _userService: UserService;

    @dependency
    private readonly _userRB: UserDto.RepresentationBuilder;

    async toRepresentation(entity: PaymentMethod): Promise<PaymentMethodDto> {
      return PaymentMethodDto.Builder.create()
        .id(entity.id)
        .name(entity.name)
        .number(entity.number)
        .expirationDate(entity.expirationDate)
        .cvv(entity.cvv)
        .user(await this._userRB.toRepresentation(entity.user))
        .build()
    }

    async fromRepresentation(representation: PaymentMethodDto, builder: PaymentMethod.Builder): Promise<PaymentMethod> {
      return builder
        .id(representation.id)
        .name(representation.name)
        .number(representation.number)
        .expirationDate(representation.expirationDate)
        .cvv(representation.cvv)
        .user(await this._userService.getByIdThrowable(representation.user.id))
        .build()
    }
  }
}
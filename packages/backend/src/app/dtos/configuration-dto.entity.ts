import { BaseBuilder } from '../builders/base-builder.interface';
import { BaseRepresentationBuilder } from '../builders/base-representation-builder.interface';
import { Configuration } from "../entities/configuration.entity";

export class ConfigurationDto {
  id: number;
  extraPackagePrice: number;
}

export namespace ConfigurationDto {
  export class Builder implements BaseBuilder<ConfigurationDto> {
    private readonly entity: ConfigurationDto;

    constructor(entity: ConfigurationDto) {
      this.entity = entity
    }

    static create() {
      return new ConfigurationDto.Builder(new ConfigurationDto());
    }

    static from(configurationDto: ConfigurationDto) {
      return new ConfigurationDto.Builder(configurationDto);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    extraPackagePrice(extraPackagePrice: number) {
      this.entity.extraPackagePrice = extraPackagePrice;
      return this;
    }

    build() {
      return this.entity;
    }
  }

  export class RepresentationBuilder implements BaseRepresentationBuilder<Configuration, ConfigurationDto, Configuration.Builder>{

    toRepresentation(entity: Configuration): ConfigurationDto {
      return ConfigurationDto.Builder.create()
        .id(entity.id)
        .extraPackagePrice(entity.extraPackagePrice)
        .build()
    }

    fromRepresentation(representation: ConfigurationDto, builder: Configuration.Builder): Configuration {
      return builder
        .extraPackagePrice(representation.extraPackagePrice)
        .build()
    }
  }
}
import { BaseBuilder } from '../builders/base-builder.interface';
import { BaseRepresentationBuilder } from '../builders/base-representation-builder.interface';
import { OrderStatus } from '../entities/order-status.entity';

export class OrderStatusDto {
  id: number;
  description: string;
  collectionStatus: boolean;
  shipmentStatus: boolean;
  internalStatus: boolean;
}

export namespace OrderStatusDto {
  export class Builder implements BaseBuilder<OrderStatusDto> {
    private readonly entity: OrderStatusDto;

    constructor(entity: OrderStatusDto) {
      this.entity = entity
    }

    static create() {
      return new OrderStatusDto.Builder(new OrderStatusDto());
    }

    static from(orderStatus: OrderStatusDto) {
      return new OrderStatusDto.Builder(orderStatus);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    description(description: string) {
      this.entity.description = description;
      return this;
    }

    collectionStatus(collectionStatus: boolean) {
      this.entity.collectionStatus = collectionStatus;
      return this;
    }

    shipmentStatus(shipmentStatus: boolean) {
      this.entity.shipmentStatus = shipmentStatus;
      return this;
    }

    internalStatus(internalStatus: boolean) {
      this.entity.internalStatus = internalStatus;
      return this;
    }

    build() {
      return this.entity;
    }
  }

  export class RepresentationBuilder implements BaseRepresentationBuilder<OrderStatus, OrderStatusDto, OrderStatus.Builder>{

    toRepresentation(entity: OrderStatus): OrderStatusDto {
      return OrderStatusDto.Builder.create()
        .id(entity.id)
        .description(entity.description)
        .collectionStatus(entity.collectionStatus)
        .shipmentStatus(entity.shipmentStatus)
        .internalStatus(entity.internalStatus)
        .build()
    }

    fromRepresentation(representation: OrderStatusDto, builder: OrderStatus.Builder): OrderStatus {
      return builder
        .description(representation.description)
        .collectionStatus(representation.collectionStatus)
        .shipmentStatus(representation.shipmentStatus)
        .internalStatus(representation.internalStatus)
        .build()
    }
  }
}
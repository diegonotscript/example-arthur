
export class PageDto {
  constructor(content: any, total: number, offset: number) {
    this.content = content;
    this.total = total;
    this.page = Math.ceil((offset - 1) / total + 1) || 1;
  }

  content: any;
  total: number;
  page: number;
}

import { Address, User } from '../entities';
import { TypeUser } from '../enums/type-user.enum';
import { BaseBuilder } from '../builders/base-builder.interface';
import { File } from '../utils/file';
import { BaseRepresentationBuilderAsync } from '../builders/base-representation-builder-async.interface';

export class UserDto {
  id: number;
  email: string;
  companyName: string;
  phoneNumber: string;
  name: string;
  isAdmin: boolean;
  addresses: Address[];
  photoUrl: string;
  companyWebsite: string;
  showWebsite: boolean = false;
  showLogo: boolean = false;
  active: boolean = false;
}

export namespace UserDto {
  export class Builder implements BaseBuilder<UserDto> {
    private readonly entity: UserDto;

    constructor(entity: UserDto) {
      this.entity = entity
    }

    static create() {
      return new UserDto.Builder(new UserDto());
    }

    static from(userDto: UserDto) {
      return new UserDto.Builder(userDto);
    }

    id(id: number) {
      this.entity.id = id;
      return this;
    }

    email(email: string) {
      this.entity.email = email;
      return this;
    }

    photoUrl(photoUrl: string) {
      this.entity.photoUrl = photoUrl;
      return this;
    }

    companyName(companyName: string) {
      this.entity.companyName = companyName;
      return this;
    }

    phoneNumber(phoneNumber: string) {
      this.entity.phoneNumber = phoneNumber;
      return this;
    }

    name(name: string) {
      this.entity.name = name;
      return this;
    }

    isAdmin(typeUser: TypeUser) {
      this.entity.isAdmin = TypeUser.Admin === typeUser;
      return this;
    }

    companyWebsite(companyWebsite: string) {
      this.entity.companyWebsite = companyWebsite;
      return this;
    }

    showWebsite(showWebsite: boolean) {
      this.entity.showWebsite = showWebsite;
      return this;
    }

    showLogo(showLogo: boolean) {
      this.entity.showLogo = showLogo;
      return this;
    }

    active(active: boolean) {
      this.entity.active = active;
      return this;
    }

    build() {
      return this.entity;
    }
  }

  export class RepresentationBuilder implements BaseRepresentationBuilderAsync<User, UserDto, User.Builder>{

    async toRepresentation(entity: User): Promise<UserDto> {
      return UserDto.Builder.create()
        .id(entity.id)
        .email(entity.email)
        .name(entity.name)
        .companyName(entity.companyName)
        .phoneNumber(entity.phoneNumber)
        .isAdmin(entity.typeUser || TypeUser.User)
        .photoUrl(await new File().read('users', entity.photoUrl))
        .companyWebsite(entity.companyWebsite)
        .showWebsite(entity.showWebsite)
        .showLogo(entity.showLogo)
        .active(entity.active)
        .build()
    }

    async fromRepresentation(representation: UserDto, builder: User.Builder): Promise<User> {
      return builder
        .email(representation.email)
        .name(representation.name)
        .companyName(representation.companyName)
        .phoneNumber(representation.phoneNumber)
        .photoUrl(representation.photoUrl)
        .companyWebsite(representation.companyWebsite)
        .showWebsite(representation.showWebsite)
        .showLogo(representation.showLogo)
        .active(representation.active)
        .build()
    }

    async toMinimalRepresentation(entity: User): Promise<UserDto> {
      return UserDto.Builder.create()
        .id(entity.id)
        .email(entity.email)
        .name(entity.name)
        .isAdmin(entity.typeUser || TypeUser.User)
        .active(entity.active)
        .photoUrl(await new File().read('users', entity.photoUrl))
        .build()
    }

  }
}
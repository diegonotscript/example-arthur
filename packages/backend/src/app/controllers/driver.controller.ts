import {
  Context,
  dependency,
  Hook,
  HttpResponseBadRequest, HttpResponseNoContent,
  HttpResponseOK,
  HttpResponseUnauthorized,
  isHttpResponseInternalServerError, Options,
  Post,
  ValidateBody
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { TypeUser } from '../enums/type-user.enum';
import { UserDto } from '../dtos';
import { credentialsSchemaCreate } from '../validators/auth.validator';
import { UserService } from '../services/user-service';
import { JsonFormatter } from '../utils/json';
import { RefreshJWT } from "../hooks/refresh-token";

@JWTRequired()
@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  if (isHttpResponseInternalServerError(response)) {
    response.body = { error: response?.error?.message };
  }
})
export class DriverController {

  @dependency
  private readonly _userService: UserService;

  @dependency
  private readonly _userRepresentationBuilder: UserDto.RepresentationBuilder;

  @Post()
  @ValidateBody(credentialsSchemaCreate)
  async addDriver(ctx: Context) {
    try {
      if (ctx.user.typeUser !== TypeUser.Admin) {
        return new HttpResponseUnauthorized();
      }

      const user = await this._userService.save(ctx.request.body, TypeUser.Driver);
      return new HttpResponseOK(this._userRepresentationBuilder.toRepresentation(user));
    } catch (e) {
      return new HttpResponseBadRequest(JsonFormatter.toJson(e.message));
    }
  }

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }
}

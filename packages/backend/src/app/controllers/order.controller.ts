import {
  Context,
  controller,
  Delete,
  dependency,
  Get,
  Hook,
  HttpResponseCreated,
  HttpResponseNoContent,
  HttpResponseOK,
  IAppController,
  isHttpResponseInternalServerError,
  Options,
  Post,
  Put
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { TypeUser } from '../enums/type-user.enum';
import { TypeStatusFilter } from '../enums/type-status-filter.enum';
import { Order } from '../entities/order.entity';
import { OrderDto } from '../dtos/order-dto.entity';
import { OrderService } from '../services/order-service';
import { OrderDriverController } from "./order-driver.controller";
import { RefreshJWT } from "../hooks/refresh-token";
import { FindOperator, IsNull, Not } from 'typeorm';
import { PageDto } from '../dtos/page-dto.entity';
import { Sort } from '../utils/Sort';


@JWTRequired()
@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  if (isHttpResponseInternalServerError(response)) {
    response.body = { error: response?.error?.message };
  }
})
export class OrderController implements IAppController {
  subControllers = [
    controller('/:id/drivers', OrderDriverController),
  ];

  @dependency
  private readonly _orderRepresentationBuilder: OrderDto.RepresentationBuilder;

  @dependency
  private readonly _orderService: OrderService;

  @Get()
  async getOrders(ctx: Context) {
    const skip = ctx.request.query.skip || 0;
    const take = ctx.request.query.take || 10;
    const sort = new Sort(ctx.request.query.sort, 'order.id', 'desc');
    let where = ctx.request.query.where;

    if (ctx.user.typeUser !== TypeUser.Admin) {
      where = (where ? `${where} and ` : '') + `customer.id = ${ctx.user.id}`;
    }

    const orders = await this._orderService.getOrders(where, skip, take, sort);

    const ordersRB: OrderDto[] = new Array<OrderDto>();
    for (const order of orders) {
      const orderRB = await this._orderRepresentationBuilder.toRepresentation(order);
      ordersRB.push(orderRB);
    }

    return new HttpResponseOK(new PageDto(ordersRB, await this._orderService.getTotal(where), skip));
  }

  @Get('/total-by-status')
  async getTotalByStatus(ctx: Context) {
    const filters = {
      customer: undefined,
      paymentMethod: undefined as unknown as FindOperator<any>
    }

    if (ctx.user.typeUser !== TypeUser.Admin) {
      filters.customer = ctx.user.id;
    }

    /* if (ctx.request.query.cart) {
      filters.paymentMethod = IsNull();
    } else {
      filters.paymentMethod = Not(IsNull());
    } */

    Object.keys(filters).forEach(key => filters[key] === undefined && delete filters[key]);

    return new HttpResponseOK(await this._orderService.getTotalByStatus(filters));
  }

  @Get('/total-cart')
  async getTotalCart(ctx: Context) {
    let where = '';

    if (ctx.user.typeUser !== TypeUser.Admin) {
      where = (where ? `${where} and ` : '') + `customer.id = ${ctx.user.id}`;
    }

    return new HttpResponseOK(await this._orderService.getTotalCart(where));
  }

  @Get('/:id')
  async getOrder(ctx: Context) {
    const orderId = ctx.request.params.id;
    const order = await this._orderService.getById(orderId);

    if (ctx.user.typeUser !== TypeUser.Admin && order.customer.id !== ctx.user.id) {
      throw new Error(`Order entity ${orderId} not found`);
    }

    return new HttpResponseOK(await this._orderRepresentationBuilder.toRepresentation(order));
  }

  @Post()
  async addOrder(ctx: Context) {
    const orderDto: OrderDto = ctx.request.body;
    const order = await this._orderRepresentationBuilder.fromRepresentationWithUser(orderDto, Order.Builder.create(), ctx.user);
    const orderSaved = await this._orderService.save(order);
    return new HttpResponseCreated(await this._orderRepresentationBuilder.toRepresentation(orderSaved));
  }

  @Put('/:id')
  async updateOrder(ctx: Context) {
    const orderId = ctx.request.params.id;
    const orderFound = await this._orderService.getById(orderId);

    if (ctx.user.typeUser !== TypeUser.Admin && orderFound.customer.id !== ctx.user.id) {
      throw new Error(`Order entity ${orderId} not found`);
    }

    const orderDto: OrderDto = ctx.request.body;
    const order = await this._orderRepresentationBuilder.fromRepresentation(orderDto, Order.Builder.from(orderFound));
    const orderSaved = await this._orderService.save(order);

    return new HttpResponseCreated(await this._orderRepresentationBuilder.toRepresentation(orderSaved));
  }

  @Delete('/:id')
  async deleteOrder(ctx: Context) {
    const orderId = ctx.request.params.id;
    const orderFound = await this._orderService.getById(orderId);

    if (ctx.user.typeUser !== TypeUser.Admin && orderFound.customer.id !== ctx.user.id) {
      throw new Error(`Order entity ${orderId} not found`);
    }

    await this._orderService.delete(orderFound);
    return new HttpResponseOK();
  }

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }

}

import {
  Context,
  Delete,
  dependency,
  Get,
  Hook,
  HttpResponseCreated,
  HttpResponseForbidden, HttpResponseNoContent,
  HttpResponseOK,
  isHttpResponseInternalServerError, Options,
  Post,
  Put
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { TypeUser } from '../enums/type-user.enum';
import { PackageService } from "../services/package-service";
import { PackageDto } from "../dtos/package-dto.entity";
import { Package } from "../entities/package.entity";
import { RefreshJWT } from "../hooks/refresh-token";
import { Sort } from '../utils/Sort';
import _ = require('lodash');


@JWTRequired()
@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  if (isHttpResponseInternalServerError(response)) {
    response.body = { error: response?.error?.message };
  }
})

export class PackageController {

  @dependency
  private readonly _packageService: PackageService;

  @dependency
  private readonly _packageRB: PackageDto.RepresentationBuilder;

  @Get()
  async getPackages(ctx: Context) {
    const skip = ctx.request.query.skip || 0;
    const take = ctx.request.query.take || 10;
    const sort = new Sort(ctx.request.query.sort, 'package.id', 'desc');

    const where = ctx.request.query.where;

    const packages = await this._packageService.getMany(where, skip, take, sort);
    const packagesDto: PackageDto[] = _.map(packages, orderPackage => this._packageRB.toRepresentation(orderPackage));

    return new HttpResponseOK({ "content": packagesDto, page: 1, lastPage: true, nextPage: false });
  }

  @Get('/:id')
  async getPackage(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }

    const packageId = ctx.request.params.id;
    const pack = await this._packageService.getById(packageId);

    return new HttpResponseOK(this._packageRB.toRepresentation(pack));
  }

  @Post()
  async addPackage(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }
    const packageDto: PackageDto = ctx.request.body;
    const pack = this._packageRB.fromRepresentation(packageDto, Package.Builder.create());
    const packageSaved = await this._packageService.save(pack);
    return new HttpResponseCreated(this._packageRB.toRepresentation(packageSaved));
  }

  @Put('/:id')
  async updatePackage(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }
    const packageId = ctx.request.params.id;
    const packageFound = await this._packageService.getById(packageId);

    const packageDto: PackageDto = ctx.request.body;
    const pack = this._packageRB.fromRepresentation(packageDto, Package.Builder.from(packageFound));
    const packageSaved = await this._packageService.save(pack);
    return new HttpResponseCreated(this._packageRB.toRepresentation(packageSaved));
  }

  @Delete('/:id')
  async deletePackage(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }

    const packageId = ctx.request.params.id;
    const packageFound = await this._packageService.getById(packageId);

    await this._packageService.delete(packageFound);
    return new HttpResponseOK();
  }

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }

}

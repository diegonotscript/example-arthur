import {
  Context,
  Delete,
  dependency,
  Get,
  Hook,
  HttpResponseCreated,
  HttpResponseForbidden,
  HttpResponseNoContent,
  HttpResponseOK,
  isHttpResponseInternalServerError,
  Options,
  Post,
  Put
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { RefreshJWT } from "../hooks/refresh-token";
import { AddressDto } from "../dtos/address-dto.entity";
import { getRepository } from "typeorm";
import { Address } from "../entities";
import { TypeUser } from '../enums/type-user.enum';
import { AddressService } from '../services/address-service';


@JWTRequired()
@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  if (isHttpResponseInternalServerError(response)) {
    response.body = { error: response?.error?.message };
  }
})

export class AddressController {

  @dependency
  private readonly _addressRB: AddressDto.RepresentationBuilder;

  @dependency
  private readonly _addressService: AddressService;

  @Get()
  async getAddresses(ctx: Context) {
    const filters = {
      // skip: ctx.request.query.skip || 0,
      // take: ctx.request.query.take || 10,
      user: ctx.user
    }

    const repository = getRepository(Address)
    const addressArray = await repository.find(filters);

    const addressDtos: AddressDto[] = new Array<AddressDto>();
    for (const address of addressArray) {
      const addressRB = this._addressRB.toRepresentation(address);
      addressDtos.push(addressRB);
    }

    return new HttpResponseOK({ "content": addressDtos, page: 1, lastPage: true, nextPage: false });
  }

  @Get('/default')
  async getAddressDefault(ctx: Context) {
    const repository = getRepository(Address)

    const address = await repository.findOne({
      user: ctx.user,
      isDefault: true
    });

    if (!address) {
      return new HttpResponseOK();
    }

    return new HttpResponseOK(this._addressRB.toRepresentation(address));
  }

  @Get('/:id')
  async getAddress(ctx: Context) {
    const addressId = ctx.request.params.id;
    const repository = getRepository(Address)

    const address = await repository.findOne({
      id: addressId
    });

    if (!address || address?.user.id !== ctx.user.id) {
      throw new Error(`Address entity ${addressId} not found`);
    }

    return new HttpResponseOK(this._addressRB.toRepresentation(address));
  }

  @Post()
  async addAddress(ctx: Context) {
    const addressDto: AddressDto = ctx.request.body;
    const address = this._addressRB.fromRepresentationWithUser(addressDto, Address.Builder.create(), ctx.user);
    const repository = getRepository(Address);

    const addressSaved = await repository.save(address);
    return new HttpResponseCreated(this._addressRB.toRepresentation(addressSaved));
  }

  @Put('/:id')
  async update(ctx: Context) {
    const addressId = ctx.request.params.id;
    const addressFound: Address = await this._addressService.getByIdThrowable(addressId);

    const addressDto: AddressDto = ctx.request.body;
    const address = this._addressRB.fromRepresentation(addressDto, Address.Builder.from(addressFound));
    const addressSaved = await this._addressService.save(address);
    return new HttpResponseCreated(this._addressRB.toRepresentation(addressSaved));
  }

  @Delete('/:id')
  async delete(ctx: Context) {
    const addressId = ctx.request.params.id;
    const addressFound = await this._addressService.getByIdThrowable(addressId);

    await this._addressService.delete(addressFound);
    return new HttpResponseOK();
  }

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }

}

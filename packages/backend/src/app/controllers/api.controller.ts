import { Context, controller, Hook, HttpResponseNoContent, IAppController, Options } from '@foal/core';
import { UserController } from './user.controller';
import { DriverController } from './driver.controller';
import { OrderController } from './order.controller';
import { RefreshJWT } from "../hooks/refresh-token";
import { OrderStatusController } from "./order-status.controller";
import { PackageController } from "./package.controller";
import { AddressController } from "./address.controller";
import { ConfigurationController } from './configuration.controller';

@RefreshJWT()
@Hook(() => response => {
  // Every response of this controller and its sub-controllers will be added this header.
  response.setHeader('Access-Control-Allow-Origin', '*');
})
export class ApiController implements IAppController {
  subControllers = [
    controller('/users', UserController),
    controller('/drivers', DriverController),
    controller('/orders', OrderController),
    controller('/address', AddressController),
    controller('/orders-status', OrderStatusController),
    controller('/packages', PackageController),
    controller('/configuration', ConfigurationController),
  ];

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }
}

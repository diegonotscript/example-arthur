import {
  Context,
  dependency,
  Get,
  Hook, HttpResponseCreated, HttpResponseNoContent,
  HttpResponseOK,
  HttpResponseUnauthorized,
  isHttpResponseInternalServerError, Options, Put
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';

import { User } from '../entities';

import { getRepository } from 'typeorm';
import { TypeUser } from '../enums/type-user.enum';
import * as _ from 'lodash';
import { UserDto } from '../dtos';
import { UserService } from '../services/user-service';
import { RefreshJWT } from "../hooks/refresh-token";
import { PageDto } from '../dtos/page-dto.entity';
import { Sort } from '../utils/Sort';

@JWTRequired()
@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  if (isHttpResponseInternalServerError(response)) {
    response.body = { error: response?.error?.message };
  }
})

export class UserController {

  @dependency
  private readonly _userService: UserService;

  @dependency
  private readonly _userRepresentationBuilder: UserDto.RepresentationBuilder;

  @Get('/@me')
  async getMe(ctx: Context) {
    const userFound = await this._userService.getById(ctx.user.id);

    if (!userFound) {
      throw new Error(`User entity ${ctx.user.id} not found`);
    }

    return new HttpResponseOK(await this._userRepresentationBuilder.toRepresentation(userFound));
  }

  @Get()
  async getUsers(ctx: Context) {
    const skip = ctx.request.query.skip || 0;
    const take = ctx.request.query.take || 10;
    const sort = new Sort(ctx.request.query.sort, 'user.id', 'desc');

    if (ctx.user.typeUser !== TypeUser.Admin) {
      return new HttpResponseUnauthorized();
    }

    const userRepository = getRepository(User);
    const users = await userRepository.find({
      skip: skip,
      take: take
    })

    const usersDto: UserDto[] = new Array<UserDto>();
    for (const user of users) {
      const userRB = await this._userRepresentationBuilder.toRepresentation(user);
      usersDto.push(userRB);
    }

    return new HttpResponseOK(new PageDto(usersDto, await userRepository.count(), skip));
  }

  @Get('/drivers')
  async getDrivers(ctx: Context) {
    const skip = ctx.request.query.skip || 0;
    const take = ctx.request.query.take || 10;
    let where = ctx.request.query.where;

    if (ctx.user.typeUser !== TypeUser.Admin) {
      return new HttpResponseUnauthorized();
    }

    where = (where ? `${where} and ` : '') + `typeUser = ${TypeUser.Driver}`;

    const userRepository = getRepository(User);
    const users = await userRepository.find({
      skip: skip,
      take: take,
      where: where
    });

    const usersDto: UserDto[] = new Array<UserDto>();
    for (const user of users) {
      const userRB = await this._userRepresentationBuilder.toRepresentation(user);
      usersDto.push(userRB);
    }

    return new HttpResponseOK(new PageDto(usersDto, await userRepository.count(), skip));
  }

  @Put()
  async updateUser(ctx: Context) {
    const userFound = await this._userService.getById(ctx.user.id);

    if (!userFound) {
      throw new Error(`User entity ${ctx.user.id} not found`);
    }

    const userDto: UserDto = ctx.request.body;
    const user = await this._userRepresentationBuilder.fromRepresentation(userDto, User.Builder.from(userFound));
    const userSaved = await this._userService.update(user, userDto);
    return new HttpResponseCreated(await this._userRepresentationBuilder.toRepresentation(userSaved));
  }

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }
}

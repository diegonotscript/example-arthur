import {
  Context,
  Delete,
  dependency,
  Get,
  Hook,
  HttpResponseCreated,
  HttpResponseForbidden, HttpResponseNoContent,
  HttpResponseOK,
  isHttpResponseInternalServerError, Options,
  Post,
  Put
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { ConfigurationDto } from '../dtos/configuration-dto.entity';
import { Configuration } from '../entities/configuration.entity';
import { TypeUser } from '../enums/type-user.enum';
import { RefreshJWT } from "../hooks/refresh-token";
import { ConfigurationService } from '../services/configuration-service';

@JWTRequired()
@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  if (isHttpResponseInternalServerError(response)) {
    response.body = { error: response?.error?.message };
  }
})

export class ConfigurationController {

  @dependency
  private readonly _configurationService: ConfigurationService;

  @dependency
  private readonly _configurationRB: ConfigurationDto.RepresentationBuilder;

  @Get()
  async getConfiguration() {
    const configuration = await this._configurationService.getFirst();

    if (!configuration) {
      return new HttpResponseOK();
    }

    return new HttpResponseOK(this._configurationRB.toRepresentation(configuration));
  }

  @Post()
  async addConfiguration(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }

    let builder = Configuration.Builder.create();
    const configurationFound = await this._configurationService.getFirst();
    if (configurationFound) {
      builder = Configuration.Builder.from(configurationFound);
    }

    const configurationDto: ConfigurationDto = ctx.request.body;
    const configuration = this._configurationRB.fromRepresentation(configurationDto, builder);
    const configurationSaved = await this._configurationService.save(configuration);

    return new HttpResponseCreated(this._configurationRB.toRepresentation(configurationSaved));
  }

  @Put()
  async updateConfiguration(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }

    const configurationFound = await this._configurationService.getFirst();

    const configurationDto: ConfigurationDto = ctx.request.body;
    const configuration = this._configurationRB.fromRepresentation(configurationDto, Configuration.Builder.from(configurationFound));
    const configurationSaved = await this._configurationService.save(configuration);

    return new HttpResponseCreated(this._configurationRB.toRepresentation(configurationSaved));
  }

  @Delete()
  async deleteConfiguration(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }

    const configurationFound = await this._configurationService.getFirst();

    await this._configurationService.delete(configurationFound);
    return new HttpResponseOK();
  }

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }

}

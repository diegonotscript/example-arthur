import {
  Context,
  dependency,
  HttpResponseBadRequest,
  HttpResponseOK,
  HttpResponseUnauthorized,
  Post,
  ValidateBody,
  verifyPassword
} from '@foal/core';
import { getSecretOrPrivateKey } from '@foal/jwt';
import { sign } from 'jsonwebtoken';

import { User } from '../entities';
import { UserDto } from '../dtos';
import { credentialsSchema, credentialsSchemaCreate, emailSchema } from '../validators/auth.validator';
import { TypeUser } from '../enums/type-user.enum';
import { UserService } from '../services/user-service';
import { JsonFormatter } from '../utils/json';

export class AuthController {

  @dependency
  private readonly _userService: UserService;

  @dependency
  private readonly _userRepresentationBuilder: UserDto.RepresentationBuilder;

  @Post('/signup')
  @ValidateBody(credentialsSchemaCreate)
  async signup(ctx: Context) {
    try {
      await this._userService.save(ctx.request.body, TypeUser.User);
      return new HttpResponseOK();
    } catch (e) {
      return new HttpResponseBadRequest(JsonFormatter.toJson(e.message));
    }
  }

  @Post('/login')
  @ValidateBody(credentialsSchema)
  async login(ctx: Context) {
    const user = await User.findOne({ email: ctx.request.body.email });

    if (!user) {
      return new HttpResponseUnauthorized('Email not found on our base');
    }

    if (!await verifyPassword(ctx.request.body.password, user.password)) {
      return new HttpResponseUnauthorized('User or password incorrectly');
    }

    const token = sign(
      { id: user.id, email: user.email, typeUser: user.typeUser },
      getSecretOrPrivateKey(),
      { expiresIn: '1h' }
    );

    return new HttpResponseOK({ token: token });
  }

  @Post('/exists-email')
  @ValidateBody(emailSchema)
  async existsEmail(ctx: Context) {
    const user = await User.findOne({ email: ctx.request.body.email });

    return new HttpResponseOK(!!user);
  }

}

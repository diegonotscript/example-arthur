import {
  Context,
  Delete,
  dependency,
  Get,
  Hook,
  HttpResponseCreated,
  HttpResponseForbidden, HttpResponseNoContent,
  HttpResponseOK,
  isHttpResponseInternalServerError, Options,
  Post,
  Put
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { TypeUser } from '../enums/type-user.enum';
import { OrderStatusService } from "../services/order-status-service";
import { OrderStatusDto } from "../dtos/order-status-dto.entity";
import { OrderStatus } from "../entities/order-status.entity";
import { RefreshJWT } from "../hooks/refresh-token";
import _ = require('lodash');


@JWTRequired()
@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  if (isHttpResponseInternalServerError(response)) {
    response.body = { error: response?.error?.message };
  }
})

export class OrderStatusController {

  @dependency
  private readonly _orderStatusService: OrderStatusService;

  @dependency
  private readonly _orderStatusRB: OrderStatusDto.RepresentationBuilder;

  @Get()
  async getMany(ctx: Context) {
    const filters = {
      skip: ctx.request.query.skip || 0,
      take: ctx.request.query.take || 10,
    }

    const orderStatuses = await this._orderStatusService.getMany(filters);
    const orderStatusDto: OrderStatusDto[] = _.map(orderStatuses, orderStatus => this._orderStatusRB.toRepresentation(orderStatus));

    return new HttpResponseOK({ "content": orderStatusDto, page: 1, lastPage: true, nextPage: false });
  }

  @Get('/:id')
  async getOrderStatus(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }

    const orderStatusId = ctx.request.params.id;
    const orderStatus = await this._orderStatusService.getByIdThrowable(orderStatusId);

    return new HttpResponseOK(this._orderStatusRB.toRepresentation(orderStatus));
  }

  @Post()
  async addOrderStatus(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }
    const orderStatusDto: OrderStatusDto = ctx.request.body;
    const orderStatus = this._orderStatusRB.fromRepresentation(orderStatusDto, OrderStatus.Builder.create());
    const orderStatusSaved = await this._orderStatusService.save(orderStatus);
    return new HttpResponseCreated(this._orderStatusRB.toRepresentation(orderStatusSaved));
  }

  @Put('/:id')
  async updateOrderStatus(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }
    const orderStatusId = ctx.request.params.id;
    const orderStatusFound = await this._orderStatusService.getByIdThrowable(orderStatusId);

    const orderStatusDto: OrderStatusDto = ctx.request.body;
    const orderStatus = this._orderStatusRB.fromRepresentation(orderStatusDto, OrderStatus.Builder.from(orderStatusFound));
    const orderStatusSaved = await this._orderStatusService.save(orderStatus);
    return new HttpResponseCreated(this._orderStatusRB.toRepresentation(orderStatusSaved));
  }

  @Delete('/:id')
  async deleteOrderStatus(ctx: Context) {
    if (TypeUser.Admin !== ctx.user.typeUser) {
      return new HttpResponseForbidden();
    }

    const orderStatusId = ctx.request.params.id;
    const orderStatusFound = await this._orderStatusService.getByIdThrowable(orderStatusId);

    await this._orderStatusService.delete(orderStatusFound);
    return new HttpResponseOK();
  }

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }

}

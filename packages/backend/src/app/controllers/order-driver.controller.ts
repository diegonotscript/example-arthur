import {
  Context,
  dependency,
  Hook,
  HttpResponseNoContent,
  isHttpResponseInternalServerError, Options
} from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { OrderService } from "../services/order-service";
import { RefreshJWT } from "../hooks/refresh-token";

@JWTRequired()
@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  if (isHttpResponseInternalServerError(response)) {
    response.body = { error: response?.error?.message };
  }
})

export class OrderDriverController {

  @dependency
  private readonly _orderService: OrderService;

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }
}

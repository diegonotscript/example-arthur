import { IOrderRepository } from './order-repository.interface';
import { Order } from '../entities/order.entity';
import { getRepository, IsNull, Not } from "typeorm";
import { Address } from '../entities';
import { Sort } from '../utils/Sort';

export class OrderRepository implements IOrderRepository {
  async save(order: Order) {
    order.updatedAt = new Date();
    await order.save();
    return order;
  }

  async delete(order: Order) {
    return await order.remove();
  }

  async getById(id: any) {
    return await this.getBaseQuery()
      .where({ id: id })
      .getOne();
  }

  async getOrders(where: any, skip: number, take: number, sort: Sort) {
    console.log(where);
    
    return await this.getBaseQuery()
      .where(where)
      .limit(take)
      .offset(skip)
      .orderBy(sort.field, sort.sort)
      .getMany();
  }

  async getTotal(where: any) {
    return await this.getBaseQuery()
      .where(where)
      .getCount();
  }

  async getTotalByStatus(where: any) {
    return {
      totalWaitingCollection: await this.getBaseQuery().where({ orderStatus: IsNull(), ...where }).getCount(),
      totalInProcess: await this.getBaseQuery().where({ orderStatus: Not(IsNull()), ...where }).getCount(),
    }
  }

  async getTotalCart(where: any) {
    where = (where ? `${where} and ` : '') + `order.paymentMethod is null`;

    return await this.getBaseQuery()
      .where(where)
      .getCount();
  }

  getBaseQuery() {
    return getRepository(Order).createQueryBuilder('order')
      .innerJoinAndSelect('order.customer', 'customer')
      .innerJoinAndSelect('order.collectionAddress', 'collectionAddress')
      .innerJoinAndSelect('order.orderPackage', 'orderPackage')
      .leftJoinAndSelect('order.orderStatus', 'orderStatus')
      .leftJoinAndSelect('order.driver', 'driver')
      .leftJoinAndSelect('order.paymentMethod', 'paymentMethod');
  }

}
import { getRepository } from 'typeorm';
import { IAddressRepository } from "./address-repository.interface";
import { Address } from "../entities";

export class AddressRepository implements IAddressRepository {

  async getById(id: any): Promise<Address | undefined> {
    const repository = getRepository(Address);
    return await repository.findOne({
      where: { id: id },
      relations: ['user']
    });
  }

  async save(address: Address) {
    await address.save();
    return address;
  }

  async delete(address: Address) {
    return await address.remove();
  }

}
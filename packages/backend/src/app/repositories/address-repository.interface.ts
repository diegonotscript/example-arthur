import { Address } from "../entities";

export interface IAddressRepository {
  getById: (id: any) => Promise<Address | undefined>;
}
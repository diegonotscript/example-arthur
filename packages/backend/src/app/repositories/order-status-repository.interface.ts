import { OrderStatus } from "../entities/order-status.entity";

export interface IOrderStatusRepository {
  getById: (id: any) => Promise<OrderStatus | undefined>;
  save: (orderStatus: OrderStatus) => Promise<OrderStatus>;
  delete: (orderStatus: OrderStatus) => Promise<OrderStatus>;
  getMany: (filter: any) => Promise<OrderStatus[]>;
}
import { TypeUser } from '../enums/type-user.enum';
import { hashPassword } from '@foal/core';
import { Address, User } from '../entities';
import { IUserRepository } from './user-repository.interface';
import { getRepository, Not } from 'typeorm';

export class UserRepository implements IUserRepository {

  async save(body, typeUser: TypeUser) {
    const user = new User();
    user.email = body.email;
    user.password = await hashPassword(body.password);
    user.companyName = body.companyName;
    user.phoneNumber = body.phoneNumber;
    user.name = body.name;
    user.typeUser = typeUser;

    if (!user.addresses) {
      user.addresses = [];

      const address = new Address();
      address.postCode = body.address.postCode;
      address.address = body.address.address;
      address.city = body.address.city;
      address.country = body.address.country;
      address.isDefault = true;
      user.addresses.push(address);
    }

    await user.save();

    return user;
  }

  async update(user: User) {
    await user.save();
    return user;
  }

  async alreadyExistByEmail(email: string, id: number | undefined): Promise<boolean> {
    const userRepository = getRepository(User)

    let params = { email: email };
    if (id) {
      params['id'] = Not(id);
    }

    return await userRepository.count(params) > 0;
  }

  async getById(id: any): Promise<User | undefined> {
    const repository = getRepository(User);
    return await repository.findOne({ id: id });
  }

}
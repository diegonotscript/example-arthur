import { Order } from '../entities/order.entity';

export interface IOrderRepository {
  save: (order: Order) => Promise<Order>;
  delete: (order: Order) => Promise<Order>;
  getById: (id: any) => Promise<Order | undefined>;
  getOrders: (filters: any, skip: number, take: number, sort: any) => Promise<Order[]>;
}
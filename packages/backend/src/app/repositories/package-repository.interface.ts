import { Package } from "../entities/package.entity";

export interface IPackageRepository {
  getById: (id: any) => Promise<Package | undefined>;
  getMany: (where: any, skip: number, take: number, sort: any) => Promise<Package[]>;
}
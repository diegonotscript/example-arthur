import { Configuration } from "../entities/configuration.entity";

export interface IConfigurationRepository {
  getFirst: () => Promise<Configuration>;
}
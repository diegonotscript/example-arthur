import { TypeUser } from '../enums/type-user.enum';
import { User } from '../entities';

export interface IUserRepository {
  alreadyExistByEmail: (email: string, id: number) => Promise<boolean>;
  save: (body, typeUser: TypeUser) => Promise<User>;
  getById: (id: any) => Promise<User | undefined>;
}
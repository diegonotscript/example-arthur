import { getRepository } from 'typeorm';
import { IConfigurationRepository } from "./configuration-repository.interface";
import { Configuration } from "../entities/configuration.entity";

export class ConfigurationRepository implements IConfigurationRepository {

  async getFirst(): Promise<any> {
    const repository = getRepository(Configuration)
    return await repository.findOne();
  }

  async save(entity: Configuration) {
    await entity.save();
    return entity;
  }

  async delete(entity: Configuration) {
    return await entity.remove();
  }
}
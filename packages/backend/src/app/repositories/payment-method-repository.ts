import { getRepository } from 'typeorm';
import { PaymentMethod } from '../entities/payment-method.entity';
import { IPaymentMethodRepository } from './payment-method-repository.interface';

export class PaymentMethodRepository implements IPaymentMethodRepository {

  async getById(id: any): Promise<PaymentMethod | undefined> {
    const repository = getRepository(PaymentMethod);
    return await repository.findOne({ id: id });
  }

  async save(entity: PaymentMethod) {
    await entity.save();
    return entity;
  }

  async delete(entity: PaymentMethod) {
    return await entity.remove();
  }

  async getPaymentMethod(filter: any) {
    const repository = getRepository(PaymentMethod);
    return await repository.find(filter);
  }
}
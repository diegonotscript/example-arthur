import { PaymentMethod } from "../entities/payment-method.entity";

export interface IPaymentMethodRepository {
  getById: (id: any) => Promise<PaymentMethod | undefined>;
  save: (paymentMethod: PaymentMethod) => Promise<PaymentMethod>;
  delete: (paymentMethod: PaymentMethod) => Promise<PaymentMethod>;
  getPaymentMethod: (filter: any) => Promise<PaymentMethod[]>;
}
import { getRepository } from 'typeorm';
import { IPackageRepository } from "./package-repository.interface";
import { Package } from "../entities/package.entity";
import { Sort } from '../utils/Sort';

export class PackageRepository implements IPackageRepository {

  async getById(id: any): Promise<Package | undefined> {
    return await this.getBaseQuery()
      .where({ id: id })
      .getOne();
  }

  async save(pack: Package) {
    await pack.save();
    return pack;
  }

  async delete(pack: Package) {
    return await pack.remove();
  }

  async getMany(where: any, skip: number, take: number, sort: Sort) {
    return await this.getBaseQuery()
      .where(where)
      .limit(take)
      .offset(skip)
      .orderBy(sort.field, sort.sort)
      .getMany();
  }

  getBaseQuery() {
    return getRepository(Package).createQueryBuilder('package');
  }

}
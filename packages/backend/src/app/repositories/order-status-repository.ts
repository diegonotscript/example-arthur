import { getRepository } from 'typeorm';
import { OrderStatus } from "../entities/order-status.entity";
import { IOrderStatusRepository } from "./order-status-repository.interface";

export class OrderStatusRepository implements IOrderStatusRepository {

  async getById(id: any): Promise<OrderStatus | undefined> {
    const repository = getRepository(OrderStatus);
    return await repository.findOne({ id: id });
  }

  async save(orderStatus: OrderStatus) {
    await orderStatus.save();
    return orderStatus;
  }

  async delete(orderStatus: OrderStatus) {
    return await orderStatus.remove();
  }

  async getMany(filter: any) {
    const repository = getRepository(OrderStatus);
    return await repository.find(filter);
  }
}
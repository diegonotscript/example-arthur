const addressSchema = {
  additionalProperties: false,
  properties: {
    postCode: { type: 'string' },
    address: { type: 'string' },
    city: { type: 'string' },
    country: { type: 'string' },
  },
  required: ['postCode', 'address', 'city', 'country'],
  type: 'object',
};

export const credentialsSchemaCreate = {
  additionalProperties: false,
  properties: {
    email: { type: 'string', format: 'email' },
    password: { type: 'string' },
    companyName: { type: 'string' },
    phoneNumber: { type: 'string' },
    name: { type: 'string' },
    address: addressSchema
  },
  required: ['email', 'password', 'companyName', 'phoneNumber', 'name', 'address'],
  type: 'object',
};

export const credentialsSchema = {
  additionalProperties: false,
  properties: {
    email: { type: 'string', format: 'email' },
    password: { type: 'string' }
  },
  required: ['email', 'password'],
  type: 'object',
};

export const emailSchema = {
  additionalProperties: false,
  properties: {
    email: { type: 'string', format: 'email' },
  },
  required: ['email'],
  type: 'object',
};

export interface BaseBuilder<E> {
    build: () => E;
}
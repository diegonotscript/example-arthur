export interface BaseRepresentationBuilder<E, D, B> {
    toRepresentation: (entity: E) => D;
    fromRepresentation: (representation: D, builder: B) => E;
}
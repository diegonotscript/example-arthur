export interface BaseRepresentationBuilderAsync<E, D, B> {
    toRepresentation: (entity: E) => Promise<D>;
    fromRepresentation: (representation: D, builder: B) => Promise<E>;
}
import {Context, controller, Hook, HttpResponseNoContent, IAppController, Options} from '@foal/core';
import {createConnection} from 'typeorm';

import {ApiController, AuthController} from './controllers';
import {RefreshJWT} from "./hooks/refresh-token";

@RefreshJWT()
@Hook(() => response => {
  response.setHeader('Access-Control-Allow-Origin', '*');
})
export class AppController implements IAppController {
  subControllers = [
    controller('/auth', AuthController),
    controller('/api', ApiController),
  ];

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    return response;
  }

  async init () {
    await createConnection();
  }
}

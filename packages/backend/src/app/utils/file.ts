import { writeFileSync, existsSync, mkdirSync, readFileSync } from 'fs';

export class File {
  private dir = './public/images/';
  public async write(path: string, name: string, base64data: string) {
    if (!name || !base64data) {
      return;
    }

    let dir = this.dir + path;

    if (!existsSync(dir)) {
      mkdirSync(dir, { recursive: true });
    }

    base64data = base64data.replace(/^data:image\/png;base64,/, "");
    base64data = base64data.replace(/^data:image\/jpg;base64,/, "");
    base64data = base64data.replace(/^data:image\/jpeg;base64,/, "");

    return writeFileSync(`${dir}/${name}`, base64data, 'base64');
  }

  public async read(path: string, name: string): Promise<string> {
    if (!name) {
      return '';
    }

    let dir = this.dir + path + '/' + name;

    return 'data:image/jpeg;base64,' + readFileSync(dir, 'base64');
  }
}
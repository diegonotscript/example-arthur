export class Sort {
  public field: string;
  public sort: any = 'ASC';

  constructor(querySort: any, defaultField: string, defaultSort: any) {
    let arraySort = [];
    if (querySort) {
      arraySort = querySort.split(',');
    }
    this.field = arraySort[0] || defaultField;
    this.sort = arraySort[1] || defaultSort;

    if (this.sort) {
      this.sort = this.sort.toUpperCase();
    }
  }

}

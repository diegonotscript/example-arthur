export enum TypeStatusFilter {
  WaitingCollection = 0,
  InProcess = 1,
}

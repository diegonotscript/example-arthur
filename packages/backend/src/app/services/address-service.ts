import { dependency } from '@foal/core';
import { IAddressService } from "./address-service.interface";
import { Address } from "../entities/address.entity";
import { AddressRepository } from "../repositories/address-repository";
import { getRepository } from 'typeorm';

export class AddressService implements IAddressService {

  @dependency
  private readonly _addressRepository: AddressRepository;

  async getById(id: any): Promise<Address | undefined> {
    return await this._addressRepository.getById(id);
  }

  async getByIdThrowable(id: any): Promise<Address> {
    const entity = await this._addressRepository.getById(id);

    if (!entity) {
      throw new Error(`Address entity ${id} not found`);
    }

    return entity;
  }

  async save(address: Address): Promise<Address> {
    const repository = getRepository(Address);

    if (address.isDefault) {
      const defaultAddress = await repository.findOne({
        user: address.user,
        isDefault: true
      });

      if (defaultAddress && defaultAddress.id !== address.id) {
        defaultAddress.isDefault = false;
        await this._addressRepository.save(defaultAddress);
      }
    }

    return await this._addressRepository.save(address);
  }

  async delete(address: Address): Promise<Address> {
    return await this._addressRepository.delete(address);
  }

}
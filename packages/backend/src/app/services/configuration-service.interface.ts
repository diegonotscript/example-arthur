import { Configuration } from '../entities/configuration.entity';

export interface IConfigurationService {
  getFirst: () => Promise<Configuration>;
  save: (entity: Configuration) => Promise<Configuration>;
  delete: (entity: Configuration) => Promise<Configuration>;
}
import { dependency } from '@foal/core';
import { PaymentMethod } from '../entities/payment-method.entity';
import { PaymentMethodRepository } from '../repositories/payment-method-repository';
import { IPaymentMethodService } from './payment-method-service.interface';

export class PaymentMethodService implements IPaymentMethodService {

  @dependency
  private readonly _paymentMethodRepository: PaymentMethodRepository;

  async save(order: PaymentMethod): Promise<PaymentMethod> {
    return await this._paymentMethodRepository.save(order);
  }

  async delete(order: PaymentMethod): Promise<PaymentMethod> {
    return await this._paymentMethodRepository.delete(order);
  }

  async getById(id: any): Promise<PaymentMethod | undefined> {
    return await this._paymentMethodRepository.getById(id);
  }

  async getByIdThrowable(id: any): Promise<PaymentMethod> {
    const entity = await this._paymentMethodRepository.getById(id);

    if (!entity) {
      throw new Error(`Payment Method entity ${id} not found`);
    }

    return entity;
  }

  async getPaymentMethod(filters: any): Promise<PaymentMethod[]> {
    return await this._paymentMethodRepository.getPaymentMethod(filters);
  }
}
import { PaymentMethod } from "../entities/payment-method.entity";

export interface IPaymentMethodService {
  save: (paymentMethod: PaymentMethod) => Promise<PaymentMethod>;
  delete: (paymentMethod: PaymentMethod) => Promise<PaymentMethod>;
  getById: (id: any) => Promise<PaymentMethod | undefined>;
  getByIdThrowable: (id: any) => Promise<PaymentMethod>;
  getPaymentMethod: (filters: any) => Promise<PaymentMethod[]>;
}
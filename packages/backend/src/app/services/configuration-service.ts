import { dependency } from '@foal/core';
import { Configuration } from '../entities/configuration.entity';
import { ConfigurationRepository } from '../repositories/configuration-repository';
import { IConfigurationService } from './configuration-service.interface';

export class ConfigurationService implements IConfigurationService {

  @dependency
  private readonly _configurationRepository: ConfigurationRepository;

  async getFirst(): Promise<any> {
    return await this._configurationRepository.getFirst();
  }

  async save(pack: Configuration): Promise<Configuration> {
    return await this._configurationRepository.save(pack);
  }

  async delete(pack: Configuration): Promise<Configuration> {
    return await this._configurationRepository.delete(pack);
  }

}
import { Address } from '../entities/address.entity';

export interface IAddressService {
  getById: (id: any) => Promise<Address | undefined>;
  save: (data: Address) => Promise<Address>;
  delete: (data: Address) => Promise<Address>;
}
import { dependency } from '@foal/core';
import { Order } from '../entities/order.entity';
import { IOrderService } from './order-service.interface';
import { OrderRepository } from "../repositories/order-repository";
import { Sort } from '../utils/Sort';
import { writeFileSync, existsSync, mkdirSync } from 'fs';
import { uniqueId } from 'lodash';
import { File } from '../utils/file';

export class OrderService implements IOrderService {

  @dependency
  private readonly _orderRepository: OrderRepository;

  async save(order: Order): Promise<Order> {

    let imageName = `${uniqueId()}.png`;

    if (order.productImageUrl && order.productImageUrl !== imageName) {
      new File().write('orders', imageName, order.productImageUrl);
      order.productImageUrl = imageName;
    }

    return await this._orderRepository.save(order);
  }

  async delete(order: Order): Promise<Order> {
    return await this._orderRepository.delete(order);
  }

  async getById(id: number): Promise<Order> {
    const entity = await this._orderRepository.getById(id);

    if (!entity) {
      throw new Error(`Order entity ${id} not found`);
    }

    return entity;
  }

  async getOrders(where: any, skip: number, take: number, sort: Sort): Promise<Order[]> {
    return await this._orderRepository.getOrders(where, skip, take, sort);
  }

  async getTotal(where: any): Promise<number> {
    return await this._orderRepository.getTotal(where);
  }

  async getTotalByStatus(where: any): Promise<any> {
    return await this._orderRepository.getTotalByStatus(where);
  }

  async getTotalCart(where: any): Promise<any> {
    return await this._orderRepository.getTotalCart(where);
  }

}
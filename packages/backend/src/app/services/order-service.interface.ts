import { Order } from '../entities/order.entity';

export interface IOrderService {
  save: (order: Order) => Promise<Order>;
  delete: (order: Order) => Promise<Order>;
  getById: (id: number) => Promise<Order>;
  getOrders: (filters: any, skip: number, take: number, sort: any) => Promise<Order[]>;
}
import { dependency } from '@foal/core';
import { IPackageService } from "./package-service.interface";
import { Package } from "../entities/package.entity";
import { PackageRepository } from "../repositories/package-repository";
import { Sort } from '../utils/Sort';

export class PackageService implements IPackageService {

  @dependency
  private readonly _packageRepository: PackageRepository;

  async getById(id: any): Promise<Package> {
    const entity = await this._packageRepository.getById(id);

    if (!entity) {
      throw new Error(`Package entity ${id} not found`);
    }

    return entity;
  }

  async save(pack: Package): Promise<Package> {
    return await this._packageRepository.save(pack);
  }

  async delete(pack: Package): Promise<Package> {
    return await this._packageRepository.delete(pack);
  }

  async getMany(where: any, skip: number, take: number, sort: Sort): Promise<Package[]> {
    return await this._packageRepository.getMany(where, skip, take, sort);
  }
}
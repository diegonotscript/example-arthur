import { dependency } from '@foal/core';
import { IOrderStatusService } from "./order-status-service.interface";
import { OrderStatus } from "../entities/order-status.entity";
import { OrderStatusRepository } from "../repositories/order-status-repository";

export class OrderStatusService implements IOrderStatusService {

  @dependency
  private readonly _orderStatusRepository: OrderStatusRepository;

  async save(order: OrderStatus): Promise<OrderStatus> {
    return await this._orderStatusRepository.save(order);
  }

  async delete(order: OrderStatus): Promise<OrderStatus> {
    return await this._orderStatusRepository.delete(order);
  }

  async getById(id: any): Promise<OrderStatus | undefined> {
    return await this._orderStatusRepository.getById(id);
  }

  async getByIdThrowable(id: any): Promise<OrderStatus> {
    const entity = await this._orderStatusRepository.getById(id);

    if (!entity) {
      throw new Error(`Order status entity ${id} not found`);
    }

    return entity;
  }

  async getMany(filters: any): Promise<OrderStatus[]> {
    return await this._orderStatusRepository.getMany(filters);
  }
}
import { IUserService } from './user-service.interface';
import { TypeUser } from '../enums/type-user.enum';
import { User } from '../entities';
import { UserRepository } from '../repositories/user-repository';
import { dependency, hashPassword, verifyPassword } from '@foal/core';
import { uniqueId } from 'lodash';
import { File } from '../utils/file';

export class UserService implements IUserService {

  @dependency
  private readonly _userRepository: UserRepository;

  async getById(id: any): Promise<User | undefined> {
    return await this._userRepository.getById(id);
  }

  async getByIdThrowable(id: any): Promise<User> {
    const entity = await this._userRepository.getById(id);

    if (!entity) {
      throw new Error(`Order status entity ${id} not found`);
    }

    return entity;
  }

  async save(body, typeUser: TypeUser): Promise<User> {
    await this.validateEmail(body, null);

    if (!body.companyWebsite) {
      body.showWebsite = false;
    }

    this.updateLogo(body);

    return await this._userRepository.save(body, typeUser);
  }

  async update(user: User, body): Promise<User> {
    await this.validateEmail(body, user);

    if (body.password) {
      if (!await verifyPassword(body.currentPassword, user.password)) {
        throw new Error(`Wrong password`);
      }
      user.password = await hashPassword(body.password);
    }

    this.updateLogo(user);

    return await this._userRepository.update(user);
  }

  async validateEmail(body, user) {
    if (await this._userRepository.alreadyExistByEmail(body.email, user?.id)) {
      throw new Error(`Email already exists`);
    }
  }

  async updateLogo(user) {

    let imageName = `${uniqueId()}.png`;

    if (user.photoUrl && user.photoUrl !== imageName) {
      new File().write('users', imageName, user.photoUrl);
      user.photoUrl = imageName;
    } else {
      user.showLogo = false;
    }

  }

}
import { OrderStatus } from "../entities/order-status.entity";

export interface IOrderStatusService {
  save: (orderStatus: OrderStatus) => Promise<OrderStatus>;
  delete: (orderStatus: OrderStatus) => Promise<OrderStatus>;
  getById: (id: any) => Promise<OrderStatus | undefined>;
  getByIdThrowable: (id: any) => Promise<OrderStatus>;
  getMany: (filters: any) => Promise<OrderStatus[]>;
}
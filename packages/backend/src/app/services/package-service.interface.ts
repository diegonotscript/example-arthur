import { Package } from '../entities/package.entity';

export interface IPackageService {
  getById: (id: number) => Promise<Package>;
  save: (data: Package) => Promise<Package>;
  delete: (data: Package) => Promise<Package>;
  getMany: (where: any, skip: number, take: number, sort: any) => Promise<Package[]>;
}
import { TypeUser } from '../enums/type-user.enum';
import { User } from '../entities';

export interface IUserService {
  save: (body, typeUser: TypeUser) => Promise<User>;
  getById: (id: any) => Promise<User | undefined>;
  getByIdThrowable: (id: any) => Promise<User>;
}
import 'source-map-support/register';

// std
// import * as http from 'http';
import * as cors from 'cors';
import * as express from 'express';
import { urlencoded, json } from 'express';

// 3p
import { Config, createApp } from '@foal/core';

// App
import { AppController } from './app/app.controller';

async function main() {
  const expressInstance = express();
  expressInstance.use(cors());
  expressInstance.use(json({ limit: '25mb' }));
  expressInstance.use(urlencoded({ limit: '25mb' }));

  // const httpServer = http.createServer(app);
  const port = Config.get('port', 'number', 3001);

  await createApp(AppController, {
    expressInstance: expressInstance
  });

  expressInstance.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  });

}

main()
  .catch(err => { console.error(err.stack); process.exit(1); });

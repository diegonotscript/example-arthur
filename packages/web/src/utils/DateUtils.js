import moment from 'moment';

class DateUtils {

  static now() {
    return moment().format('YYYY-MM-DD');
  };

  static formatDateDefault(date) {
    return moment(date).format('YYYY-MM-DD');
  };

  static formatDateHourDefault(date) {
    return moment(date).format('YYYY-MM-DD HH:mm');
  };

}

export default DateUtils;
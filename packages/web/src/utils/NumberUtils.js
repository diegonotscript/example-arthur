import CurrencyFormatter from 'currency-formatter';

class NumberUtils {

  static format(number, symbol, locale) {
    const options = {
      symbol: symbol ? symbol + ' ' : '',
      locale: locale || 'en-US',
      code: 'EUR'
    };

    return CurrencyFormatter.format(number, options);
  };

}

export default NumberUtils;
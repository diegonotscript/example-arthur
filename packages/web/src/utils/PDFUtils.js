import { Document, Page, StyleSheet, Text, Image, View } from '@react-pdf/renderer';
import React from 'react';
import OrderService from 'services/api/order/OrderService';
import LogoImg from 'app/images/logo.png';
import UserService from 'services/api/user/UserService';

const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    padding: 16,
    fontFamily: 'Helvetica',
  },
  logo: {
    width: '70%'
  },
  qrCode: {
    width: '200px'
  },
  header: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    border: '2px solid #000'
  },
  headerLeft: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    width: '100%'
  },
  headerRight: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'flex-end',
    alignContent: 'center',
  },
  content: {
    border: '2px solid #000',
    borderTop: 'none',
    marginTop: -1,
    padding: 22,
    flex: 1,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    border: '2px solid #000',
    borderTop: 'none',
    marginTop: -2,
    padding: 22
  },
  footerLeft: {
    flexDirection: 'column',
    width: '100%'
  },
  footerRight: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'flex-end',
    alignContent: 'center',
  },
  photo: {
    width: '200px',
    height: '200px',
    border: '2px solid #000'
  },
  website: {
    fontSize: 14
  },
  toFrom: {
    fontSize: 16,
    fontFamily: 'Helvetica-Bold',
    marginBottom: 6,
  },
  shippingName: {
    fontSize: 24,
    fontFamily: 'Helvetica-Bold',
  },
  orderNumber: {
    textAlign: 'right',
    fontFamily: 'Helvetica-Bold'
  },
  contactNumber: {
    fontFamily: 'Helvetica-Bold'
  },
  space: {
    marginVertical: 10
  }
});

const PageLabel = ({ order, authenticatedUser, extraIndex = undefined }) => {
  return <Page size="A4" style={styles.page}>

    <View style={styles.header}>
      <View style={styles.headerLeft}>
        <Image src={LogoImg} style={styles.logo} />
        <Text style={styles.website}>www.xpeedt.co.uk</Text>
      </View>

      <View style={styles.headerRight}>
        <Image style={styles.qrCode} src={order.qrCode} />
      </View>
    </View>

    <View style={styles.content}>
      <View style={styles.orderNumber}>
        <Text>ORDER NUMBER: {order.id} {extraIndex && ('/' + extraIndex)}</Text>
      </View>

      <View style={styles.space} />

      <Text style={styles.toFrom}>Shipping to</Text>

      <View style={styles.space} />

      <Text style={styles.shippingName}>{order.shippingName}</Text>
      <Text>{order.shippingAddress}</Text>

      <View style={styles.space} />

      <Text style={styles.contactNumber}>Contact Number</Text>
      <Text>{order.shippingPhone}</Text>
    </View>

    <View style={styles.footer}>
      <View style={styles.footerLeft}>
        <Text style={styles.toFrom}>From</Text>

        <View style={styles.space} />

        <Text style={styles.shippingName}>{authenticatedUser.name}</Text>
        <Text>{authenticatedUser.showWebsite && authenticatedUser.companyWebsite}</Text>
      </View>

      <View style={styles.footerRight}>
        {authenticatedUser.showLogo && <Image src={authenticatedUser.photoUrl} style={styles.photo} />}
      </View>
    </View>

  </Page>;
};

const LabelDocument = ({ order, authenticatedUser }) => {
  const extras = Array.from(Array(order.extraPackages).keys());

  return <>
    <Document>
      <PageLabel order={order} authenticatedUser={authenticatedUser} />
      {extras.map(extraIndex => (
        <PageLabel key={extraIndex}
          order={order}
          authenticatedUser={authenticatedUser}
          extraIndex={extraIndex + 1} />
      ))}
    </Document>
  </>
};

export default async function generateLabels(orderId) {

  const data = {
    name: '',
    component: '',
    extras: []
  }

  let authenticatedUser;

  await UserService().me()
    .then(response => {
      authenticatedUser = response.data;
    });

  await OrderService()
    .getById(orderId)
    .then(async (response) => {

      const order = response.data;
      const packageLabelName = `order-label-${orderId}`;

      data.name = `${packageLabelName}.pdf`;
      data.component = <LabelDocument order={order} authenticatedUser={authenticatedUser} />;
    })
    .catch(err => {
      console.log(err);
    });

  return data;
}

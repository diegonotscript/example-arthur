import React, { createContext, useEffect, useRef, useState } from 'react';

import Routes from '../config/Routes';
import { ThemeProvider } from '@material-ui/styles';
import AppTheme from './styles/themes/AppTheme'
import AuthProvider from '../config/AuthProvider';
import NavBar from './components/nav-bar/NavBar';
import { BrowserRouter } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import usePageLoader from './components/loader/PageLoader';

export const AppContext = createContext();
export const AppConsumer = AppContext.Consumer;
const Provider = AppContext.Provider;

function App() {
  const _isMounted = useRef(true);
  const [pageLoader, showPageLoader, hidePageLoader] = usePageLoader(true);
  const [loading, setLoading] = useState(true);

  const context = {
    showPageLoader,
    hidePageLoader
  };

  // initial page loader
  useEffect(() => {
    setTimeout(() => {
      hidePageLoader();
      setLoading(false);
    }, 1000);
    return () => {
      _isMounted.current = false;
    }
  }, [hidePageLoader])

  return (
    <Provider value={context}>
      <BrowserRouter>
        <ThemeProvider theme={AppTheme}>
          <SnackbarProvider maxSnack={3}>
            {pageLoader}
            {!loading &&
              <AuthProvider>
                <NavBar>
                  <Routes />
                </NavBar>
              </AuthProvider>
            }
          </SnackbarProvider>
        </ThemeProvider>
      </BrowserRouter>
    </Provider>
  );
};

export default App;

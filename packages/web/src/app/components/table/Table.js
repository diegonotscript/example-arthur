import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { Typography } from '@material-ui/core';
import Notification from '../notification/Notification';
import Button from '../button/Button';
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import { Box } from '@material-ui/core';

export const Table = forwardRef((props, ref) => {
  const _isMounted = useRef(true);
  const { notify } = Notification();

  const defaultLimit = 10;
  const defaultOffset = 0;

  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [limit, setLimit] = useState(defaultLimit);
  const [offset, setOffset] = useState(defaultOffset);
  const [filtering, setFiltering] = useState(false);
  const [filter, setFilter] = useState('');

  const service = props.service;
  const method = props.method;
  const onLoad = props.onLoad;
  const filtersRef = props.filtersRef || {};
  const initialFilter = props.initialFilter || '';
  const columns = props.columns || [];

  const [sortModel, setSortModel] = useState([
    { field: columns[0].field, sort: 'desc' },
  ]);

  useImperativeHandle(ref, () => ({
    refresh(filters) {
      loadItems(null, null, filters);
    }
  }));

  const getSort = () => {
    return sortModel[0].field + ',' + sortModel[0].sort
  }

  const updateFilters = (filters) => {
    setFilter(filters);
    if (filters) {
      setFiltering(true);
    } else {
      setFiltering(false);
    }
  }

  const loadItems = (limit = defaultLimit, offset = defaultOffset, filters = '') => {
    if (!_isMounted.current || props.hasLoadedItems) {
      return;
    }

    setLoading(true);
    setFiltering(false);
    let isFiltersEmpty = !filters;

    if (initialFilter && isFiltersEmpty) {
      filters = initialFilter;
    }

    updateFilters(filters);

    const params = {
      limit: limit,
      offset: offset,
      sort: getSort(),
      where: filters
    };
    /* 
        Object.keys(filters)
          .forEach(key => {
            let value = filters[key];
            if (value) {
              params[key] = filters[key];
              if (!isFiltersEmpty) {
                setFiltering(true);
              }
            }
          });
     */
    return service()[method](params)
      .then(response => {
        const data = response.data;
        if (!data) {
          return;
        }

        const content = data.content || [];

        onLoad(content);
        setItems(content);
      })
      .catch(err => {
        notify(err.message, 'error');
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    const abortController = new AbortController();
    if (props.hasLoadedItems) {
      setItems(props.rows);
      return;
    }
    loadItems();
    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  const handleClearFilters = () => {
    if (filtersRef.current) {
      filtersRef.current.clear();
    }
    loadItems();
  };

  const handleSortModelChange = (newModel) => {
    const hasChanged = newModel[0].field !== sortModel[0].field || newModel[0].sort !== sortModel[0].sort;
    if (_isMounted.current && hasChanged) {
      setSortModel(newModel);
      loadItems(null, null, filter);
    }
  };

  return (
    <>
      {
        (filtering && filtersRef?.current) &&
        <Box mb={2} className="textRight">
          <Button
            color="primary"
            variant="outlined"
            onClick={handleClearFilters}
            startIcon={<CloseOutlinedIcon />}
            size="small"
          >
            Clear filters
          </Button>
        </Box>
      }
      <DataGrid
        rows={(items && !loading) ? items : []}
        columns={columns}
        hideFooter="true"
        autoHeight="true"
        hideFooterPagination="true"
        loading={loading}
        rowHeight={props.rowHeight || undefined}
        disableColumnMenu
        components={{
          NoRowsOverlay: () => {
            return <div className="MuiDataGrid-overlay" style={{ top: '56px' }}>
              <Typography color="textSecondary" variant="h6" className="textCenter">
                No items found
              </Typography>
            </div>;
          },
        }}
        sortingOrder={['desc', 'asc']}
        sortModel={sortModel}
        onSortModelChange={handleSortModelChange}

      />
    </>
  );
});
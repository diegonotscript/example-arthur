import React, { useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Backdrop } from '@material-ui/core';
import { CircularProgress } from '@material-ui/core';
import PackageIcon from 'app/components/icons/PackageIcon';
import { grey } from "@material-ui/core/colors";
import { Grid } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  backdrop: {
    background: '#F36C2B',
    color: theme.palette.common.white,
    zIndex: 100
  },
  progress: {
    color: theme.palette.common.white,
  }
}));

const usePageLoader = (initialValue = false) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(initialValue);

  const showPageLoader = () => setLoading(true);
  const hidePageLoader = () => setTimeout(setLoading(false), 500);

  return [
    <Backdrop className={classes.backdrop} open={loading}>
      <Grid container
        direction="column"
        justifycontent="center"
        alignItems="center"
        spacing={2}
      >
        <Grid item>
          <PackageIcon color={grey[100]} style={{ fontSize: '10rem' }} strokeWidth="0.5" />
        </Grid>
        <Grid item>
          <CircularProgress className={classes.progress} />
        </Grid>
      </Grid>
    </Backdrop>,
    showPageLoader,
    hidePageLoader
  ];
};

export default usePageLoader;
import React from "react";
import { CircularProgress } from '@material-ui/core';
import { Grid } from '@material-ui/core';

export default function Loader(props) {
  return (
    <>
      <Grid container
        direction="column"
        justifycontent="center"
        alignItems="center"
      >
        <Grid item xs={12} className="textCenter">
          <CircularProgress {...props} />
        </Grid>
      </Grid>
    </>
  );
};
import React, { forwardRef } from 'react';
import { Box, Grid, Typography, useTheme } from '@material-ui/core';
import PackageIcon from '../icons/PackageIcon';

export default forwardRef((props, ref) => {
  const { message = 'Information not found', button, hideIcon = false } = props;
  const theme = useTheme();

  return (

    <Grid container
      direction="column"
      justifycontent="center"
      alignItems="center"
      className="textCenter"
      style={{ marginTop: '32px' }}
    >
      {!hideIcon &&
        <Grid item xs>
          <Box my={2}>
            <PackageIcon color={theme.palette.text.secondary} style={{ fontSize: '10rem' }} strokeWidth="0.5" />
          </Box>
        </Grid>
      }
      <Grid item xs>
        <Box>
          <Typography color="textSecondary" variant="h6">
            {message}
          </Typography>
        </Box>
      </Grid>
      {button &&
        <Grid item xs>
          <Box my={2}>
            {button}
          </Box>
        </Grid>
      }
    </Grid>
  );
});
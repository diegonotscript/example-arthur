import React from 'react';
import IconButtonComponent from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Tooltip } from '@material-ui/core';

export default function IconButton(props) {
  const { disabled, loading, tooltip, variant, size, ...rest } = props;

  const getClassIfContained = () => {
    if (variant === 'contained') {
      let className = 'MuiButton-contained MuiButton-disableElevation';
      if (rest.color === 'secondary') {
        return className + ' MuiButton-containedSecondary';
      } else {
        return className + ' MuiButton-containedPrimary';
      }
    }
  }

  const buttonElement = () => {
    return (
      <IconButtonComponent
        className={`${size === 'xs' ? 'MuiIconButton-sizeExtraSmall' : ''} ${getClassIfContained()}`}
        size={size === 'xs' ? 'small' : size}
        {...rest}
        disabled={disabled || loading}
      >
        {props.children} {loading && <CircularProgress size={24}
          color="secondary"
          style={{ position: 'absolute' }} />}
      </IconButtonComponent>
    );
  }

  if (tooltip) {
    return (
      <Tooltip title={tooltip}>
        <div>
          {buttonElement()}
        </div>
      </Tooltip>
    );
  }

  return buttonElement();
}
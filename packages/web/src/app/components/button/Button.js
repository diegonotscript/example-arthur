import React, { forwardRef } from 'react';
import ButtonComponent from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Tooltip } from '@material-ui/core';

export default forwardRef((props, ref) => {
  const { disabled, loading, tooltip, pdf, filename, ...rest } = props;

  if (tooltip) {
    return (
      <Tooltip title={tooltip}>
        <div className="flex">
          <ButtonComponent
            {...rest}
            disableElevation
            disabled={disabled || loading}
          >
            {props.children} {loading && <CircularProgress size={24}
              color="secondary"
              style={{ position: 'absolute' }} />}
          </ButtonComponent>
        </div>
      </Tooltip>
    );
  }

  return (
    <ButtonComponent
      {...rest}
      disableElevation
      disabled={disabled || loading}
    >
      {props.children} {loading && <CircularProgress size={24}
        color="secondary"
        style={{ position: 'absolute' }} />}
    </ButtonComponent>
  );
});
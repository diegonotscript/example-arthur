import React, { useEffect, useRef, useState } from 'react';
import { Breadcrumbs, Typography, Toolbar, IconButton, Grid } from '@material-ui/core';
import { NavigateNext, ArrowBack } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";

const useBreadcrumbStyles = makeStyles(theme => ({
  toolbar: {
    borderBottom: '2px solid rgb(0 0 0 / 8%)',
  }
}));

const EXCHANGE_PATHS = {
  'label': 'Shipping Label'
};

export default function Breadcrumb(props) {
  const classes = useBreadcrumbStyles();
  const _isMounted = useRef(true);

  const history = useHistory();
  const [paths, setPaths] = useState([]);

  const getName = (path) => {
    if (EXCHANGE_PATHS[path]) {
      return EXCHANGE_PATHS[path];
    }

    return path.charAt(0).toUpperCase() + path.slice(1)
  };

  useEffect(() => {
    setPaths([]);
    props.paths.forEach(p => {
      if (!p || !p.match(/[a-z]/i)) {
        return;
      }

      setPaths((prevPaths) => {
        return [
          ...prevPaths,
          getName(p)
        ]
      });
    });
    return () => {
      _isMounted.current = false;
    }
  }, [props.paths]);

  const handleGoBack = () => {
    history.push('/orders');
  };

  return (
    <Toolbar className={classes.toolbar}>
      <Grid container
        alignItems="center">
        <Grid item zeroMinWidth>
          <IconButton aria-label="go back" color="inherit" onClick={handleGoBack}>
            <ArrowBack />
          </IconButton>
        </Grid>

        <Grid item>
          <Breadcrumbs separator={<NavigateNext fontSize="small" />}
            aria-label="breadcrumb">
            <Typography>
              Home
            </Typography>
            {paths.map((path, index) => (
              <Typography key={index} color={index === (paths.length - 1) ? "secondary" : "inherit"}>
                {path}
              </Typography>
            ))}
          </Breadcrumbs>
        </Grid>
      </Grid>
    </Toolbar>
  );
}
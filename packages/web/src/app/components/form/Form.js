import React from 'react';
import { FormControlLabel, TextField, Checkbox, FormHelperText, Switch, Grid } from '@material-ui/core';
import MaterialUiPhoneNumber from 'material-ui-phone-number';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Field, getIn } from 'formik';
import CurrencyTextField from '@unicef/material-ui-currency-textfield';

const useFormStyles = makeStyles(() => ({
  footer: {
    borderTop: '2px solid rgb(0 0 0 / 8%)',
    "& .middle": {
      flexGrow: 1,
    },
    "& .last": {
      textAlign: 'right',
    },
  },
  footerFixed: {
    position: 'fixed',
    width: '100%',
    bottom: 0,
    left: 0,
    margin: 0,
    background: 'white'
  }
}));

export function FormField(props) {
  const { formik,
    subLabel,
    options = [],
    getOptionLabel,
    renderOption,
    loading,
    onChange,
    noOptionsText,
    onlyCountries = [],
    currencySymbol = '£',
    type = 'text',
    ...rest } = props;

  const name = props.name;
  const id = props.id || name;

  const handleChange = (e, value) => {
    formik.setFieldValue(name, value, true);
    if (onChange) {
      onChange(e, value);
    }
  }

  if (type === 'currency') {
    return (
      <Field
        component={CurrencyTextField}
        currencySymbol={currencySymbol}
        outputFormat="string"
        minimumValue="0"
        fullWidth
        variant="outlined"
        margin="normal"
        type="text"
        id={id}
        {...rest}
        value={getIn(formik.values, name) || ''}
        onChange={(e) => handleChange(e, e.target.value)}
        error={getIn(formik.touched, name) && Boolean(getIn(formik.errors, name))}
        helperText={getIn(formik.touched, name) && getIn(formik.errors, name)}
      />
    );
  }

  if (type === 'date') {
    return (
      <Field
        component={TextField}
        type="date"
        fullWidth
        variant="outlined"
        margin="normal"
        id={id}
        {...rest}
        value={getIn(formik.values, name) || ''}
        onChange={(e) => handleChange(e, e.target.value)}
        error={getIn(formik.touched, name) && Boolean(getIn(formik.errors, name))}
        helperText={getIn(formik.touched, name) && getIn(formik.errors, name)}
      />
    );
  }

  if (type === 'autoComplete') {
    return (
      <Autocomplete
        id={id}
        type={type}
        options={options}
        getOptionLabel={(option) => option && (option[getOptionLabel] || '')}
        renderOption={(option) => renderOption ? renderOption(option) : option[getOptionLabel] }
        getOptionSelected={(option) => option[getOptionLabel]}
        loading={loading}
        disabled={props.disabled}
        onChange={(e, v) => handleChange(e, v)}
        noOptionsText={noOptionsText}
        value={getIn(formik.values, name) || ''}
        disableClearable={props.required}
        renderInput={(params) => <FormField {...params}
          name={name}
          label={props.label}
          required={props.required}
          placeholder={props.placeholder}
          formik={formik}
          error={getIn(formik.touched, name) && Boolean(getIn(formik.errors, name))}
          helperText={getIn(formik.touched, name) && getIn(formik.errors, name)} />}
      />
    );
  }

  if (type === 'tel') {
    return (
      <Field
        component={MaterialUiPhoneNumber}
        fullWidth
        variant="outlined"
        margin="normal"
        id={id}
        type={type}
        {...rest}
        value={getIn(formik.values, name) || ''}
        onChange={(v) => handleChange(null, v)}
        error={getIn(formik.touched, name) && Boolean(getIn(formik.errors, name))}
        helperText={getIn(formik.touched, name) && getIn(formik.errors, name)}
        defaultCountry="gb"
        onlyCountries={onlyCountries}
        disableDropdown={true}
      />
    );
  }

  if (type === 'checkbox') {
    return (
      <>
        <FormControlLabel
          control={<Checkbox
            onChange={(e) => handleChange(e, e.target.checked)}
            name={name}
            checked={getIn(formik.values, name) || false} />
          }
          id={id}
          name={name}
          required={props.required}
          label={props.label}
        />
        {subLabel && <FormHelperText style={{ marginLeft: '32px', marginTop: '-4px' }}>{subLabel}</FormHelperText>}
      </>
    );
  }

  if (type === 'switch') {
    return (
      <>
        <FormControlLabel
          control={<Switch
            onChange={(e) => handleChange(e, e.target.checked)}
            name={name}
            checked={getIn(formik.values, name) || false} />
          }
          id={id}
          name={name}
          required={props.required}
          label={props.label}
        />
        {subLabel && <FormHelperText style={{ marginLeft: '48px', marginTop: '-4px' }}>{subLabel}</FormHelperText>}
      </>
    );
  }

  return (
    <Field
      component={TextField}
      fullWidth
      variant="outlined"
      margin="normal"
      id={id}
      type={type}
      {...rest}
      value={getIn(formik.values, name) || ''}
      onChange={(e) => handleChange(e, e.target.value)}
      error={getIn(formik.touched, name) && Boolean(getIn(formik.errors, name))}
      helperText={getIn(formik.touched, name) && getIn(formik.errors, name)}
    />
  );
};

export function FormFooter(props) {
  const classes = useFormStyles();
  const fixed = props.fixed;

  return (
    <Grid container
      direction="row"
      justifycontent="flex-start"
      alignItems="center"
      spacing={2}
      className={clsx(classes.footer, fixed && classes.footerFixed)}
    >
      {props.children}
    </Grid>
  );
}

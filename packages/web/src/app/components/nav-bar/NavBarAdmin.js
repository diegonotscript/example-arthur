import React, { useContext, useEffect, useRef, useState } from 'react';
import { AppBar, Toolbar, IconButton, Container, useMediaQuery, Popover } from '@material-ui/core';
import { HomeOutlined, LocalShippingOutlined } from '@material-ui/icons'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useHistory, useLocation } from 'react-router-dom';
import PackageIcon from '../icons/PackageIcon';
import { AuthContext } from 'config/AuthProvider';
import { Drawer } from '@material-ui/core';
import { Divider } from '@material-ui/core';
import { List } from '@material-ui/core';
import { ListItem } from '@material-ui/core';
import { ListItemIcon } from '@material-ui/core';
import { ListItemText } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import PersonOutlinedIcon from '@material-ui/icons/PersonOutlined';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import LabelOutlinedIcon from '@material-ui/icons/LabelOutlined';
import PaymentOutlinedIcon from '@material-ui/icons/PaymentOutlined';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';

import logo from 'app/images/logo.png';
import xpeedtLogo from 'app/images/xpeedt-logo.png';
import xpeedtName from 'app/images/xpeedt-name.png';

const drawerWidth = 100;

const useNavBarAdminStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    height: '100%',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(1),
    justifyContent: 'center',
  },
  list: {
    '& .MuiListItem-root': {
      flexDirection: 'column',
    },
    '& .MuiListItemIcon-root': {
      minWidth: 0
    }
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: 0,
    position: 'absolute',
  },
  hide: {
    display: 'none',
  },
  container: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    flex: 4
  },
  sectionDesktop: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  sectionMobile: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

const NavBarAdmin = (props) => {
  const _isMounted = useRef(true);
  const context = useContext(AuthContext);
  const history = useHistory();
  const location = useLocation();
  const theme = useTheme();

  const desktop = useMediaQuery(theme.breakpoints.up('md'));
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));

  const [open, setOpen] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);

  const PATHS = [
    { name: 'Home', path: '/home', icon: <HomeOutlined /> },
    { name: 'Orders', path: '/orders', icon: <LocalShippingOutlined /> },
    { name: 'Packages', path: '/packages', icon: <PackageIcon strokeWidth="1.5" color="rgba(0, 0, 0, 0.54)" /> },
    { name: 'Drivers', path: '/drivers', icon: <PeopleAltOutlinedIcon /> },
    { name: 'Customers', path: '/customers', icon: <PersonOutlinedIcon /> },
  ];

  const PATHS_SETTINGS = [
    { name: 'Users', path: '/users', icon: <PeopleAltOutlinedIcon /> },
    { name: 'Order status', path: '/order-status', icon: <LabelOutlinedIcon /> },
    { name: 'Payments methods', path: '/payments-methods', icon: <PaymentOutlinedIcon /> },
    { name: 'Global messages', path: '/global-messages', icon: <EmailOutlinedIcon /> },
    { name: 'Contact information', path: '/contact-information', icon: <InfoOutlinedIcon /> },
  ];

  const classes = useNavBarAdminStyles();
  const isAuthenticated = props.isAuthenticated;
  const children = props.children;

  useEffect(() => {
    if (desktop) {
      setOpen(true);
    } else {
      setOpen(false);
    }

    return () => {
      _isMounted.current = false;
    }
  }, [desktop, mobile]);

  useEffect(() => {
    if (!isAuthenticated) {
      return;
    }

    return () => {
      _isMounted.current = false;
    }
  }, [location.pathname]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const toggleDrawer = () => {
    setOpen(!open);
  };

  const handleGoTo = (path) => {
    handlePopoverClose();
    history.push(path);
  };

  const handleLogout = (e) => {
    context.finishSession();
  };

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const SubListSettings = () => {
    return (
      PATHS_SETTINGS.map((path, index) => (
        <ListItem button key={index} onClick={() => handleGoTo(path.path)}>
          <ListItemIcon>{path.icon}</ListItemIcon>
          <ListItemText primary={path.name} />
        </ListItem>
      ))
    );
  };

  if (!isAuthenticated) {
    return (children);
  }

  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant={mobile ? "temporary" : "persistent"}
        anchor="left"
        open={open}
        onClose={toggleDrawer}
      >
        <div className={classes.drawerHeader}>
          <img src={xpeedtLogo} alt="Xpeedt logo" />
          <img src={xpeedtName} alt="Xpeedt" />
        </div>

        <Divider />

        <List className={classes.list}>
          {PATHS.map((path, index) => (
            <ListItem button key={index} onClick={() => handleGoTo(path.path)}>
              <ListItemIcon>{path.icon}</ListItemIcon>
              <ListItemText primary={path.name} />
            </ListItem>
          ))}
          <ListItem button onClick={handlePopoverOpen}>
            <ListItemIcon><SettingsOutlinedIcon /></ListItemIcon>
            <ListItemText primary="Settings" />
          </ListItem>
          <ListItem button onClick={handleLogout}>
            <ListItemIcon><ExitToAppOutlinedIcon /></ListItemIcon>
            <ListItemText primary="Logout" />
          </ListItem>
        </List>
      </Drawer>

      <Popover
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handlePopoverClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <SubListSettings />
      </Popover>

      <AppBar position="fixed" color="default" elevation={0} className={classes.sectionMobile}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>

          <div className={classes.grow} style={{ textAlign: 'center' }}>
            <img src={logo} alt="Logo" style={{ height: '48px' }} />
          </div>

        </Toolbar>
      </AppBar>
      <Toolbar />

      <Container className={classes.container}>
        {children}
      </Container>
    </div>
  );
}

export default NavBarAdmin;
import React from 'react';
import { AuthConsumer } from 'config/AuthProvider';

import NavBarCustomer from './NavBarCustomer';
import NavBarAdmin from './NavBarAdmin';

const NavBarConsumer = ({ children }) => (
  <AuthConsumer>
    {
      (context) => (
        context.isAuthenticated ? (
          context.authenticatedUser.isAdmin ?
            <NavBarAdmin
              isAuthenticated={context.isAuthenticated}
              authenticatedUser={context.authenticatedUser}
              children={children} />
            :
            <NavBarCustomer
              isAuthenticated={context.isAuthenticated}
              authenticatedUser={context.authenticatedUser}
              children={children} />
        )
        :
        children
      )
    }
  </AuthConsumer>
);

export default NavBarConsumer;
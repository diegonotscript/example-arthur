import React, { useContext, useEffect, useRef, useState } from 'react';
import { AppBar, Toolbar, Button, IconButton, Badge, Avatar, BottomNavigation, BottomNavigationAction, Container } from '@material-ui/core';
import { ExitToAppOutlined, HomeOutlined, LocalShippingOutlined, MailOutlined, SettingsOutlined, StoreOutlined } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles';
import { useHistory, useLocation } from 'react-router-dom';
import clsx from 'clsx';
import Breadcrumb from '../breadcrumb/Breadcrumb';
import PackageIcon from '../icons/PackageIcon';
import { findIndex } from 'lodash';
import { Menu, MenuItem } from '@material-ui/core';
import { AuthContext } from 'config/AuthProvider';

import logo from 'app/images/logo.png';
import OrderService from 'services/api/order/OrderService';

const useNavBarCustomerStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.common.white,
    overflowY: 'auto',
    display: 'flex',
    flexFlow: 'column',
    height: '100%',
    [theme.breakpoints.down('sm')]: {
      height: 'calc(100% - 56px)'
    },
  },
  grow: {
    flexGrow: 1,
  },
  sectionDesktop: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  sectionMobile: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  appBar: {
    boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.25)'
  },
  navBottom: {
    position: 'fixed',
    bottom: 0,
    width: '100%',
    boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.25)'
  },
  container: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    flex: 4
  }
}));

const NavBarCustomer = (props) => {
  const _isMounted = useRef(true);
  const context = useContext(AuthContext);
  const history = useHistory();
  const location = useLocation();

  const [navValue, setNavValue] = useState(0);
  const [anchorEl, setAnchorEl] = useState(null);
  const [breadcrumbs, setBreadcrumbs] = useState([]);
  const [totalCart, setTotalCart] = useState(0);

  const PATHS = [
    { name: 'Home', path: '/orders', icon: <HomeOutlined /> },
    { name: 'New Order', path: '/orders/new', icon: <PackageIcon /> },
    { name: 'Messages', path: '/orders', icon: <MailOutlined /> },
  ];

  const classes = useNavBarCustomerStyles();
  const isAuthenticated = props.isAuthenticated;
  const authenticatedUser = props.authenticatedUser;
  const children = props.children;

  useEffect(() => {
    const arrayPath = location.pathname.split("/");

    setBreadcrumbs(arrayPath);

    const index = findIndex(PATHS, (p) => {
      return p.path === ("/" + arrayPath[1]);
    }, 0);

    setNavValue(index < 0 ? 0 : index);
    handleProfileMenuClose();

    return () => {
      _isMounted.current = false;
    }
  }, [location.pathname]);

  useEffect(() => {
    const abortController = new AbortController();

    OrderService()
      .getTotalCart()
      .then((response) => {
        setTotalCart(response.data || 0);
      });

    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  const handleGoTo = (path) => {
    history.push(path);
  };

  const handleLogout = (e) => {
    context.finishSession();
  };

  const handleProfileMenuClose = () => {
    setAnchorEl(null);
  };

  const handleProfileMenuOpen = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleChangeNav = (e, newValue) => {
    setNavValue(newValue);
    handleGoTo(PATHS[newValue].path);
  };

  const handleCart = () => {
    handleGoTo('/cart');
  };

  if (!isAuthenticated) {
    return (children);
  }

  return (
    <div className={classes.root}>
      <AppBar position="fixed" color="default" elevation={0} className={classes.appBar}>
        <Toolbar>
          <img src={logo} alt="Logo" style={{ height: '48px' }} className={classes.sectionDesktop} />

          <div className={classes.grow} style={{ textAlign: 'center' }}>
            <img src={logo} alt="Logo" style={{ height: '48px' }} className={classes.sectionMobile} />
          </div>

          <div className={classes.sectionDesktop}>
            {PATHS.map((path, index) => (
              <Button key={index}
                color="inherit"
                onClick={(e) => handleChangeNav(e, index)}>
                {path.name}
              </Button>
            ))}
            <Button color="inherit" onClick={handleProfileMenuOpen}
              endIcon={<Avatar src={`${authenticatedUser?.photoUrl}`} className="smallAvatar" />}
            >
              Profile
            </Button>
          </div>

          <IconButton aria-label={`show ${totalCart} new notifications`}
            color="inherit"
            onClick={handleCart}>
            <Badge badgeContent={totalCart} color="secondary"
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}>
              <LocalShippingOutlined />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
      <Toolbar />

      {breadcrumbs.length > 1 && <Breadcrumb paths={breadcrumbs} />}

      <Container className={classes.container}>
        {children}
      </Container>

      <BottomNavigation
        value={navValue}
        onChange={handleChangeNav}
        showLabels
        className={clsx(classes.sectionMobile, classes.navBottom)}
      >
        <BottomNavigationAction label="Home" icon={<HomeOutlined />} />
        <BottomNavigationAction label="New Order" icon={<PackageIcon />} />
        <BottomNavigationAction label="Messages" icon={<MailOutlined />} />
        <BottomNavigationAction label="Profile" icon={<StoreOutlined />} />
      </BottomNavigation>

      <Menu
        anchorEl={anchorEl || null}
        id="primary-profile-menu"
        /* keepMounted */
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorEl)}
        onClose={handleProfileMenuClose}
        getContentAnchorEl={null}
      >
        <MenuItem onClick={() => handleGoTo('/settings')} style={{ width: '200px' }}><SettingsOutlined style={{ marginRight: 16 }} /> Settings</MenuItem>
        <MenuItem onClick={handleLogout}><ExitToAppOutlined style={{ marginRight: 16 }} />Logout</MenuItem>
      </Menu>
    </div>
  );
}

export default NavBarCustomer;
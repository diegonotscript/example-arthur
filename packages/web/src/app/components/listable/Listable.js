import React, { useState, useEffect, useImperativeHandle, forwardRef, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import { Radio, Typography } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import Skeleton from "@material-ui/lab/Skeleton";
import { green } from "@material-ui/core/colors";
import clsx from "clsx";
import IconButton from "../button/IconButton";
import Notification from "../notification/Notification";
import EmptyState from 'app/components/empty-state/EmptyState';

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  item: {
    border: '1px solid rgba(224, 224, 224, 1)',
    borderRadius: '4px',
    marginBottom: theme.spacing(1)
  },
  itemSelected: {
    backgroundColor: green[100],
    '&:hover': {
      backgroundColor: green[100],
    }
  },
  skeleton: {
    margin: theme.spacing(1, 0),
  },
  pagination: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: theme.spacing(1, 0),
    '& nav': {
      marginLeft: theme.spacing(2)
    },
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      justifyContent: 'center',
      '& nav': {
        marginLeft: 0,
        marginTop: theme.spacing(2)
      },
    },
  },
  hideOnMobile: {
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    },
  }
}));

const ListItemSkeleton = () => {
  const classes = useStyles();
  return (
    [0, 1, 2, 3, 4].map((index) => (
      <Skeleton key={index} variant="rect" width="100%" height="50px" className={classes.skeleton}></Skeleton>
    ))
  )
}

export const Listable = forwardRef((props, ref) => {
  const { notify } = Notification();
  const classes = useStyles();
  const _isMounted = useRef(true);

  const defaultLimit = 10;
  const defaultOffset = 0;

  const checkable = props.checkable || false;
  const multiple = props.multiple || false;
  const dense = props.dense || false;
  const hideStartIconOnMobile = props.hideStartIconOnMobile || false;
  const endAction = props.endAction;
  const onToggle = props.onToggle;
  const startIcon = props.startIcon || null;
  const loadedItems = props.loadedItems || [];
  const searchRef = props.searchRef || {};
  const initialFilter = props.initialFilter || '';

  const [checked, setChecked] = useState(multiple ? [] : null);

  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [firstLoading, setFirstLoading] = useState(true);
  const [total, setTotal] = useState(0);
  const [showing, setShowing] = useState(0);
  const [countPages, setCountPages] = useState(0);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(defaultLimit);
  const [offset, setOffset] = useState(defaultOffset);
  const [loadingMore, setLoadingMore] = useState(false);
  const [filtering, setFiltering] = useState(false);

  const service = props.service;
  const method = props.method;
  const onLoad = props.onLoad;

  const handleChange = (event, value) => {
    if (value === page) {
      return;
    }

    setLimit(defaultLimit * value);
    setOffset((defaultLimit * value) - defaultLimit);
    setPage(value);
  };

  useEffect(() => {
    if (checked !== null) {
      return;
    }
    handleToggle(props.checked);
    return () => {
      _isMounted.current = false;
    }
  }, [props.checked]);

  useEffect(() => {
    if (props.hasLoadedItems) {
      return;
    }
    loadItems();
    return () => {
      _isMounted.current = false;
    }
  }, [page]);

  useEffect(() => {
    if (!props.hasLoadedItems) {
      return;
    }
    setLoadedItems();
    return () => {
      _isMounted.current = false;
    }
  }, [props.loadedItems]);

  useImperativeHandle(ref, () => ({
    loadMore(newLimit) {
      if (!newLimit) {
        newLimit = defaultLimit + limit;
      }
      setLimit(newLimit);
      setLoadingMore(true);
      loadItems(newLimit);
    },
    refresh(filters) {
      loadItems(null, null, filters);
    },
    handleCheck(index) {
      handleToggle(index);
    }
  }));

  const setLoadedItems = () => {
    onLoad(loadedItems);
    setItems(loadedItems);
    setFirstLoading(false);
  }

  const updateFilters = (filters, isFiltersEmpty) => {
    if (filters && !isFiltersEmpty) {
      setFiltering(true);
      if (searchRef.current) {
        searchRef.current.filtering(true);
      }
    } else {
      setFiltering(false);
      if (searchRef.current) {
        searchRef.current.filtering(false);
      }
    }
  }

  const loadItems = (limit = defaultLimit, offset = defaultOffset, filters = '') => {
    setLoading(true);
    setFiltering(false);
    let isFiltersEmpty = !filters;

    if (initialFilter && isFiltersEmpty) {
      filters = initialFilter;
    }
    
    updateFilters(filters, isFiltersEmpty);

    const params = {
      limit: limit,
      offset: offset,
      where: filters
    };

    return service()[method](params)
      .then(response => {
        const data = response.data;
        if (!data) {
          return;
        }

        const content = data.content || [];
        const pageContent = data.page || 1;
        const totalContent = content.length;
        const totalResponse = data.total || totalContent;

        onLoad(content);

        setItems(content);
        setTotal(totalResponse);
        setPage(pageContent);
        setShowing(Number(totalContent));
        setCountPages(Math.ceil(totalResponse / defaultLimit) || 1);
      })
      .catch(err => {
        notify(err.message, 'error');
      })
      .finally(() => {
        setLoading(false);
        setFirstLoading(false);
        setLoadingMore(false);
      });
  };

  const handleToggle = (value) => {
    let newChecked = value;
    if (multiple) {
      const currentIndex = checked.indexOf(value);
      newChecked = [...checked];

      if (currentIndex === -1) {
        newChecked.push(value);
      } else {
        newChecked.splice(currentIndex, 1);
      }
    }

    setChecked(newChecked);
  };

  const handleClick = (children, index) => {
    if (checkable) {
      handleToggle(index);
      onToggle(index);
      return;
    }

    children.props.endAction && children.props.endAction.props.onClick();
  };

  const handleClearFilters = (e) => {
    setFirstLoading(true);
    searchRef?.current?.clear();
    loadItems();
  };

  const isItemSelected = (index) => {
    if (multiple) {
      return checked.indexOf(index) !== -1;
    }

    return checked === index;
  }

  const emptyState = () => {
    return <EmptyState hideIcon={true} message="No items found" />
  }

  const getEmptyState = () => {
    if (total === 0) {
      return props.emptyState || emptyState();
    }

    return emptyState();
  }

  return (
    <>
      {firstLoading ?
        <ListItemSkeleton />
        :
        (items.length === 0 ?
          getEmptyState()
          :
          <>
            {(loading && !loadingMore) ?
              <ListItemSkeleton />
              :
              <>
                <List className={classes.root}>
                  {props.children.map((children, index) => {
                    const labelId = `checkbox-list-label-${index}`;

                    return (
                      <ListItem
                        className={clsx(classes.item, isItemSelected(index) && classes.itemSelected)}
                        key={index}
                        role={undefined}
                        button
                        dense={dense}
                        onClick={() => handleClick(children, index)}
                      >
                        {(checkable || multiple || startIcon) &&
                          <ListItemIcon className={hideStartIconOnMobile ? classes.hideOnMobile : ''}>
                            {(checkable && !multiple) &&
                              <Radio
                                edge="start"
                                checked={checked === index}
                                inputProps={{ "aria-labelledby": labelId }}
                              />
                            }
                            {(checkable && multiple) &&
                              <Checkbox
                                edge="start"
                                checked={checked.indexOf(index) !== -1}
                                inputProps={{ "aria-labelledby": labelId }}
                              />
                            }
                            {(!checkable && !multiple && startIcon) && startIcon}
                          </ListItemIcon>
                        }

                        <ListItemText id={labelId}>
                          {children}
                        </ListItemText>

                        {endAction &&
                          <ListItemSecondaryAction>
                            <IconButton edge="end" size="xs" aria-label={endAction.label}
                              tooltip={endAction.label}
                              onClick={(e) => endAction.onClick(e, index)}>
                              {endAction.icon}
                            </IconButton>
                          </ListItemSecondaryAction>
                        }
                      </ListItem>
                    );
                  })}
                </List>
              </>
            }

            {loadingMore && <ListItemSkeleton />}

            {props.hasPagination &&
              <div className={classes.pagination}>
                <Typography color="primary" variant="body2">
                  Showing {showing} of {total} results
                </Typography>

                <Pagination
                  count={countPages}
                  showFirstButton
                  showLastButton
                  onChange={handleChange} />
              </div>
            }
          </>
        )
      }
    </>
  );
});

import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { Grid, TextField, Box, InputAdornment, useMediaQuery } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { SearchOutlined } from '@material-ui/icons';
import Button from '../button/Button';
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';

const useStyles = makeStyles(theme => ({
  box: {
    background: '#0e1f3808'
  },
  grid: {
    flexWrap: 'nowrap'
  },
  button: {
    marginLeft: theme.spacing(2)
  }
}));

export function SearchInput(props) {
  return (
    <TextField
      variant="outlined"
      id={props.name}
      name={props.name}
      value={props.value || ''}
      placeholder={props.placeholder}
      fullWidth={props.fullWidth}
      onChange={props.onChange || undefined}
      type={props.type || 'text'}
      size="small"
      InputProps={{
        startAdornment:
          <InputAdornment position="start">
            <SearchOutlined />
          </InputAdornment>
      }}
    />
  )
};

export default forwardRef((props, ref) => {
  const classes = useStyles();
  const onSearch = props.onSearch;
  const enableClear = props.enableClear;
  const listRef = props.listRef;
  const [value, setValue] = useState("");
  const [filtering, setFiltering] = useState(false);

  const handleChangeInput = (e) => {
    setValue(e.target.value);
  };

  const handleSearch = (e) => {
    setFiltering(value.trim() !== '');
    if (onSearch) {
      onSearch(e, value);
    }
  };

  const handleClearInput = () => {
    setValue("");
    setFiltering(false);
    if (onSearch) {
      onSearch();
    }
  };

  useImperativeHandle(ref, () => ({
    clear() {
      setValue("");
      setFiltering(false);
    },
    filtering(value) {
      setFiltering(value);
    }
  }));

  return (
    <Box p={2} my={2} className={classes.box}>
      <Grid container
        justifycontent="center"
        alignItems="center"
        className={classes.grid}>

        <SearchInput {...props}
          value={value}
          onChange={handleChangeInput} />

        <Button
          type="submit"
          variant="contained"
          color="secondary"
          className={classes.button}
          onClick={handleSearch}
        >
          Search
        </Button>

        {(filtering && enableClear) &&
          <Button
            color="primary"
            className={classes.button}
            onClick={handleClearInput}
            startIcon={<CloseOutlinedIcon />}
          >
            Clear
          </Button>}
      </Grid>
    </Box>
  )
});

export const Filters = forwardRef((props, ref) => {
  const classes = useStyles();
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));
  const formik = props.formik;

  useImperativeHandle(ref, () => ({
    clear() {
      formik.resetForm();
    }
  }));

  return (
    <Box p={2} my={2} className={classes.box}>
      <Grid container
        direction="column"
        className={classes.grid}>

        <Grid item md={12}>
          {props.children}
        </Grid>

        <Grid item md={12} className="textRight" style={{ marginTop: theme.spacing(2) }}>
          <Button
            type="submit"
            variant="contained"
            color="secondary"
            fullWidth={mobile}
          >
            Search
          </Button>
        </Grid>

      </Grid>
    </Box>
  )
});
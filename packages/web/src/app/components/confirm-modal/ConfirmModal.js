import React, { forwardRef } from 'react';
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import Button from '../button/Button';
import IconButton from '../button/IconButton';
import CloseIcon from '@material-ui/icons/Close';

export default forwardRef((props, ref) => {
  const { title, children, onCancel, onConfirm, open, maxWidth = 'xs' } = props;

  return (
    <Dialog onClose={onCancel} aria-labelledby="confirm-dialog"
      open={open}
      maxWidth={maxWidth}
      fullWidth={true}
      scroll="paper"
    >
      {title &&
        <DialogTitle id="confirm-dialog-title">
          {title}
          <IconButton aria-label="close" className="MuiDialogClose" onClick={onCancel}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
      }
      <DialogContent dividers={true}>
        {children}
      </DialogContent>
      <DialogActions>
        <Button
          color="primary"
          onClick={onCancel}>
          Cancel
        </Button>
        <Button
          color="secondary"
          variant="contained"
          onClick={onConfirm}>
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  );
});
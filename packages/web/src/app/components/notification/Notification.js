import { useSnackbar } from 'notistack';
import { isEmpty } from 'lodash';
import { Close } from '@material-ui/icons';
import IconButton from '../button/IconButton';

const TYPES = {
  DEFAULT: "default",
  INFO: "info",
  ERROR: "error",
  WARNING: "warning",
  SUCCESS: "success",
};

export default function Notification() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const action = (key) => (
    <>
      <IconButton onClick={() => closeSnackbar(key)}>
        <Close fontSize="small" style={{color: "#fff"}} />
      </IconButton>
    </>
  );

  const notify = (message, type) => {
    if (isEmpty(message)) {
      return;
    }

    enqueueSnackbar(message, {
      variant: type || TYPES.INFO,
      preventDuplicate: true,
      action
    });
  };

  return { notify };

}
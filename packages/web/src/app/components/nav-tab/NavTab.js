import { Tabs, Tab, Badge } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useNavTabStyles = makeStyles(theme => ({
  badge: {
    "& .MuiBadge-badge:not(.MuiBadge-invisible)": {
      position: 'initial',
      marginLeft: theme.spacing(1),
      transform: 'none'
    }
  }
}));

export default function NavTab(props) {
  const classes = useNavTabStyles();

  return (
    <Tabs
      value={props.value}
      onChange={props.onChange}
      className={props.className}
      aria-label="tabs">
      {props.options.map((option, index) => (
        <Tab key={index} label={
          <Badge color="secondary" 
            badgeContent={option.total}
            className={classes.badge}>
            {option.label}
          </Badge>
        } />
      ))}
    </Tabs>
  );
}
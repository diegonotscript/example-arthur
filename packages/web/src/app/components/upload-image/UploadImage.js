import React, { useCallback, useState } from 'react';
import { Button, FormHelperText, Grid, IconButton } from '@material-ui/core';
import { CameraAltOutlined, Close, PublishOutlined } from '@material-ui/icons';
import useButtonStyles from 'app/styles/themes/button/ButtonTheme';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';
import Cropper from 'react-easy-crop';
import ConfirmModal from '../confirm-modal/ConfirmModal';
import getCroppedImg from 'utils/CropUtils';

const useUploadImageStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(4, 0),
    textAlign: 'center',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  div__image: {
    width: '250px',
    height: '250px',
    backgroundColor: '#F8F8F8',
    borderRadius: '5px',
    display: 'flex',
    alignItems: 'center',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    margin: theme.spacing(3, 'auto'),
    marginTop: 0,
    position: 'relative',
    "& p": {
      width: '100%',
      textAlign: 'center',
      margin: theme.spacing(3, 3),
    },
    "& button": {
      position: 'absolute',
      top: theme.spacing(1),
      right: theme.spacing(1),
    }
  },
  div__crop: {
    position: 'relative',
    width: '100%',
    height: '400px'
  }
}));

export default function UploadImage(props) {
  const classes = useUploadImageStyles();
  const btnClasses = useButtonStyles();

  const [loading, setLoading] = useState(false);

  const [crop, setCrop] = useState({ x: 0, y: 0 })
  const [zoom, setZoom] = useState(1)
  const [openCrop, setOpenCrop] = useState(false);
  const [croppedImg, setCroppedImg] = useState(null);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);

  const formik = props.formik;
  const name = props.name;
  const helpText = props.helpText;
  const hideUseCamera = props.hideUseCamera;
  const cropShape = props.cropShape || 'round';

  const accept = 'image/png,image/jpeg,iage/jpg';

  const onCropComplete = useCallback((croppedArea, areaPixels) => {
    setCroppedAreaPixels(areaPixels);
  }, []);

  const handleChange = (e) => {
    formik.handleChange({ target: { name: name, files: [] } });

    setLoading(true);
    setOpenCrop(true);
    setCroppedImg(String(URL.createObjectURL(e.target.files[0])));

    e.target.value = '';
    e.target.files = undefined;

    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }

  const handleRemove = () => {
    setOpenCrop(false);
    setCroppedImg(null);
    formik.handleChange({ target: { name: name, files: [] } });
  }

  const onConfirmCrop = async () => {
    const croppedImage = await getCroppedImg(
      croppedImg,
      croppedAreaPixels,
      true
    );
    setOpenCrop(false);
    setCroppedImg(null);
    formik.handleChange({ target: { name: name, value: croppedImage } });
  }

  return (
    <div className={classes.root}>

      <ConfirmModal
        open={openCrop}
        onCancel={handleRemove}
        onConfirm={onConfirmCrop}
        maxWidth="md"
      >
        <div className={classes.div__crop}>
          <Cropper
            image={croppedImg}
            crop={crop}
            zoom={zoom}
            aspect={1 / 1}
            onCropChange={setCrop}
            onCropComplete={onCropComplete}
            onZoomChange={setZoom}
            cropShape={cropShape}
          />
        </div>
      </ConfirmModal>

      <Grid container
        justifycontent="center"
        alignItems="center"
      >
        <Grid item xs={12} md={12}>
          <div className={classes.div__image} style={{ backgroundImage: 'url(' + String(formik.values[name]) + ')' }}>
            {(!formik.values[name] && loading) && <Skeleton variant="rect" width="100%" height="100%" />}

            {(helpText && !loading && !formik.values[name]) && <FormHelperText>{helpText}</FormHelperText>}

            {(formik.values[name] && !loading) &&
              <IconButton
                className={btnClasses.danger}
                aria-label="remove image"
                onClick={handleRemove}
                title="Remove image"
                size="small"
              >
                <Close fontSize="small" />
              </IconButton>
            }
          </div>

        </Grid>

        {!hideUseCamera &&
          <Grid item xs={6}>
            <input
              accept={accept}
              id="contained-button-camera"
              type="file"
              onChange={(e) => handleChange(e)}
              name={name}
              hidden
            />
            <label htmlFor="contained-button-camera">
              <Button variant="outlined"
                color="primary"
                component="span"
                endIcon={<CameraAltOutlined />}>
                Camera
              </Button>
            </label>
          </Grid>
        }

        <Grid item xs={!hideUseCamera ? 6 : 12}>
          <input
            accept={accept}
            id="outlined-button-upload"
            type="file"
            onChange={(e) => handleChange(e)}
            name={name}
            hidden
          />
          <label htmlFor="outlined-button-upload">
            <Button variant="outlined"
              color="primary"
              component="span"
              endIcon={<PublishOutlined />}>
              Upload
            </Button>
          </label>
        </Grid>
      </Grid>

    </div>
  );
}
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  success: {
    color: theme.palette.common.white,
    borderColor: theme.palette.success.main,
    backgroundColor: theme.palette.success.main,
    '&:hover': {
      backgroundColor: theme.palette.success.dark,
    }
  },
  danger: {
    color: theme.palette.common.white,
    borderColor: theme.palette.error.main,
    backgroundColor: theme.palette.error.main,
    '&:hover': {
      backgroundColor: theme.palette.error.dark,
    }
  },
  dangerOutlined: {
    color: theme.palette.error.main,
    borderColor: theme.palette.common.white,
    backgroundColor: theme.palette.common.white,
    '&:hover': {
      backgroundColor: '#f48f8f2b',
    }
  }
}));

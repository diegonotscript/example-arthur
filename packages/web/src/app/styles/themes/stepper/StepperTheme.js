import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles, createStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Check from '@material-ui/icons/Check';
import StepConnector from '@material-ui/core/StepConnector';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const useColorlibStepIconStyles = makeStyles((theme) => createStyles({
  root: {
    zIndex: 1,
    color: '#fff',
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.primary.main
  },
  active: {
    backgroundColor: theme.palette.secondary.main,
    [theme.breakpoints.down('xs')]: {
      '&::before': {
        content: '""',
        width: 0,
        height: 0,
        borderLeft: '10px solid transparent',
        borderRight: '10px solid transparent',
        borderBottom: '10px solid #fff',
        position: 'absolute',
        bottom: '-14px'
      }
    },
  },
  completed: {
    backgroundColor: theme.palette.secondary.main
  },
}));

export function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {completed ? <Check /> : <FiberManualRecordIcon />}
    </div>
  );
}

ColorlibStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.node,
};

export const ColorlibConnector = withStyles(theme => ({
  root: {
    [theme.breakpoints.up('sm')]: {
      height: '100%',
      display: 'inline-grid',
      marginLeft: '7px',
      padding: '8px 0px',
    },
  },
  alternativeLabel: {
    top: 10,
  },
  active: {
    '& $line': {
      backgroundColor: theme.palette.secondary.main,
    },
  },
  completed: {
    '& $line': {
      backgroundColor: theme.palette.secondary.main,
    },
  },
  line: {
    [theme.breakpoints.up('sm')]: {
      width: 10,
      backgroundColor: '#C4C4C4',
    },
    border: 0,
    borderRadius: 1,
    [theme.breakpoints.down('xs')]: {
      height: 4,
      backgroundColor: '#fff',
    },
  },
}))(StepConnector);

import { createMuiTheme } from '@material-ui/core/styles';
import 'app/styles/fonts.css';

export default createMuiTheme({
  typography: {
    fontFamily: `"Roboto", sans-serif`,
    fontWeightLight: 400,
    fontWeightRegular: 500,
    fontWeightMedium: 600,
  },
  palette: {
    primary: {
      main: '#0F1F38',
      contrastText: '#fff'
    },
    secondary: {
      main: '#F36C2B',
      contrastText: '#fff'
    },
  },
});
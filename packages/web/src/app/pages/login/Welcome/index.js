import React from 'react';
import Button from '@material-ui/core/Button';

export default function Welcome(props) {
  const classes = props.classes;
  const onSignIn = props.onSignIn;
  const onSignUp = props.onSignUp;

  return (
    <>
      <Button
        type="button"
        fullWidth
        variant="contained"
        color="secondary"
        className={classes.button}
        onClick={onSignUp}
      >
        Registration with Email
      </Button>

      <Button
        type="button"
        fullWidth
        color="primary"
        className={classes.button}
        onClick={onSignIn}
      >
        Login
      </Button>
    </>
  );
}
import React, { useContext } from 'react';
import { InputAdornment, Link, Grid, Typography } from '@material-ui/core';
import { EmailOutlined, LockOutlined } from '@material-ui/icons/';
import { object, string } from 'yup';
import { AuthContext } from 'config/AuthProvider';
import { FormField } from 'app/components/form/Form';
import { Formik, Form } from 'formik';
import Button from 'app/components/button/Button';
import Notification from 'app/components/notification/Notification';
import { useHistory } from 'react-router-dom';

const VALIDATION = object().shape({
  email: string().email('Invalid email address').required('Required'),
  password: string().required('Required'),
});

export default function SignIn(props) {
  const authContext = useContext(AuthContext);
  const history = useHistory();
  const { notify } = Notification();

  const classes = props.classes;
  const onSignUp = props.onSignUp;
  const onRecoverPassword = props.onRecoverPassword;

  const initialValues = {
    email: '',
    password: '',
  };

  const handleSubmit = (values, { setSubmitting }) => {
    authContext.startSession(values)
      .then(() => {
        history.push('/orders');
      })
      .catch(err => {
        notify(err.message, 'error');
        setSubmitting(false);
      });
  };

  return (
    <div className={classes.div__root}>

      <div style={{ textAlign: 'left', width: '100%' }}>
        <Typography color="primary" variant="h6">
          Login
        </Typography>
        <Typography color="textSecondary" variant="body2">
          Log in and start sending your products
        </Typography>
      </div>

      <Formik initialValues={initialValues}
        validationSchema={VALIDATION}
        onSubmit={handleSubmit}>
        {(formik) => (
          <Form noValidate>
            <FormField
              required
              label="Email address"
              name="email"
              autoComplete="email"
              autoFocus
              formik={formik}
              InputProps={{
                startAdornment:
                  <InputAdornment position="start">
                    <EmailOutlined />
                  </InputAdornment>
              }}
            />

            <FormField
              required
              label="Password"
              name="password"
              type="password"
              autoComplete="password"
              formik={formik}
              InputProps={{
                startAdornment:
                  <InputAdornment position="start">
                    <LockOutlined />
                  </InputAdornment>
              }}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.button}
              loading={formik.isSubmitting}
            >
              Login
            </Button>
          </Form>
        )}
      </Formik>

      <Grid container>
        <Grid item xs>
          <Link href="#" color="primary" onClick={onSignUp} className="strong">
            Create an account
          </Link>
        </Grid>
        <Grid item>
          <Link href="#" color="primary" onClick={onRecoverPassword} className="strong">
            I forgot the password
          </Link>
        </Grid>
      </Grid>
    </div>
  );
}
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useHistory, withRouter } from 'react-router-dom';
import {
  CssBaseline, Paper, Grid, Typography, Hidden, CircularProgress,
  Breadcrumbs,
  makeStyles
} from '@material-ui/core';
import NavigateNext from '@material-ui/icons/NavigateNext';
import SignIn from '../SignIn';
import SignUp from '../SignUp';
import logo from 'app/images/logo.png';
import Welcome from '../Welcome';
import Storage from 'config/Storage';
import RecoverPassword from '../RecoverPassword';
import loginWideImage from 'app/images/login_wide.png';
import { AuthContext } from 'config/AuthProvider';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: `url(${loginWideImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.primary.main,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundBlendMode: 'multiply',
    position: 'relative'
  },
  image__mobile: {
    backgroundImage: `url(${loginWideImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.primary.main,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundBlendMode: 'multiply',
    position: 'relative',
    height: '250px',
    borderBottom: '4px solid ' + theme.palette.secondary.main,
  },
  header__create_acount__mobile: {
    backgroundColor: '#CCCCCC',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(4, 2),
    paddingBottom: theme.spacing(8),
  },
  image__typo: {
    position: 'absolute',
    margin: 'auto',
    top: '0px',
    bottom: '0px',
    left: '16%',
    height: 'fit-content',
    width: '60%',
  },
  grid__form: {
    minHeight: 'calc(100% - 250px)',
    position: 'relative',
  },
  paper: {
    margin: theme.spacing(8, 6),
    [theme.breakpoints.down('xs')]: {
      margin: theme.spacing(4, 2),
      position: 'relative',
    },
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  logo: {
    margin: theme.spacing(4, 1)
  },
  logo__mobile: {
    margin: 'auto',
    zIndex: 1,
    backgroundColor: '#fff',
    border: '4px solid ' + theme.palette.secondary.main,
    borderRadius: '4px 4px 0px 0px',
    borderBottomColor: 'transparent',
    position: 'absolute',
    bottom: '-4px',
    left: '0px',
    right: '0px',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(3, 0, 2),
    padding: theme.spacing(1, 1)
  },
  stepper: {
    [theme.breakpoints.up('sm')]: {
      left: '-12px',
      backgroundColor: 'transparent',
      margin: 'auto',
      top: '0px',
      padding: '0px',
      position: 'absolute',
      bottom: '0px',
      height: '100%',
      minHeight: '200px',
      maxHeight: '400px',
    },
    [theme.breakpoints.down('xs')]: {
      paddingTop: '0px',
      position: 'absolute',
      background: 'transparent',
      width: '100%',
      marginTop: '-70px',
    },
  },
  breadcrumbs: {
    position: 'absolute',
    margin: 'auto',
    top: theme.spacing(8),
    left: theme.spacing(6),
    [theme.breakpoints.down('sm')]: {
      top: theme.spacing(4),
      left: theme.spacing(2),
    },
    height: 'fit-content',
    width: 'fit-content',
    zIndex: 1,
    color: '#fff'
  },
  div__root: {
    width: '80%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  }
}));

const FRAMES = {
  WELCOME: 1,
  SIGN_IN: 2,
  SIGN_UP: 3,
  RECOVER_PASSWORD: 4
};

export default withRouter(function Login(props) {
  const classes = useStyles();
  const authContext = useContext(AuthContext);
  const history = useHistory();
  const _isMounted = useRef(true);

  const pathName = props.location.pathname;
  const isPathSignUp = Boolean(pathName.match('/sign-up'));
  const isPathRecoverPassword = Boolean(pathName.match('/recover-password'));
  const isLogout = Boolean(pathName.match('/logout'));
  const isPathSignIn = !isPathSignUp && !isPathRecoverPassword;
  const hasBeenWelcomed = Storage.getHasBeenWelcomed();
  let initialFrame = FRAMES.WELCOME;

  if (hasBeenWelcomed) {
    if (isPathSignIn) {
      initialFrame = FRAMES.SIGN_IN;
    }
    if (isPathSignUp) {
      initialFrame = FRAMES.SIGN_UP;
    }
    if (isPathRecoverPassword) {
      initialFrame = FRAMES.RECOVER_PASSWORD;
    }
  }

  const [loading, setLoading] = useState(true);
  const [frame, setFrame] = useState(initialFrame);

  const handleSignIn = (e) => {
    if (!hasBeenWelcomed) {
      Storage.setHasBeenWelcomed();
    }

    e.stopPropagation();
    e.preventDefault();

    history.push('/login');
  };

  const handleSignUp = (e) => {
    if (!hasBeenWelcomed) {
      Storage.setHasBeenWelcomed();
    }

    e.stopPropagation();
    e.preventDefault();

    history.push('/sign-up');
  };

  const handleRecoverPassword = (e) => {
    e.stopPropagation();
    e.preventDefault();
    history.push('/recover-password');
  };

  setTimeout(() => {
    setLoading(false);
  }, 1000);

  return (
    <Grid container component="main" className={classes.root}>

      <CssBaseline />

      <Hidden smUp>
        {frame !== FRAMES.SIGN_UP ?
          <Grid xs={12} sm={12} item className={classes.image__mobile}>
            <img src={logo} alt="Logo" className={classes.logo__mobile} />
          </Grid>
          :
          <Grid xs={12} sm={12} item className={classes.header__create_acount__mobile}>
            <img src={logo} alt="Logo" />
            <Typography color="primary" variant="h6">
              Create an account
            </Typography>
          </Grid>
        }
      </Hidden>

      <Hidden xsDown>
        <Breadcrumbs separator={<NavigateNext fontSize="small" />}
          className={classes.breadcrumbs}
          aria-label="breadcrumb">
          <Typography>
            Home
          </Typography>
          <Typography color="secondary">
            {(frame === FRAMES.WELCOME || frame === FRAMES.SIGN_IN) && 'Login'}
            {frame === FRAMES.SIGN_UP && 'Create an account'}
            {frame === FRAMES.RECOVER_PASSWORD && 'Recover password'}
          </Typography>
        </Breadcrumbs>

        <Grid item xs={false} sm={5} md={7} className={classes.image}>
          <div className={classes.image__typo}>
            <Typography color="secondary" variant="h3">
              {(frame === FRAMES.WELCOME || frame === FRAMES.SIGN_IN) && 'Welcome to Xpeedt'}
              {frame === FRAMES.SIGN_UP && 'Create an account'}
              {frame === FRAMES.RECOVER_PASSWORD && 'Recover password'}
            </Typography>
            <Typography variant="h5" style={{ color: '#fff' }}>
              We make sending your sales to
              <br />
              your customers more efficient.
            </Typography>
          </div>
        </Grid>
      </Hidden>

      <Grid item xs={12} sm={7} md={5}
        className={classes.grid__form}
        component={Paper} elevation={6} square>
        {loading ?
          <CircularProgress className="loading" />
          :
          <div className={classes.paper}>
            <Hidden xsDown>
              <img src={logo} alt="Logo" className={classes.logo} />
            </Hidden>

            {frame === FRAMES.WELCOME ?
              <Welcome onSignIn={handleSignIn}
                onSignUp={handleSignUp}
                classes={classes} />
              :
              <>
                {frame === FRAMES.SIGN_IN && <SignIn onSignUp={handleSignUp}
                  onRecoverPassword={handleRecoverPassword}
                  classes={classes} />}
                {frame === FRAMES.SIGN_UP && <SignUp onCancel={handleSignIn}
                  classes={classes} />}
                {frame === FRAMES.RECOVER_PASSWORD && <RecoverPassword onCancel={handleSignIn}
                  classes={classes} />}
              </>
            }
          </div>
        }
      </Grid>
    </Grid>
  );
})
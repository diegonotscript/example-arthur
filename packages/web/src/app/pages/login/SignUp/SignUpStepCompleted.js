import React from 'react';
import Typography from '@material-ui/core/Typography';

export default (props) => {
  const formik = props.formik;

  return (
    <div>
      <div style={{ textAlign: 'center', width: '100%' }}>
        <Typography color="primary" variant="h6">
          Complete registration
        </Typography>
        <Typography color="textSecondary" variant="body2">
          We sent a confirmation email to the email address:
        </Typography>
        <Typography color="secondary" variant="body2" className="strong">
          {formik.values.email.replace(/(\w{3})[\w.-]+@([\w.]+\w)/, "$1***@$2")}
        </Typography>
        <Typography color="textSecondary" variant="body2">
          informed by you.
        </Typography>
      </div>

    </div>
  );
}
import React, { useState } from 'react';
import { InputAdornment, Typography, IconButton } from '@material-ui/core';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import StoreOutlinedIcon from '@material-ui/icons/StoreOutlined';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { FormField } from 'app/components/form/Form';

export default (props) => {
  const formik = props.formik;
  const editing = props.editing || false;

  const [showCurrentPassword, setShowCurrentPassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showRepeatPassword, setShowRepeatPassword] = useState(false);

  const handleClickShowPassword = (repeat) => {
    if (repeat) {
      setShowRepeatPassword(!showRepeatPassword);
      return;
    }

    setShowPassword(!showPassword);
  };

  const handleClickShowCurrentPassword = () => {
    setShowCurrentPassword(!showCurrentPassword);
  };

  return (
    <div>
      <div style={{ textAlign: 'left', width: '100%' }}>
        <Typography color="primary" variant="h6">
          Company information
        </Typography>
        <Typography color="textSecondary" variant="body2">
          The information provided will help us manage your orders.
        </Typography>
      </div>

      <FormField
        required
        placeholder="Company name"
        name="companyName"
        formik={formik}
        InputProps={{
          startAdornment:
            <InputAdornment position="start">
              <StoreOutlinedIcon />
            </InputAdornment>
        }}
      />

      <FormField
        required
        placeholder="Email address"
        name="email"
        autoComplete="email"
        formik={formik}
        InputProps={{
          startAdornment:
            <InputAdornment position="start">
              <EmailOutlinedIcon />
            </InputAdornment>
        }}
      />

      <FormField
        required
        type="tel"
        placeholder="Phone number"
        name="phoneNumber"
        formik={formik}
      />

      <FormField
        required
        placeholder="Contact name"
        name="name"
        formik={formik}
        InputProps={{
          startAdornment:
            <InputAdornment position="start">
              <PersonOutlineIcon />
            </InputAdornment>
        }}
      />

      {editing &&
        <>
          <div className="space-between-h"></div>
          <Typography color="primary" variant="h6">
            Change password
          </Typography>
          <FormField
            required
            name="currentPassword"
            placeholder="Current password"
            type={showCurrentPassword ? 'text' : 'password'}
            formik={formik}
            InputProps={{
              startAdornment:
                <InputAdornment position="start">
                  <LockOutlinedIcon />
                </InputAdornment>,
              endAdornment:
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowCurrentPassword}
                  >
                    {showCurrentPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </IconButton>
                </InputAdornment>
            }}
          />
        </>
      }

      <FormField
        required
        name="password"
        placeholder="Password"
        type={showPassword ? 'text' : 'password'}
        formik={formik}
        InputProps={{
          startAdornment:
            <InputAdornment position="start">
              <LockOutlinedIcon />
            </InputAdornment>,
          endAdornment:
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={() => handleClickShowPassword(false)}
              >
                {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
              </IconButton>
            </InputAdornment>
        }}
      />

      <FormField
        required
        name="repeatPassword"
        placeholder="Repeat password"
        type={showRepeatPassword ? 'text' : 'password'}
        formik={formik}
        InputProps={{
          startAdornment:
            <InputAdornment position="start">
              <LockOutlinedIcon />
            </InputAdornment>,
          endAdornment:
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={() => handleClickShowPassword(true)}
              >
                {showRepeatPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
              </IconButton>
            </InputAdornment>
        }}
      />

    </div>
  );
}
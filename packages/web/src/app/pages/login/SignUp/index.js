import React, { useState, useContext } from 'react';
import { Stepper, Step, StepLabel, Hidden, Grid } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import SignUpStepOne from './SignUpStepOne';
import SignUpStepTwo from './SignUpStepTwo';
import SignUpStepThree from './SignUpStepThree';
import SignUpStepCompleted from './SignUpStepCompleted';
import { ColorlibConnector, ColorlibStepIcon } from 'app/styles/themes/stepper/StepperTheme';
import { object, string, ref } from 'yup';
import { AuthContext } from 'config/AuthProvider';
import { Form, Formik } from 'formik';
import AuthService from 'services/auth/AuthService';
import Notification from 'app/components/notification/Notification';
import Button from 'app/components/button/Button';

const STEP_VALIDATIONS = {
  0: object().shape({
    companyName: string().required('Required'),
    email: string().email('Invalid email address').required('Required')
      .test('Unique Email', 'Email already in use',
        async (value) => {
          if (!value) {
            return true;
          }
          return !(await AuthService().existsEmail({ email: value }))?.data;
        }
      ),
    phoneNumber: string().min(11, 'Invalid phone number').required('Required'),
    name: string().required('Required'),
    password: string().required('Required')
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        "Password must contain at least 8 characters, one uppercase, one number and one special character"
      ),
    repeatPassword: string().oneOf([ref('password'), null], 'Passwords must match').required('Required'),
  }),
  2: object().shape({
    address: object().shape({
      postCode: string().required('Required'),
      address: string().required('Required'),
      city: string().required('Required'),
      country: string().required('Required'),
    })
  })
};

export default withRouter(function SignUp(props) {
  const authContext = useContext(AuthContext);
  const { notify } = Notification();

  const classes = props.classes;
  const onCancel = props.onCancel;

  const steps = [0, 1, 2];

  const [activeStep, setActiveStep] = useState(0);

  function isCompleted() {
    return activeStep > steps[2];
  }

  function isLastStep() {
    return activeStep === steps[2];
  }

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSubmit = (values, { setSubmitting }) => {
    values.phoneNumber = values.phoneNumber.replace(/\D/g, "");

    if (isLastStep()) {
      AuthService().signup(values)
        .then(() => {
          handleNext();
          setSubmitting(false);
        })
        .catch(err => {
          notify(err.message, 'error');
          setSubmitting(false);
        });
    } else if (isCompleted()) {
      authContext.startSession({ email: values.email, password: values.password })
        .catch(err => {
          notify(err.message, 'error');
          setSubmitting(false);
        });
    } else {
      handleNext();
      setSubmitting(false);
    }
  };

  const initialValues = {
    companyName: '',
    email: '',
    phoneNumber: '',
    name: '',
    password: '',
    repeatPassword: '',
    companyWebsite: '',
    showWebsite: true,
    photoUrl: '',
    showLogo: true,
    address: {
      postCode: '',
      address: '',
      city: '',
      country: '',
    }
  };

  return (
    <div className={classes.div__root}>

      <Hidden smUp>
        <Stepper
          alternativeLabel
          activeStep={activeStep}
          className={classes.stepper}
          connector={<ColorlibConnector />}>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel StepIconComponent={ColorlibStepIcon}></StepLabel>
            </Step>
          ))}
        </Stepper>
      </Hidden>

      <Hidden xsDown>
        <Stepper
          activeStep={activeStep}
          className={classes.stepper}
          connector={<ColorlibConnector />}
          orientation="vertical">
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel StepIconComponent={ColorlibStepIcon}></StepLabel>
            </Step>
          ))}
        </Stepper>
      </Hidden>

      <Formik initialValues={initialValues}
        validationSchema={STEP_VALIDATIONS[activeStep]}
        onSubmit={handleSubmit}>
        {(formik) => (
          <Form noValidate>
            {activeStep === steps[0] && <SignUpStepOne formik={formik} />}
            {activeStep === steps[1] && <SignUpStepTwo formik={formik} />}
            {activeStep === steps[2] && <SignUpStepThree formik={formik} />}
            {activeStep > steps[2] && <SignUpStepCompleted formik={formik} />}

            {activeStep > steps[2] ?
              <Button
                type="submit"
                variant="contained"
                color="secondary"
                fullWidth
                className={classes.button}
                loading={formik.isSubmitting}
              >
                Place my first collection order
              </Button>
              :
              <Grid container spacing={2}>
                <Grid item xs={5} sm={5} md={5}>

                  {activeStep === steps[0] &&
                    <Button
                      type="button"
                      variant="outlined"
                      color="primary"
                      fullWidth
                      className={classes.button}
                      onClick={onCancel}
                      disabled={formik.isSubmitting}
                    >
                      Cancel
                    </Button>
                  }

                  {activeStep !== steps[0] &&
                    <Button
                      type="button"
                      variant="outlined"
                      color="primary"
                      fullWidth
                      className={classes.button}
                      onClick={handleBack}
                      disabled={formik.isSubmitting}
                    >
                      Back
                    </Button>
                  }
                </Grid>

                <Grid item xs={7} sm={7} md={7}>
                  <Button
                    type="submit"
                    variant="contained"
                    color="secondary"
                    fullWidth
                    className={classes.button}
                    loading={formik.isSubmitting}
                  >
                    Next
                  </Button>

                </Grid>
              </Grid>
            }
          </Form>
        )}
      </Formik>

    </div>
  );
})
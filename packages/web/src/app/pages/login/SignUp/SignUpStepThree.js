import React from 'react';
import { InputAdornment, Typography } from '@material-ui/core';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';
import { FormField } from 'app/components/form/Form';

export default (props) => {
  const formik = props.formik;

  return (
    <div>
      <div style={{ textAlign: 'left', width: '100%' }}>
        <Typography color="primary" variant="h6">
          Enter a package collection address
        </Typography>
        <Typography color="textSecondary" variant="body2">
          The collection address is essential for us to complete your orders. You can change it whenever you want.
        </Typography>
      </div>
      
      <FormField
        required
        name="address.postCode"
        label="Post code"
        formik={formik}
      />

      <FormField
        required
        name="address.address"
        label="Address"
        formik={formik}
        InputProps={{
          startAdornment:
            <InputAdornment position="start">
              <RoomOutlinedIcon />
            </InputAdornment>
        }}
      />

      <FormField
        required
        name="address.city"
        label="Town"
        formik={formik}
      />

      <FormField
        required
        name="address.country"
        label="Country"
        formik={formik}
      />
    </div>
  );
}
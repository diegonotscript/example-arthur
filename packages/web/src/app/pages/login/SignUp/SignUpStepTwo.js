import React from 'react';
import { InputAdornment, Typography } from '@material-ui/core';
import LanguageIcon from '@material-ui/icons/Language';
import { FormField } from 'app/components/form/Form';
import UploadImage from 'app/components/upload-image/UploadImage';

export default (props) => {
  const formik = props.formik;

  return (
    <div>
      <Typography color="primary" variant="h6">
        Your website
      </Typography>

      <FormField
        required
        placeholder="Company website"
        name="companyWebsite"
        formik={formik}
        InputProps={{
          startAdornment:
            <InputAdornment position="start">
              <LanguageIcon />
            </InputAdornment>
        }}
      />
      <FormField
        required
        type="checkbox"
        name="showWebsite"
        formik={formik}
        label="Do you want to show the site on the label?"
      />

      <div className="space-between-h"></div>

      <Typography color="primary" variant="h6">
        Your logo
      </Typography>

      <UploadImage
        name="photoUrl"
        formik={formik}
        hideUseCamera />
      <FormField
        required
        type="checkbox"
        name="showLogo"
        formik={formik}
        label="Do you want to show the logo on the label?"
      />
    </div >
  );
}
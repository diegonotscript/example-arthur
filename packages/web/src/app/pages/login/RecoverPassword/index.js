import React from 'react';
import { InputAdornment, Typography, Grid } from '@material-ui/core';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import { object, string } from 'yup';
import { FormField } from 'app/components/form/Form';
import { Formik, Form } from 'formik';
import Button from 'app/components/button/Button';
import Notification from 'app/components/notification/Notification';
import UserService from 'services/api/user/UserService';

const VALIDATION = object().shape({
  email: string().email('Invalid email address').required('Required'),
});

const initialValues = {
  email: '',
};

export default function RecoverPassword(props) {
  const { notify } = Notification();

  const classes = props.classes;
  const onCancel = props.onCancel;

  const handleSubmit = (values, { setSubmitting }) => {
    UserService().recoverPassword(values.email)
      .then(() => {
        notify("If your email is registered in Xpeedt, you will receive a reset link");
        setSubmitting(false);
      });
  };

  return (
    <div className={classes.div__root}>

      <div style={{ textAlign: 'left', width: '100%' }}>
        <Typography color="primary" variant="h6">
          Recover password
        </Typography>
        <Typography color="textSecondary" variant="body2">
          I forgot the password
        </Typography>
      </div>

      <Formik initialValues={initialValues}
        validationSchema={VALIDATION}
        onSubmit={handleSubmit}>
        {(formik) => (
          <Form noValidate>

            <FormField
              required
              label="Registered email"
              name="email"
              autoComplete="email"
              autoFocus
              formik={formik}
              InputProps={{
                startAdornment:
                  <InputAdornment position="start">
                    <EmailOutlinedIcon />
                  </InputAdornment>
              }}
            />

            <Grid container spacing={2}>
              <Grid item xs={5}>
                <Button
                  type="button"
                  variant="outlined"
                  color="primary"
                  fullWidth
                  className={classes.button}
                  onClick={onCancel}
                >
                  Cancel
                </Button>
              </Grid>
              <Grid item xs={7}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="secondary"
                  className={classes.button}
                  loading={formik.isSubmitting}
                >
                  Recover password
                </Button>
              </Grid>
            </Grid>

          </Form>
        )}
      </Formik>

    </div>
  );
}
import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { DialogContent, useTheme } from '@material-ui/core';
import { DialogContentText } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import PackageIcon from 'app/components/icons/PackageIcon';
import PackageService from 'services/api/package/PackageService';
import { Typography } from '@material-ui/core';
import Notification from 'app/components/notification/Notification';
import Loader from 'app/components/loader/Loader';

export default function MapDialog(props) {
  const { onClose, idPackage, idTrack, open } = props;
  const [tracking, setTracking] = useState({});
  const [loading, setLoading] = useState(true);
  const theme = useTheme();
  const { notify } = Notification();
  const _isMounted = useRef(true);

  useEffect(() => {
    setLoading(true);
    PackageService()
      .track(idPackage, idTrack)
      .then((response) => {
        setTracking(response.data);
      })
      .catch(err => {
        notify(err.message, 'error');
      })
      .finally(() => {
        setLoading(false);
      });
    return () => {
      _isMounted.current = false;
    }
  }, [idPackage, idTrack]);

  const handleClose = () => {
    setTracking({});
    onClose();
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title"
      open={open}
      maxWidth="sm"
      fullWidth={true}>
      <DialogTitle id="form-dialog-title">
        Tracking package
        <DialogContentText>
          <PackageIcon color={theme.palette.secondary.main} style={{ marginBottom: '-6px', marginRight: '8px' }} />
          <Typography color="primary" variant="body1" component="span">
            Package:
          </Typography>
          &nbsp;
          <Typography color="textSecondary" variant="body1" component="span" noWrap>
            {tracking.name}
          </Typography>
        </DialogContentText>

        <IconButton aria-label="close" className="MuiDialogClose" onClick={handleClose}>
          <CloseIcon />
        </IconButton>

      </DialogTitle>
      <DialogContent dividers={true}>
        <Grid container
          alignItems="center">
          <Grid item xs={12}>

            {loading && <Loader />}

            {(!loading && tracking) &&
              <>
                Map here. Tracking id: {tracking.id}
              </>
            }

          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
};
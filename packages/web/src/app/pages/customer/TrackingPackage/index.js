import React, { useEffect, useRef, useState } from 'react';
import { Grid, Typography, Container, Box, useTheme, makeStyles, Chip, Avatar } from '@material-ui/core';
import OrderService from 'services/api/order/OrderService';
import PackageIcon from 'app/components/icons/PackageIcon';
import { useParams } from 'react-router-dom';
import Notification from 'app/components/notification/Notification';
import NumberUtils from 'utils/NumberUtils';
import Button from 'app/components/button/Button';
import Skeleton from '@material-ui/lab/Skeleton';
import { Table } from 'app/components/table/Table';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';
import MapDialog from './MapDialog';
import { CircularProgress } from '@material-ui/core';
import DateUtils from 'utils/DateUtils';

const useStyles = makeStyles(theme => ({
  box: {
    background: '#0e1f3808',
    border: '1px solid rgba(224, 224, 224, 1)',
    borderRadius: theme.spacing(0.5)
  },
  packageTitle: {
    marginBottom: theme.spacing(2),
    display: 'inline-block'
  }
}));

export default function TrackingPackage() {
  const classes = useStyles();
  const _isMounted = useRef(true);

  let { idOrder, idPackage } = useParams();
  const { notify } = Notification();
  const theme = useTheme();

  const [idTrack, setIdTrack] = useState(null);
  const [mainPackage, setMainPackage] = useState({});
  const [loadingPackage, setLoadingPackage] = useState(true);
  const [openMap, setOpenMap] = useState(false);

  useEffect(() => {
    setLoadingPackage(true);
    OrderService()
      .getPackageById(idOrder, idPackage)
      .then((response) => {
        setMainPackage(response.data);
      })
      .catch(err => {
        notify(err.message, 'error');
      })
      .finally(() => {
        setLoadingPackage(false);
      });
    return () => {
      _isMounted.current = false;
    }
  }, [idOrder])

  const handleClickOpenMap = (e, id) => {
    e.preventDefault();
    setIdTrack(id);
    setOpenMap(true);
  };

  const handleCloseMap = (value) => {
    setOpenMap(false);
  };

  const rows = [
    { id: 1, date: '2021-07-27', hour: '10:30', address: 'Customer\'s address', status: 'On its way' },
    { id: 2, date: '2021-07-27', hour: '10:30', address: 'Customer\'s address', status: '' },
    { id: 3, date: '2021-07-27', hour: '10:30', address: 'Customer\'s address', status: '' },
  ];

  const columns = [
    { field: 'date', headerName: 'Date', disableColumnMenu: true },
    { field: 'hour', headerName: 'Hour', disableColumnMenu: true },
    { field: 'address', headerName: 'Address', flex: 1, disableColumnMenu: true },
    {
      field: 'status', headerName: 'Status', flex: 1, renderCell: (params) => {
        return <>
          {params.value}
          <Button onClick={(e) => handleClickOpenMap(e, params.id)}
            variant="contained"
            color="secondary"
            disableElevation
            size="small"
            startIcon={<RoomOutlinedIcon />}
            style={{ marginLeft: theme.spacing(1) }}
          >
            See in the map
          </Button>
        </>;
      }
    },
  ];

  return (
    <Container>

      <MapDialog
        idPackage={idPackage}
        idTrack={idTrack}
        open={openMap}
        onClose={handleCloseMap} />

      <Grid container
        direction="column"
        justifycontent="flex-start"
      >
        <Grid item xs>
          <Box p={2} my={2} className={classes.box}>
            <Grid item xs={12}>
              <Typography color="primary" variant="h6" className={classes.packageTitle}>
                Package
              </Typography>
              <Chip className="chip-reverse"
                avatar={<Avatar>
                  {loadingPackage && <CircularProgress color="inherit" size={16} />}
                  {(!loadingPackage && mainPackage.id) ? mainPackage.id : ""}
                </Avatar>}
                color="default"
                label="Number"
                style={{ marginTop: '-4px', marginLeft: '8px' }} />
            </Grid>

            {loadingPackage && <Skeleton variant="rect" width="100%" height="24px" />}

            {(!loadingPackage && mainPackage.id) &&
              <>
                <Grid container direction="row"
                  justifycontent="flex-start"
                  alignItems="center"
                  spacing={2}
                >
                  <Grid item zeroMinWidth>
                    <PackageIcon color={theme.palette.secondary.main} size="small" style={{ marginBottom: '-4px', marginRight: '8px' }} />
                    <Typography color="primary" variant="body1" component="span">
                      Name:
                    </Typography>
                    &nbsp;
                    <Typography color="textSecondary" variant="body1" component="span" noWrap>
                      {mainPackage.name}
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Price:
                    </Typography>
                    &nbsp;
                    <Typography color="secondary" variant="body1" component="span" noWrap>
                      {NumberUtils.format(mainPackage.price, '£')}
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Date:
                    </Typography>
                    &nbsp;
                    <Typography color="textSecondary" variant="body1" component="span" noWrap>
                      {DateUtils.formatDateHourDefault(mainPackage.order.createdAt)}
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Order number:
                    </Typography>
                    &nbsp;
                    <Typography color="textSecondary" variant="body1" component="span" noWrap>
                      {mainPackage.order.id}
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Payment method:
                    </Typography>
                    &nbsp;
                    <Typography color="textSecondary" variant="body1" component="span" noWrap>
                      {mainPackage.order.paymentMethod}
                    </Typography>
                  </Grid>
                </Grid>
              </>
            }
          </Box>
        </Grid>

        <Grid item xs>
          <Typography color="primary" variant="h6" paragraph>
            Tracking history
          </Typography>
        </Grid>

        {/* TABLE */}
        <Grid item xs>
          <Box mt={2}>
            <Table
              hasLoadedItems
              columns={columns}
              rows={rows}
              loading={loadingPackage}
            />
          </Box>
        </Grid>

      </Grid>
    </Container>
  );
}
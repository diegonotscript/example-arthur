import React, { forwardRef, useEffect, useImperativeHandle, useRef } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { DialogContent, InputAdornment } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import AddressService from 'services/api/address/AddressService';
import Notification from 'app/components/notification/Notification';
import { FormField } from 'app/components/form/Form';
import { object, string } from 'yup';
import { Formik, Form } from 'formik';
import { DialogActions } from '@material-ui/core';
import Button from 'app/components/button/Button';
import { RoomOutlined } from '@material-ui/icons';

const VALIDATION = object().shape({
  postCode: string().required('Required'),
  address: string().required('Required'),
  city: string().required('Required'),
  country: string().required('Required'),
});

const INITIAL_VALUES = {
  id: '',
  postCode: '',
  address: '',
  city: '',
  country: '',
  isDefault: true
};

export default function CollectionAddressForm(props) {
  const { onClose, open, itemToEdit } = props;

  const childRef = useRef();
  const { notify } = Notification();

  const handleSubmit = (values, { setSubmitting }) => {
    AddressService()
      .save(values)
      .then(() => {
        handleClose(null, true);

        if (values.id) {
          notify('The address was updated', 'success');
          return;
        }

        notify('A new address was added', 'success');
      })
      .catch(err => {
        notify(err.message, 'error');
        setSubmitting(false);
      })
      .finally(() => {
        setSubmitting(false);
      });
  };

  const handleClose = (e, reload) => {
    childRef.current?.handleClose(e, reload);
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="collection-address-dialog"
      open={open}
      maxWidth="xs"
      fullWidth={true}
      scroll="paper">
      <Formik initialValues={INITIAL_VALUES}
        validationSchema={VALIDATION}
        onSubmit={handleSubmit}>
        {(formik) => (
          <Form noValidate>
            <CollectionAddressFormContent formik={formik} ref={childRef} onClose={onClose} open={open} itemToEdit={itemToEdit} />
          </Form>
        )}
      </Formik>
    </Dialog>
  );
};

const CollectionAddressFormContent = forwardRef((props, ref) => {
  const _isMounted = useRef(true);

  const { onClose, formik, itemToEdit } = props;

  useEffect(() => {
    if (!itemToEdit.id) {
      return;
    }

    const fields = ['id', 'postCode', 'address', 'city', 'country', 'isDefault'];
    fields.forEach(field => {
      formik.setFieldValue(field, itemToEdit[field]);
    });

    return () => {
      _isMounted.current = false;
    }
  }, [itemToEdit]);

  const handleClose = (e, reload) => {
    formik.resetForm();
    onClose(e, reload);
  };

  useImperativeHandle(ref, () => ({
    handleClose(e, reload) {
      handleClose(e, reload);
    },
  }));

  return (
    <>
      <DialogTitle id="form-dialog-title">
        {formik.values?.id ? 'Editing' : 'New'} address

        <IconButton aria-label="close" className="MuiDialogClose" onClick={handleClose}>
          <CloseIcon />
        </IconButton>

      </DialogTitle>
      <DialogContent dividers={true}>
        <FormField
          required
          name="postCode"
          label="Post code"
          formik={formik}
        />

        <br />

        <FormField
          required
          name="address"
          label="Address"
          formik={formik}
          InputProps={{
            startAdornment:
              <InputAdornment position="start">
                <RoomOutlined />
              </InputAdornment>
          }}
        />

        <br />

        <FormField
          required
          name="city"
          label="Town"
          formik={formik}
        />

        <br />

        <FormField
          required
          name="country"
          label="Country"
          formik={formik}
        />

        <br />

        <FormField
          required
          type="checkbox"
          name="isDefault"
          formik={formik}
          label="Do you want to make this collection address as a default?"
        />

      </DialogContent>
      <DialogActions>
        <Button
          color="secondary"
          variant="contained"
          type="submit"
          loading={formik.isSubmitting}>
          Save
        </Button>
      </DialogActions>
    </>
  );
});

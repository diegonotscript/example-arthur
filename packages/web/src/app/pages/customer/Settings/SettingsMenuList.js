import React from 'react';
import { Avatar, Divider, List, ListItem, ListItemAvatar, ListItemText, ListSubheader } from '@material-ui/core';

export default function SettingsMenuList(props) {
  const paths = props.paths || [];
  const authenticatedUser = props.authenticatedUser || {};
  const onClick = props.onClick;

  return (
    <List
      subheader={
        <ListSubheader>
          Menu
        </ListSubheader>
      }
    >
      <ListItem button onClick={() => onClick(paths[0])}>
        <ListItemAvatar>
          <Avatar src={`${authenticatedUser?.photoUrl}`}>

          </Avatar>
        </ListItemAvatar>
        <ListItemText primary={authenticatedUser?.name} secondary="Edit profile" />
      </ListItem>
      {paths.filter(path => path.name !== 'Profile').map((path, index) => (
        <div key={index}>
          <Divider />
          <ListItem button onClick={() => onClick(path)}>
            <ListItemAvatar>
              {path.icon}
            </ListItemAvatar>
            <ListItemText primary={path.name} />
          </ListItem>
        </div>
      ))}
    </List>
  );
}
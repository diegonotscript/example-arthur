import React, { useRef, useState } from 'react';
import { Grid, Typography, Box, useTheme, useMediaQuery } from '@material-ui/core';
import { SearchInput } from 'app/components/search-input/SearchInput';
import Notification from 'app/components/notification/Notification';
import { Table } from 'app/components/table/Table';
import Button from 'app/components/button/Button';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import IconButton from 'app/components/button/IconButton';
import AddressService from 'services/api/address/AddressService';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CollectionAddressForm from './CollectionAddressForm';

export default function CollectionAddress() {
  const theme = useTheme();
  const { notify } = Notification();

  const mobile = useMediaQuery(theme.breakpoints.down('sm'));

  const [openForm, setOpenForm] = useState(false);
  const [itemToEdit, setItemToEdit] = useState({});
  const tableRef = useRef();

  const handleLoadList = () => {
  };

  const handleClickOpenForm = () => {
    setOpenForm(true);
  };

  const handleCloseForm = (e, reload) => {
    setOpenForm(false);
    setItemToEdit({});
    if (reload) {
      tableRef.current.refresh();
    }
  };

  const handleEdit = (item) => {
    setItemToEdit(item);
    handleClickOpenForm();
  };

  const handleRemove = (item) => {
    item.loading = true;
    AddressService()
      .remove(item.id)
      .then(() => {
        tableRef.current.refresh();
        notify('The address ' + item.name + ' was removed', 'success');
      })
      .catch(err => {
        item.loading = false;
        notify(err.message, 'error');
      });
  };

  const columns = [
    { field: 'id', headerName: 'Id', width: 80, align: 'center' },
    { field: 'postCode', headerName: 'Postcode', flex: 1 },
    {
      field: 'address', headerName: 'Address', sortable: false, align: 'center', flex: 0.5
    },
    {
      field: 'city', headerName: 'Town', sortable: false, align: 'center', flex: 0.5, hide: mobile
    },
    {
      field: 'country', headerName: 'Country', sortable: false, align: 'center', flex: 0.5, hide: mobile
    },
    {
      field: 'isDefault', headerName: 'Default', sortable: false, align: 'center', renderCell: (params) => {
        return <>
          {params.row?.isDefault && <CheckCircleIcon style={{ color: theme.palette.success.main }} />}
        </>;
      }
    },
    {
      field: '', headerName: '', width: 80, sortable: false, renderCell: (params) => {
        return <>
          <IconButton color="primary"
            aria-label="edit item"
            component="span"
            size="xs"
            tooltip="Edit"
            onClick={() => handleEdit(params.row)}>
            <EditOutlinedIcon />
          </IconButton>
          <IconButton color="primary"
            aria-label="remove item"
            component="span"
            size="xs"
            tooltip="Remove"
            loading={params.row.loading}
            onClick={() => handleRemove(params.row)}>
            <DeleteOutlineOutlinedIcon />
          </IconButton>
        </>;
      }
    },
  ];

  return (
    <>
      <CollectionAddressForm
        open={openForm}
        onClose={handleCloseForm}
        itemToEdit={itemToEdit} />

      <Grid container
        direction="column"
      >
        <Grid item xs>
          <Typography color="primary" variant="h6">
            Collection Address
          </Typography>
          <Typography color="textSecondary" variant="body2">
            Add, edit or remove a collection address
          </Typography>
        </Grid>

        <Grid item xs style={{ marginTop: theme.spacing(4) }}>
          <Grid container
            direction="row"
            justifycontent="flex-end"
            alignItems="center"
            spacing={2}
          >
            <Grid item xs={12} md={6} className={mobile ? "textRight" : ""}>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleClickOpenForm}
              >
                Add address
              </Button>
            </Grid>
            <Grid item xs={12} md={6}>
              <SearchInput
                placeholder="Address"
                name="searchInput"
                fullWidth={true} />
            </Grid>
          </Grid>
        </Grid>

        {/* TABLE */}
        <Grid item xs>
          <Box mt={2}>
            <Table
              columns={columns}
              service={AddressService}
              method="list"
              onLoad={handleLoadList}
              ref={tableRef}
            />
          </Box>
        </Grid>

      </Grid>
    </>
  );
}
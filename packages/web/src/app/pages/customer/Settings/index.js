import { Grid } from '@material-ui/core';
import { Payment, RoomOutlined } from '@material-ui/icons';
import Storage from 'config/Storage';
import React, { useEffect, useRef, useState } from 'react';
import CollectionAddress from './CollectionAddress';
import Profile from './Profile';
import SettingsMenuList from './SettingsMenuList';

export default function Settings() {
  const _isMounted = useRef(true);
  const [path, setPath] = useState({
    name: ''
  });
  const authenticatedUser = Storage.getUser();

  const PATHS = [
    { name: 'Profile', component: <Profile authenticatedUser={authenticatedUser} /> },
    { name: 'Collection Address', icon: <RoomOutlined />, component: <CollectionAddress authenticatedUser={authenticatedUser} /> },
    { name: 'Payments Methods', icon: <Payment />, component: <></> },
  ];

  useEffect(() => {
    if (!_isMounted) {
      return;
    }

    setPath(PATHS[0]);

    return () => {
      _isMounted.current = false;
    }
  }, []);

  const handleClickMenu = (item) => {
    setPath(item);
  };

  return (
    <>
      <Grid container
        direction="column"
        justifycontent="flex-start"
      >

        <Grid item xs>
          <Grid container
            direction="row"
            justifycontent="flex-start"
          >
            <Grid item md={3} style={{ borderRight: '1px solid #d4d4d4' }}>
              <SettingsMenuList paths={PATHS} authenticatedUser={authenticatedUser} onClick={handleClickMenu} />
            </Grid>

            <Grid item md={9} style={{ padding: '0 32px', height: '80vh', overflowY: 'auto' }}>
              {path?.component}
            </Grid>

          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
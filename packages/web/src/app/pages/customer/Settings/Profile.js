import SignUpStepOne from 'app/pages/login/SignUp/SignUpStepOne';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { object, string, ref } from 'yup';
import { Form, Formik } from 'formik';
import Button from 'app/components/button/Button';
import SignUpStepTwo from 'app/pages/login/SignUp/SignUpStepTwo';
import Loader from 'app/components/loader/Loader';
import UserService from 'services/api/user/UserService';
import Notification from 'app/components/notification/Notification';
import { AuthContext } from 'config/AuthProvider';

const VALIDATION = object().shape({
  companyName: string().required('Required'),
  email: string().email('Invalid email address').required('Required'),
  phoneNumber: string().min(11, 'Invalid phone number').required('Required'),
  name: string().required('Required'),
  currentPassword: string()
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      "Password must contain at least 8 characters, one uppercase, one number and one special character"
    ),
  password: string()
    .when('currentPassword', {
      is: (currentPassword) => currentPassword,
      then: string()
        .matches(
          /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
          "Password must contain at least 8 characters, one uppercase, one number and one special character"
        )
        .notOneOf([ref('currentPassword'), null], 'New password must be different from current password')
        .required('Required'),
    }),
  repeatPassword: string()
    .when('currentPassword', {
      is: (currentPassword) => currentPassword,
      then: string().oneOf([ref('password'), null], 'Passwords must match')
        .required('Required'),
    })
});

export default function Profile(props) {
  const formikRef = useRef();
  const [loading, setLoading] = useState(false);
  const { notify } = Notification();
  const authContext = useContext(AuthContext);
  const authenticatedUser = props.authenticatedUser || {};

  const initialValues = {
    companyName: '',
    email: '',
    phoneNumber: '',
    name: '',
    currentPassword: '',
    password: '',
    repeatPassword: '',
    companyWebsite: '',
    showWebsite: false,
    photoUrl: '',
    showLogo: false
  };

  useEffect(() => {
    setLoading(true);
    const data = authenticatedUser || initialValues;
    if (formikRef.current) {
      const fields = Object.keys(initialValues);
      fields.forEach(field => {
        if (data[field]) {
          formikRef.current.setFieldValue(field, data[field]);
        }
      });
    }
    setLoading(false);
  }, []);

  const handleSubmit = (values, { setSubmitting }) => {
    values.phoneNumber = values.phoneNumber.replace(/\D/g, "");
    UserService().update(values)
      .then((response) => {
        notify('Your profile was updated', 'success');

        authContext.setUser(response.data);

        if (formikRef.current) {
          formikRef.current.setFieldValue('currentPassword', '');
          formikRef.current.setFieldValue('password', '');
          formikRef.current.setFieldValue('repeatPassword', '');
        }

        setSubmitting(false);
      })
      .catch(err => {
        notify(err.message, 'error');
        setSubmitting(false);
      });
  };

  return (
    <>
      {loading && <Loader />}
      {!loading && <Formik initialValues={initialValues}
        validationSchema={VALIDATION}
        onSubmit={handleSubmit}
        innerRef={formikRef}>
        {(formik) => (
          <Form noValidate>
            <SignUpStepOne formik={formik} editing={true} />

            <div className="space-between-h"></div>

            <SignUpStepTwo formik={formik} />

            <div className="space-between-h"></div>

            <Button
              type="submit"
              variant="contained"
              color="secondary"
              loading={formik.isSubmitting}
              className="floatRight"
            >
              Update profile
            </Button>
          </Form>
        )}
      </Formik>
      }
    </>
  );
}
import React, { useEffect, useRef, useState } from 'react';
import { Grid, Typography, useTheme } from '@material-ui/core';
import NavTab from 'app/components/nav-tab/NavTab';
import OrderService from 'services/api/order/OrderService';
import ListSearch from 'app/components/search-input/SearchInput';
import PackageIcon from 'app/components/icons/PackageIcon';
import { Listable } from 'app/components/listable/Listable';
import { Hidden } from '@material-ui/core';
import { ArrowForwardIosOutlined } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';
import Notification from 'app/components/notification/Notification';
import Skeleton from '@material-ui/lab/Skeleton';
import Button from 'app/components/button/Button';
import DateUtils from 'utils/DateUtils';
import EmptyState from 'app/components/empty-state/EmptyState';
import NumberUtils from 'utils/NumberUtils';

export default function Order() {
  const _isMounted = useRef(true);
  const theme = useTheme();
  const history = useHistory();
  const { notify } = Notification();

  const listRef = useRef();
  const listSearchRef = useRef();
  const [tabValue, setTabValue] = useState(0);
  const [items, setItems] = useState([]);
  const [tabOptions, setTabOptions] = useState([
    { label: 'Waiting collection', total: 0 },
    { label: 'In process', total: 0 },
  ]);
  const [loadingTotal, setLoadingTotal] = useState(true);
  const [filters, setFilters] = useState('');

  useEffect(() => {
    const abortController = new AbortController();

    setLoadingTotal(true);
    loadTotalByStatus();
    setLoadingTotal(false);

    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  const loadTotalByStatus = () => {
    OrderService()
      .getTotalByStatus()
      .then((response) => {
        setTabOptions((prevTabOptions) => {
          prevTabOptions[0].total = response?.data?.totalWaitingCollection || 0;
          prevTabOptions[1].total = response?.data?.totalInProcess || 0;
          return prevTabOptions;
        });
      })
      .catch(err => {
        if (_isMounted.current) {
          notify(err.message, 'error');
        }
      });
  };

  const handleChangeTab = (e, newValue) => {
    setTabValue(newValue);
    listRef.current.refresh(joinFilters(filters, getTabFilter(newValue)));
  };

  const joinFilters = (filter1, filter2) => {
    if (filter1 && filter2) {
      return [filter1, filter2].join(' and ');
    }

    if (filter1) {
      return filter1;
    }

    if (filter2) {
      return filter2;
    }
  }

  const getTabFilter = (newValue) => {

    if (newValue === 0) {
      return 'order.orderStatus is null';
    }

    if (newValue === 1) {
      return 'order.orderStatus is not null';
    }

    return null;
  };

  const handleSearch = (e, value) => {
    let where = value ? `order.id = ${value}` : '';
    setFilters(where);
    listRef.current.refresh(joinFilters(where, getTabFilter(tabValue)));
  };

  const handleLoadList = (response) => {
    setItems(response);
  };

  const handleClickOrder = (e, index) => {
    const order = items[index];
    history.push('/orders/' + order.id);
  };

  const emptyState = () => {
    return <EmptyState message="You haven't done any collection request yet."
      button={<Button
        type="button"
        variant="contained"
        color="secondary"
        onClick={() => history.push('/orders/new')}
      >
        Place my first collection order
      </Button>} />
  }

  const initialFilter = getTabFilter(0);

  return (
    <Grid container
      direction="column"
      justifycontent="flex-start"
    >
      <Grid item xs>
        <Typography color="primary" variant="h6">
          Orders
        </Typography>
      </Grid>

      <Grid item xs>
        <ListSearch
          placeholder="Number"
          name="searchInput"
          onSearch={handleSearch}
          type="number"
          ref={listSearchRef}
          listRef={listRef}
          enableClear
        />
      </Grid>

      {/* TABS */}
      <Grid item xs>
        {loadingTotal && <Skeleton variant="rect" width="360px" height="48px" />}
        {!loadingTotal &&
          <NavTab
            className={"filter-status-order" + (tabValue === 1 ? " filter-status-order__second-selected" : "")}
            value={tabValue}
            onChange={handleChangeTab}
            options={tabOptions}
          />}
      </Grid>

      {/* TABLE */}
      <Grid item xs>
        <Listable
          hasPagination
          service={OrderService}
          method="list"
          onLoad={handleLoadList}
          ref={listRef}
          searchRef={listSearchRef}
          emptyState={emptyState()}
          initialFilter={initialFilter}
          endAction={{
            onClick: handleClickOrder,
            icon: <ArrowForwardIosOutlined />,
            label: 'Edit'
          }}>
          {items.map((item, index) => (
            <Grid container direction="row"
              justifycontent="flex-start"
              alignItems="center"
              key={index}>
              <Grid item xs={6} md={2} className="ellipsis">
                <PackageIcon color={theme.palette.secondary.main} style={{ marginBottom: '-6px', marginRight: '8px' }} />
                <Typography color="primary" variant="body1" component="span">
                  Number:
                </Typography>
                <Hidden xsDown>&nbsp;</Hidden>
                <Hidden smUp><br /></Hidden>
                <Typography color="textSecondary" variant="body1" component="span">
                  {item.id}
                </Typography>
              </Grid>
              <Grid item xs={6} md={3} className="ellipsis">
                <Typography color="primary" variant="body1" component="span">
                  Sales Number:
                </Typography>
                <Hidden xsDown>&nbsp;</Hidden>
                <Hidden smUp><br /></Hidden>
                <Typography color="textSecondary" variant="body1" component="span">
                  {item.salesNumber}
                </Typography>
              </Grid>
              <Grid item xs={4} md={3} className="ellipsis">
                <Typography color="primary" variant="body1" component="span">
                  Package:
                </Typography>
                <Hidden xsDown>&nbsp;</Hidden>
                <Hidden smUp><br /></Hidden>
                <Typography color="secondary" variant="body1" component="span">
                  {item.orderPackage.name}
                </Typography>
              </Grid>
              <Grid item xs={4} md={2} className="ellipsis">
                <Typography color="primary" variant="body1" component="span">
                  Date:
                </Typography>
                <Hidden xsDown>&nbsp;</Hidden>
                <Hidden smUp><br /></Hidden>
                <Typography color="textSecondary" variant="body1" component="span">
                  {DateUtils.formatDateHourDefault(item.createdAt)}
                </Typography>
              </Grid>
              <Grid item xs={4} md={2} className="ellipsis">
                <Typography color="primary" variant="body1" component="span">
                  Total price:
                </Typography>
                <Hidden xsDown>&nbsp;</Hidden>
                <Hidden smUp><br /></Hidden>
                <Typography color="secondary" variant="body1" component="span">
                  {NumberUtils.format(item.totalPrice, '£')}
                </Typography>
              </Grid>
            </Grid>
          ))}
        </Listable>

      </Grid>

    </Grid>
  );
}
import React, { useEffect, useRef, useState } from 'react';
import { Grid, Typography, Container, Box, useTheme, makeStyles, useMediaQuery, Chip, Tooltip } from '@material-ui/core';
import OrderService from 'services/api/order/OrderService';
import { useHistory } from 'react-router-dom';
import Notification from 'app/components/notification/Notification';
import Button from 'app/components/button/Button';
import IconButton from 'app/components/button/IconButton';
import DateUtils from 'utils/DateUtils';
import { orange } from '@material-ui/core/colors';
import EmptyState from 'app/components/empty-state/EmptyState';
import { Table } from 'app/components/table/Table';
import EditOutlined from '@material-ui/icons/EditOutlined';
import NumberUtils from 'utils/NumberUtils';
import { DeleteOutlineOutlined, PrintOutlined } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  box: {
    background: '#0e1f3808',
    border: '1px solid rgba(224, 224, 224, 1)',
    borderRadius: theme.spacing(0.5)
  },
  packageTitle: {
    marginBottom: theme.spacing(2),
    display: 'inline-block'
  }
}));

export default function Cart() {
  const _isMounted = useRef(true);
  const classes = useStyles();
  const { notify } = Notification();
  const theme = useTheme();
  const history = useHistory();
  const [items, setItems] = useState([]);
  const tableRef = useRef();
  const filtersRef = useRef();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));
  const initialFilter = 'order.paymentMethod is null';

  const handleLoadList = (event) => {
    setItems(event);
  };

  const handlePrintLabel = (idOrder) => {
    history.push('/orders/label/' + idOrder);
  };

  const handleNewOrder = () => {
    history.push('/orders/new');
  };

  const emptyState = () => {
    return <EmptyState message="There is nothing in your cart."
      button={<Button
        type="button"
        variant="contained"
        color="secondary"
        onClick={handleNewOrder}
      >
        Create a new order
      </Button>}
    />
  }

  const handleEdit = (item) => {
    history.push('/orders/' + item.id);
  };

  const handleRemove = (item) => {
    item.loading = true;
    OrderService()
      .remove(item.id)
      .then(() => {
        tableRef.current.refresh();
        notify('The order ' + item.salesNumber + ' was removed', 'success');
      })
      .catch(err => {
        item.loading = false;
        notify(err.message, 'error');
      });
  };

  const columns = [
    {
      field: 'order.id', headerName: 'Number', width: 110, align: 'center', renderCell: (params) => {
        return params.row.id;
      }
    },
    {
      field: 'order.createdAt', headerName: 'Date', renderCell: (params) => {
        return DateUtils.formatDateDefault(params.row.createdAt);
      }
    },
    {
      field: 'order.shippingAddress', headerName: 'Shipping Address', flex: 0.5, renderCell: (params) => {
        return <strong className="ellipsis">{params.row.shippingAddress}</strong>;
      }
    },
    {
      field: 'collectionAddress.address', headerName: 'Collection Address', flex: 0.5, renderCell: (params) => {
        return <strong className="ellipsis">{params.row.collectionAddress?.fullAddress}</strong>;
      }
    },
    {
      field: 'orderPackage.name', headerName: 'Package', flex: 0.5, align: 'left', renderCell: (params) => {
        return <>
          <div className="ellipsis">{params.row.orderPackage.name}</div>
          {params.row.extraPackages > 0 &&
            <Tooltip title="Extra packages">
              <Chip label={`+${params.row.extraPackages}`} color="default" style={{ marginLeft: 8 }} />
            </Tooltip>
          }
        </>;
      }
    },
    {
      field: 'order.totalPrice', headerName: 'Total price', flex: 0.3, align: 'right', renderCell: (params) => {
        return NumberUtils.format(params.row.totalPrice, '£');
      }
    },
    {
      field: '', headerName: '', width: 80, align: 'center', sortable: false, renderCell: (params) => {
        return <>
          <IconButton color="primary"
            aria-label="Print label"
            component="span"
            size="xs"
            tooltip="Print label"
            loading={params.row.loading}
            onClick={() => handlePrintLabel(params.row.id)}>
            <PrintOutlined />
          </IconButton>
          <IconButton color="primary"
            aria-label="edit item"
            component="span"
            size="xs"
            tooltip="Edit"
            loading={params.row.loading}
            onClick={() => handleEdit(params.row)}>
            <EditOutlined />
          </IconButton>
          <IconButton color="primary"
            aria-label="remove item"
            component="span"
            size="xs"
            tooltip="Remove"
            loading={params.row.loading}
            onClick={() => handleRemove(params.row)}>
            <DeleteOutlineOutlined />
          </IconButton>
        </>;
      }
    },
  ];


  return (
    <Container>
      <Grid container
        direction="column"
        justifycontent="flex-start"
      >
        <Grid item xs>
          <Box p={2} my={2} style={{ backgroundColor: orange[100] }}>
            <Typography color="primary" variant="body1">
              Orders placed until 12:00 a.m will be collected on the same day until two in the afternoon after
              that time the collection will be carried out the next day.
            </Typography>
          </Box>
        </Grid>

        <Grid item xs>
          <Typography color="primary" variant="h6">
            Orders in cart
          </Typography>
        </Grid>

        {/* TABLE */}
        <Grid item xs style={{ marginTop: theme.spacing(1) }}>
          <Table
            columns={columns}
            service={OrderService}
            method="list"
            onLoad={handleLoadList}
            ref={tableRef}
            emptyState={emptyState()}
            initialFilter={initialFilter}
            filtersRef={filtersRef}
          />
        </Grid>

      </Grid>
    </Container>
  );
}
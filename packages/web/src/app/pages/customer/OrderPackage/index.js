import React, { useEffect, useRef, useState } from 'react';
import { Grid, Typography, Container, Box, useTheme, makeStyles, Chip, Avatar, CircularProgress } from '@material-ui/core';
import OrderService from 'services/api/order/OrderService';
import { Listable } from 'app/components/listable/Listable';
import PackageIcon from 'app/components/icons/PackageIcon';
import { useHistory, useParams } from 'react-router-dom';
import Notification from 'app/components/notification/Notification';
import { Hidden } from '@material-ui/core';
import NumberUtils from 'utils/NumberUtils';
import Button from 'app/components/button/Button';
import PrintOutlinedIcon from '@material-ui/icons/PrintOutlined';
import Skeleton from '@material-ui/lab/Skeleton';
import LocalShippingOutlinedIcon from '@material-ui/icons/LocalShippingOutlined';
import Loader from 'app/components/loader/Loader';
import DateUtils from 'utils/DateUtils';
import EmptyState from 'app/components/empty-state/EmptyState';

const useStyles = makeStyles(theme => ({
  box: {
    background: '#0e1f3808',
    border: '1px solid rgba(224, 224, 224, 1)',
    borderRadius: theme.spacing(0.5)
  },
  packageTitle: {
    marginBottom: theme.spacing(2),
    display: 'inline-block'
  }
}));

export default function OrderPackage() {
  const _isMounted = useRef(true);
  const classes = useStyles();

  let { idOrder } = useParams();
  const { notify } = Notification();
  const theme = useTheme();
  const history = useHistory();

  const [items, setItems] = useState([]);
  const [order, setOrder] = useState({ packages: [] });
  const [loadingOrder, setLoadingOrder] = useState(true);

  useEffect(() => {
    setLoadingOrder(true);
    OrderService()
      .getById(idOrder)
      .then((response) => {
        response.data.completeAddress = getCompleteAddress(response.data.collectionAddress);
        response.data.orderStatus = { name: 'oi' }
        setOrder(response.data);
      })
      .catch(err => {
        notify(err.message, 'error');
      })
      .finally(() => {
        setLoadingOrder(false);
      });
    return () => {
      _isMounted.current = false;
    }
  }, [idOrder])

  const handleLoadList = (event) => {
    setItems(event);
  };

  const handleClickTrack = (e, index) => {
    const idPackage = items[index]?.id;
    history.push('/orders/' + idOrder + '/packages/' + idPackage + '/tracking');
  };

  const handleClickGoToOrders = (e) => {
    history.push('/orders/');
  };

  const handleClickPrintLabel = (e, index) => {
    history.push('/orders/label/' + idOrder);
  };

  const getCompleteAddress = (collectionAddress) => {
    if (!collectionAddress) {
      return;
    }
    return [collectionAddress.address, collectionAddress.city, collectionAddress.country].join(", ");
  }

  const emptyState = () => {
    return <EmptyState message="Packages not found"
      button={<Button
        type="button"
        variant="contained"
        color="secondary"
        onClick={handleClickGoToOrders}
      >
        Go to my orders
      </Button>}
    />
  }

  return (
    <Container>
      <Grid container
        direction="column"
        justifycontent="flex-start"
      >
        <Grid item xs>
          <Box p={2} my={2} className={classes.box}>
            <Grid item xs={12}>
              <Typography color="primary" variant="h6" className={classes.packageTitle}>
                Order
              </Typography>
              <Chip className="chip-reverse"
                avatar={<Avatar>
                  {loadingOrder && <CircularProgress color="inherit" size={16} />}
                  {(!loadingOrder && order.id) ? order.id : ""}
                </Avatar>}
                color="default"
                label="Number"
                style={{ marginTop: '-4px', marginLeft: '8px' }} />
            </Grid>

            {loadingOrder && <Skeleton variant="rect" width="100%" height="24px" />}

            {(!loadingOrder && order.id) &&
              <>
                <Grid container direction="row"
                  justifycontent="flex-start"
                  alignItems="center"
                  spacing={2}
                >
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Date:
                    </Typography>
                    &nbsp;
                    <Typography color="textSecondary" variant="body1" component="span" noWrap>
                      {DateUtils.formatDateHourDefault(order.createdAt)}
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Payment method:
                    </Typography>
                    &nbsp;
                    <Typography color="textSecondary" variant="body1" component="span" noWrap>
                      {order.paymentMethod}
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Total price:
                    </Typography>
                    &nbsp;
                    <Typography color="secondary" variant="body1" component="span" noWrap>
                      {NumberUtils.format(order.totalPrice, '£')}
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Total packages:
                    </Typography>
                    &nbsp;
                    <Typography color="secondary" variant="body1" component="span" noWrap className="strong">
                      {order.totalPackages}
                    </Typography>
                  </Grid>
                </Grid>
              </>
            }
          </Box>
        </Grid>

        <Grid item xs>
          <Typography color="primary" variant="h6">
            Packages
          </Typography>
        </Grid>

        {/* TABLE */}
        <Grid item xs>
          {loadingOrder && <Loader />}

          {(!loadingOrder && order.id) &&
            <Listable
              hasLoadedItems
              loadedItems={order.packages}
              onLoad={handleLoadList}
              emptyState={emptyState()}
              endAction={{
                onClick: (!order.orderStatus ? handleClickPrintLabel : handleClickTrack),
                icon: (!order.orderStatus ? <PrintOutlinedIcon /> : <LocalShippingOutlinedIcon />),
                label: (!order.orderStatus ? 'Print label' : 'Track history'),
              }}>
              {items.map((item, index) => (
                <Grid container direction="row"
                  justifycontent="flex-start"
                  alignItems="center"
                  spacing={2}
                  key={index}>
                  <Grid item xs={6} md={3} className="ellipsis">
                    <PackageIcon color={theme.palette.secondary.main} style={{ marginBottom: '-6px', marginRight: '8px' }} />
                    <Typography color="primary" variant="body1" component="span">
                      Package:
                    </Typography>
                    <Hidden xsDown>&nbsp;</Hidden>
                    <Hidden smUp><br /></Hidden>
                    <Typography color="textSecondary" variant="body1" component="span">
                      {item.name}
                    </Typography>
                  </Grid>
                  <Grid item xs={6} md={2} className="ellipsis">
                    <Typography color="primary" variant="body1" component="span">
                      Price:
                    </Typography>
                    <Hidden xsDown>&nbsp;</Hidden>
                    <Hidden smUp><br /></Hidden>
                    <Typography color="secondary" variant="body1" component="span">
                      {NumberUtils.format(item.price, '£')}
                    </Typography>
                  </Grid>
                  <Grid item xs={6} md={3} className="ellipsis">
                    <Typography color="primary" variant="body1" component="span">
                      Shipping to:
                    </Typography>
                    <Hidden xsDown>&nbsp;</Hidden>
                    <Hidden smUp><br /></Hidden>
                    <Typography color="textSecondary" variant="body1" component="span">
                      {order.shippingName}
                    </Typography>
                  </Grid>
                  <Grid item xs={6} md={4} className="ellipsis">
                    <Typography color="primary" variant="body1" component="span">
                      Address:
                    </Typography>
                    <Hidden xsDown>&nbsp;</Hidden>
                    <Hidden smUp><br /></Hidden>
                    <Typography color="textSecondary" variant="body1" component="span">
                      {order.completeAddress}
                    </Typography>
                  </Grid>
                </Grid>
              ))}
            </Listable>
          }
        </Grid>

      </Grid>
    </Container>
  );
}
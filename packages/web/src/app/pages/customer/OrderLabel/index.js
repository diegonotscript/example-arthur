import React, { createRef, forwardRef, useEffect, useRef, useState } from 'react';
import { Grid, Typography, Box, makeStyles, Paper, useTheme, Link } from '@material-ui/core';
import OrderService from 'services/api/order/OrderService';
import { useParams, useHistory } from 'react-router-dom';
import { CircularProgress } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import PrintOutlinedIcon from '@material-ui/icons/PrintOutlined';
import Button from 'app/components/button/Button';
import GetAppOutlinedIcon from '@material-ui/icons/GetAppOutlined';
import { ArrowForwardIosOutlined } from '@material-ui/icons';
import { Hidden } from '@material-ui/core';
import Notification from 'app/components/notification/Notification';
import generateLabels from 'utils/PDFUtils';
import { PDFDownloadLink, PDFViewer, usePDF } from '@react-pdf/renderer';

const useStyles = makeStyles(theme => ({
  root: {
    background: green[100],
    position: 'relative',
    textAlign: 'center',
    [theme.breakpoints.up('md')]: {
      height: '100%'
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(2)
    },
  },
  fullHeight: {
    height: '100%'
  },
  paper: {
    height: 'calc(100% - 208px)'
  },
  buttonGoTo: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2)
  },
  checkIcon: {
    fontSize: '10rem',
    color: green[500],
    background: theme.palette.common.white,
    borderRadius: '50%',
    [theme.breakpoints.down('sm')]: {
      fontSize: '3rem',
      position: 'absolute',
      margin: 'auto',
      top: '-1.5rem',
      left: 0,
      right: 0
    },
  }
}));

export default forwardRef((props, ref) => {
  const classes = useStyles();
  const { notify } = Notification();
  const _isMounted = useRef(true);

  let { idOrder } = useParams();
  const history = useHistory();
  const theme = useTheme();
  const filenamePDF = `ORDER-label-${idOrder}`;

  const [loading, setLoading] = useState(true);
  const [order, setOrder] = useState({ collectionAddress: {}, customer: {} });
  const [qrCode, setQrCode] = useState("");
  const [pdfContent, setPdfContent] = useState({});
  let orderLabelToPrint = createRef();

  const loadOrder = () => {
    setLoading(true);
    OrderService().getById(idOrder)
      .then(response => {
        const data = response.data || {};
        if (_isMounted.current) {
          setOrder(data);
          setQrCode(data.qrCode);
          setLoading(false);
        }
      })
      .catch(err => {
        notify(err.message, 'error');
      });
  }

  useEffect(() => {
    const abortController = new AbortController();
    loadOrder();
    handleGenerateLabels();

    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  const handleGoToOrders = () => {
    history.push('/orders');
  }

  const handleGoToCart = () => {
    history.push('/cart');
  }

  const handleGenerateLabels = async () => {
    const data = await generateLabels(idOrder);
    setPdfContent(data);
  }

  const frameLabel = () => {
    if (!pdfContent || !pdfContent.component) {
      return <></>;
    }

    return <>
      <PDFViewer width="100%" height="100%" showToolbar={false}>
        {pdfContent.component}
      </PDFViewer>
    </>;
  }

  const buttonDownloadLabel = () => {
    if (!pdfContent || !pdfContent.component) {
      return <></>;
    }

    return <>
      <PDFDownloadLink document={pdfContent.component} fileName={pdfContent.name}>
        {({ loading }) =>
          <Button
            startIcon={<GetAppOutlinedIcon />}
            fullWidth
            variant="contained"
            color="secondary"
            loading={loading}
          >
            Download
          </Button>
        }
      </PDFDownloadLink>
    </>;
  }

  const handlePrint = (e) => {
    window.open(pdfContent, "PRINT", "height=400,width=600");
  }

  return (
    <>
      {loading ?
        <CircularProgress className="loading" />
        :
        <>
          <div className={classes.root}>
            <Hidden smDown>
              <Grid container
                direction="row"
                justifycontent="center"
                alignItems="center"
                className={classes.root}
                spacing={3}
              >
                <Grid item xs={12} md={6} className={classes.fullHeight}>
                  {frameLabel()}
                </Grid>

                <Grid item xs={12} md={6}>
                  <Grid container
                    direction="column"
                    justifycontent="center"
                    alignItems="center"
                    spacing={3}
                  >
                    <Grid item xs>
                      <Grid container
                        direction="column"
                        justifycontent="center"
                        alignItems="center"
                        spacing={3}
                        className={classes.maxWidth}
                      >
                        <Grid item xs>
                          <CheckCircleIcon style={{ fontSize: '10rem', color: green[500] }} />
                        </Grid>

                        <Grid item xs>
                          <Box my={2}>
                            <Typography color="primary">
                              Your shipping tag was successfully generated and added to <Link href="#" onClick={handleGoToCart} color="secondary" variant="body1">your cart</Link> for payment.
                              The label must be placed on the shipping package.
                            </Typography>
                          </Box>
                        </Grid>
                        {/* 
                        <Grid item xs>
                          <Button
                            variant="contained"
                            color="secondary"
                            startIcon={<PrintOutlinedIcon />}
                            onClick={handlePrint}
                          >
                            Print label
                          </Button>
                        </Grid>
 */}
                        <Grid item>
                          <Box my={1}>
                            {buttonDownloadLabel()}
                          </Box>
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item xs>
                      <Button
                        endIcon={<ArrowForwardIosOutlined />}
                        className={classes.buttonGoTo}
                        onClick={handleGoToOrders}
                      >
                        Go to my orders
                      </Button>
                    </Grid>

                  </Grid>
                </Grid>

              </Grid>
            </Hidden>

            <Hidden mdUp>
              <Grid container
                direction="column"
                justifycontent="center"
                alignItems="center"
                className={classes.fullHeight}
              >
                <Grid item>
                  <CheckCircleIcon className={classes.checkIcon} />
                  <Box mb={2} mt={4} px={2}>
                    <Typography color="primary" variant="body2">
                      Your shipping tag was successfully generated and added to
                      <Typography color="secondary" variant="body2" component="span"> your cart </Typography>
                      for payment.
                      The label must be placed on the shipping package.
                    </Typography>
                  </Box>
                </Grid>

                <Grid item
                className={classes.fullHeight}>
                  {frameLabel()}
                </Grid>

                <Grid item>
                  <Box my={1}>
                    {buttonDownloadLabel()}
                  </Box>
                </Grid>
              </Grid>
            </Hidden>

          </div>
          <Hidden mdUp>
            <Box mt={2} className="textCenter">
              <Button
                endIcon={<ArrowForwardIosOutlined />}
                onClick={handleGoToOrders}
              >
                Go to my orders
              </Button>
            </Box>
          </Hidden>
        </>
      }
    </>
  );
})
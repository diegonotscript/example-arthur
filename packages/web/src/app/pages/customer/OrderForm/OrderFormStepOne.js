import React, { useState, useEffect, useRef, forwardRef } from 'react';
import { Grid, Typography, Box, InputAdornment, Lin, useTheme, Link } from '@material-ui/core';
import { FormField } from 'app/components/form/Form';
import { useFormikContext } from 'formik';
import { EmailOutlined, PersonOutlined, PhoneOutlined, RoomOutlined } from '@material-ui/icons';
import ChangeCollectionAddressDialog from './ChangeCollectionAddressDialog';
import AddressService from 'services/api/address/AddressService';
import Skeleton from '@material-ui/lab/Skeleton';

export default forwardRef((props, ref) => {
  const formik = props.formik;

  const _isMounted = useRef(true);
  const { values, setFieldValue } = useFormikContext();

  const [openCollectionAddress, setOpenCollectionAddress] = useState(false);
  const [collectionAddresses, setCollectionAddresses] = useState([]);
  const [defaultCollectionAddress, setDefaultCollectionAddress] = useState({});
  const [loadingDefaultCollectionAddress, setLoadingDefaultCollectionAddress] = useState(true);
  const [loadingCollectionAddress, setLoadingCollectionAddress] = useState(true);

  const handleClickOpenCollectionAddress = (e) => {
    e.preventDefault();
    setOpenCollectionAddress(true);
  };

  const handleCloseCollectionAddress = (value) => {
    setOpenCollectionAddress(false);
    if (value) {
      setDefaultCollectionAddress(value);
      setFieldValue('useDefaultCollectionAddress', true);
      onChangeUseDefaultCollectionAddress(null, value);
    }
  };

  useEffect(() => {
    const abortController = new AbortController();

    loadDefaultAddress();
    loadAutocompleteAddress();

    if (_isMounted.current && values?.collectionAddress?.isDefault) {
      setFieldValue('useDefaultCollectionAddress', true);
    }

    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  const loadAutocompleteAddress = () => {
    setLoadingCollectionAddress(true);
    AddressService().list()
      .then(response => {
        if (_isMounted.current) {
          setCollectionAddresses(response.data.content);
          setLoadingCollectionAddress(false);
        }
      });
  }

  const loadDefaultAddress = () => {
    setLoadingDefaultCollectionAddress(true);
    AddressService().getDefault()
      .then(response => {
        if (_isMounted.current) {
          setDefaultCollectionAddress(response.data);
          setLoadingDefaultCollectionAddress(false);
        }
      });
  }

  const onChangeUseDefaultCollectionAddress = (e, value) => {
    setFieldValue('collectionAddress', value ? defaultCollectionAddress : {});
  }

  return (
    <Grid item xs={12} md={4}>
      <Box mb={2}>
        <Typography color="primary" variant="h6">
          Enter your sales number
        </Typography>
        <Typography color="textSecondary" variant="body1">
          This is any internal reference that the company uses to manage the sale of the product. Ex: Order number | Invoice
        </Typography>

        <FormField
          required
          name="salesNumber"
          label="Sales number"
          formik={formik}
          autoFocus
        />
      </Box>

      <Box mb={2}>
        <Typography color="primary" variant="h6">
          Shipping to
        </Typography>

        <FormField
          required
          name="shippingName"
          label="Name"
          formik={formik}
          InputProps={{
            startAdornment:
              <InputAdornment position="start">
                <PersonOutlined />
              </InputAdornment>
          }}
        />

        <FormField
          required
          type="tel"
          name="shippingPhone"
          label="Phone number"
          formik={formik}
          InputProps={{
            startAdornment:
              <InputAdornment position="start">
                <PhoneOutlined />
              </InputAdornment>
          }}
        />

        <FormField
          required
          type="email"
          name="shippingEmail"
          label="Email address"
          formik={formik}
          InputProps={{
            startAdornment:
              <InputAdornment position="start">
                <EmailOutlined />
              </InputAdornment>
          }}
        />

        <FormField
          required
          name="shippingAddress"
          label="Shipping address"
          formik={formik}
          InputProps={{
            startAdornment:
              <InputAdornment position="start">
                <RoomOutlined />
              </InputAdornment>
          }}
        />
      </Box>

      <Box mb={2}>
        <Typography color="primary" variant="h6">
          Collection
        </Typography>

        {/* <Link href="#" onClick={handleClickOpenCollectionAddress} color="secondary" variant="body1"
          className="strong"
          style={{ marginLeft: '32px' }}>
          Change my collection address
        </Link> */}

        <FormField
          required
          type="autoComplete"
          options={collectionAddresses}
          getOptionLabel="fullAddress"
          loading={loadingCollectionAddress}
          name="collectionAddress"
          label="Address"
          formik={formik}
          disabled={formik.values.useDefaultCollectionAddress}
          noOptionsText={<Link href="#" onClick={handleClickOpenCollectionAddress} color="primary" variant="body1">
            Add collection address
          </Link>}
        />

        <FormField
          required
          type="checkbox"
          name="useDefaultCollectionAddress"
          onChange={onChangeUseDefaultCollectionAddress}
          formik={formik}
          label="Use my default collection address"
          subLabel={loadingDefaultCollectionAddress ? <Skeleton variant="text" /> : defaultCollectionAddress.fullAddress}
        />

        <ChangeCollectionAddressDialog
          selectedValue={defaultCollectionAddress}
          open={openCollectionAddress}
          onClose={handleCloseCollectionAddress} />
      </Box>
    </Grid >
  );
});
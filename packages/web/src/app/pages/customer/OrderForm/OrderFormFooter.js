import React, { forwardRef } from 'react';
import { Grid, Typography, FormHelperText } from '@material-ui/core';
import { FormFooter } from 'app/components/form/Form';
import Button from 'app/components/button/Button';
import NumberUtils from 'utils/NumberUtils';

export default forwardRef((props, ref) => {
  const { formik } = props;

  return (
    <FormFooter>
      <Grid item md={9} className="middle">
        <div className="floatRight">
          {formik.values.totalPrice > 0 ?
            <>
              <Typography color="textSecondary" variant="body1" component="span">
                Price
              </Typography>
              <br />
              <Typography color="primary" variant="h6" component="span">
                {NumberUtils.format(formik.values.totalPrice, '£')}
              </Typography>
            </>
            :
            <FormHelperText style={{ lineHeight: '52px' }}>Choose a package type</FormHelperText>
          }
        </div>
      </Grid>
      <Grid item md={3} className="last">
        <Button
          type="submit"
          variant="contained"
          color="secondary"
          loading={formik.isSubmitting}
        >
          Generate shipping label
        </Button>
      </Grid>
    </FormFooter>
  );
});
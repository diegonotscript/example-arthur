import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { DialogContent } from '@material-ui/core';
import { DialogContentText } from '@material-ui/core';
import { DialogActions } from '@material-ui/core';
import AddressService from 'services/api/address/AddressService';
import { Listable } from 'app/components/listable/Listable';
import { Grid } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import { ArrowForwardIosOutlined, RoomOutlined } from '@material-ui/icons';
import CloseIcon from '@material-ui/icons/Close';
import { Link } from '@material-ui/core';
import { Slide } from '@material-ui/core';
import { Formik, Form } from 'formik';
import { object, string } from 'yup';
import { FormField } from 'app/components/form/Form';
import { InputAdornment } from '@material-ui/core';
import Button from 'app/components/button/Button';
import Notification from 'app/components/notification/Notification';
import { findIndex } from 'lodash';

const VALIDATION = object().shape({
  postCode: string().required('Required'),
  address: string().required('Required'),
  city: string().required('Required'),
  country: string().required('Required'),
});

const initialValues = {
  postCode: '',
  address: '',
  city: '',
  country: '',
  isDefault: true
};

export default function ChangeCollectionAddressDialog(props) {
  const { onClose, selectedValue, open } = props;

  const childRef = useRef();
  const { notify } = Notification();

  const handleSubmit = (values, { setSubmitting }) => {
    AddressService().save(values)
      .then(() => {
        childRef.current?.toggleEditing();
        notify('A new collection address was added', 'success');
        setSubmitting(false);
      })
      .catch((err) => {
        notify(err.message, 'error');
        setSubmitting(false);
      });
  };

  const handleClose = () => {
    childRef.current?.handleClose();
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="collection-address-dialog"
      open={open}
      maxWidth="sm"
      fullWidth={true}
      scroll="paper">
      <Formik initialValues={initialValues}
        validationSchema={VALIDATION}
        onSubmit={handleSubmit}>
        {(formik) => (
          <Form noValidate>
            <ChangeCollectionAddressDialogContent formik={formik} ref={childRef} onClose={onClose} selectedValue={selectedValue} open={open} />
          </Form>
        )}
      </Formik>
    </Dialog>
  );
};

const ChangeCollectionAddressDialogContent = forwardRef((props, ref) => {
  const _isMounted = useRef(true);

  const { onClose, selectedValue, formik } = props;
  const [collectionAddresses, setCollectionAddresses] = useState([]);
  const [checked, setChecked] = useState(null);
  const [editing, setEditing] = useState(false);
  const [itemToEdit, setItemToEdit] = useState(initialValues);
  const { notify } = Notification();

  useEffect(() => {
    if (!collectionAddresses.length) {
      return;
    }
    const index = findIndex(collectionAddresses, (item) => { return item.id === selectedValue.id });
    setChecked(index);
    return () => {
      _isMounted.current = false;
    }
  }, [selectedValue, collectionAddresses]);

  useEffect(() => {
    const fields = ['postCode', 'address', 'city', 'country', 'isDefault'];
    fields.forEach(field => {
      formik.setFieldValue(field, itemToEdit[field]);
    });
    return () => {
      _isMounted.current = false;
    }
  }, [itemToEdit]);

  const handleClose = () => {
    formik.resetForm();
    setItemToEdit(initialValues);
    setEditing(false);
    onClose();
  };

  const handleLoadList = (event) => {
    setCollectionAddresses(event);
  };

  const handleToggle = (index) => {
    setChecked(index);
  };

  const toggleEditing = () => {
    formik.resetForm();
    setItemToEdit(initialValues);
    setEditing(!editing);
  };

  const handleEditCollection = (e, index) => {
    setItemToEdit(collectionAddresses[index]);
    setEditing(!editing);
  };

  const handleChangeCollection = () => {
    const newCollectionAddress = collectionAddresses[checked];
    formik.setSubmitting(true);
    AddressService().setDefault(newCollectionAddress)
      .then(() => {
        onClose(newCollectionAddress);
        notify('Your default collection address was updated', 'success');
      })
      .catch(err => {
        notify(err.message, 'error');
      });
  };

  useImperativeHandle(ref, () => ({
    toggleEditing() {
      toggleEditing();
    },
    handleClose() {
      handleClose();
    }
  }));

  return (
    <>
      <DialogTitle id="form-dialog-title">
        Change collection address
        <DialogContentText>
          Choose or add a new collection address
        </DialogContentText>

        <IconButton aria-label="close" className="MuiDialogClose" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <DialogContent dividers={true}>
        {!editing &&
          <Slide direction="right" in={!editing}>
            <div>
              <Grid container
                alignItems="center">
                <Grid item xs={12} className="textRight">
                  <Link href="#" color="secondary" variant="body1" className="strong" onClick={toggleEditing}>
                    Add collection address
                  </Link>
                </Grid>
              </Grid>

              <Listable
                service={AddressService}
                method="list"
                onLoad={handleLoadList}
                dense
                checkable
                checked={checked}
                onToggle={handleToggle}
                endAction={{ onClick: handleEditCollection, icon: <ArrowForwardIosOutlined /> }}
              >
                {collectionAddresses.map((item, index) => (
                  <Grid container direction="row"
                    justifycontent="flex-start"
                    alignItems="center"
                    key={index}>
                    <Grid item xs={12}>{item.address}, {item.city}, {item.country}</Grid>
                  </Grid>
                ))}
              </Listable>
            </div>
          </Slide>
        }

        {editing &&
          <Slide direction="left" in={editing}>
            <div>
              <FormField
                required
                name="postCode"
                label="Post code"
                formik={formik}
              />

              <FormField
                required
                name="address"
                label="Address"
                formik={formik}
                InputProps={{
                  startAdornment:
                    <InputAdornment position="start">
                      <RoomOutlined />
                    </InputAdornment>
                }}
              />

              <FormField
                required
                name="city"
                label="Town"
                formik={formik}
              />

              <FormField
                required
                name="country"
                label="Country"
                formik={formik}
              />

              <FormField
                required
                type="checkbox"
                name="isDefault"
                formik={formik}
                label="Do you want to make this collection address as a default?"
              />
            </div>

          </Slide>
        }

      </DialogContent>

      <DialogActions>
        {(!editing && collectionAddresses.length) &&
          <Button
            onClick={handleChangeCollection}
            color="secondary"
            variant="contained"
            loading={formik.isSubmitting}>
            Change collection address
          </Button>
        }
        {editing &&
          <>
            <Button onClick={toggleEditing}
              color="primary">
              Back
            </Button>
            <Button
              color="secondary"
              variant="contained"
              type="submit"
              loading={formik.isSubmitting}>
              Save
            </Button>
          </>
        }
      </DialogActions>
    </>
  );
});

ChangeCollectionAddressDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.object.isRequired,
};
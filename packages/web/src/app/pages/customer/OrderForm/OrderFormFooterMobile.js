import React, { forwardRef } from 'react';
import { Grid, Typography, FormHelperText } from '@material-ui/core';
import { FormFooter } from 'app/components/form/Form';
import Button from 'app/components/button/Button';
import NumberUtils from 'utils/NumberUtils';
import { useHistory } from 'react-router';

export default forwardRef((props, ref) => {
  const { formik, isFirstStep, isLastStep, handleBack } = props;
  const history = useHistory();

  return (
    <FormFooter>

      {isLastStep &&
        <Grid item xs={12} sm={12} md={12} className="middle">
          <div className="floatRight">
            {formik.values.totalPrice > 0 ?
              <>
                <Typography color="textSecondary" variant="body1" component="span">
                  Price
                </Typography>
                <br />
                <Typography color="primary" variant="h6" component="span">
                  {NumberUtils.format(formik.values.totalPrice, '£')}
                </Typography>
              </>
              :
              <FormHelperText style={{ lineHeight: '52px' }}>Choose a package type</FormHelperText>
            }
          </div>
        </Grid>
      }

      <Grid item xs={5} sm={5} md={5}>

        {isFirstStep ?
          <Button
            type="button"
            variant="outlined"
            color="primary"
            fullWidth
            onClick={() => history.push('/orders')}
            disabled={formik.isSubmitting}
          >
            Cancel
          </Button>
          :
          <Button
            type="button"
            variant="outlined"
            color="primary"
            fullWidth
            onClick={handleBack}
            disabled={formik.isSubmitting}
          >
            Back
          </Button>
        }
      </Grid>

      <Grid item xs={7} sm={7} md={7}>
        <Button
          type="submit"
          variant="contained"
          color="secondary"
          fullWidth
          loading={formik.isSubmitting}
        >
          {!isLastStep ? 'Next' : 'Generate shipping label'}
        </Button>
      </Grid>
    </FormFooter>
  );
});
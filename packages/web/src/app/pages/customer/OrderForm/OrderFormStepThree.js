import React, { useState, useEffect, useRef, forwardRef } from 'react';
import { Grid, Typography, Box, makeStyles, useTheme } from '@material-ui/core';
import PackageService from 'services/api/package/PackageService';
import { useFormikContext } from 'formik';
import PackageIcon from 'app/components/icons/PackageIcon';
import { Listable } from 'app/components/listable/Listable';
import NumberUtils from 'utils/NumberUtils';
import ExtraPackage from './ExtraPackage';
import { findIndex } from 'lodash-es';
import ConfigurationService from 'services/api/configuration/ConfigurationService';

const useStyles = makeStyles(() => ({
  scrollDiv: {
    maxHeight: '345px',
    overflowX: 'auto'
  }
}));

export default forwardRef((props, ref) => {
  const { formik } = props;

  const classes = useStyles();
  const _isMounted = useRef(true);
  const listRef = useRef();
  const theme = useTheme();
  const { values, setFieldValue } = useFormikContext();

  const [packages, setPackages] = useState([]);
  const [extraPackagePrice, setExtraPackagePrice] = useState(0);

  useEffect(() => {
    const abortController = new AbortController();
    loadExtraPackagePrice();
    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  const handleLoadList = (e) => {
    setPackages(e);
  };

  const loadExtraPackagePrice = () => {
    ConfigurationService()
      .getUnique()
      .then((response) => {
        setExtraPackagePrice(response?.data?.extraPackagePrice || 0);
      });
  }

  useEffect(() => {
    setPackagesIfNecessary();
    return () => {
      _isMounted.current = false;
    }
  }, [packages]);

  const handleTogglePackages = (index) => {
    let item = packages[index];
    item.selected = !item.selected;

    setFieldValue('orderPackage', item);
    updatePackage(item);
  };

  const updatePackage = (item) => {
    setPackages((prevPackages) => {
      let index = findIndex(prevPackages, e => e.id === item.id);
      if (index !== -1) {
        prevPackages[index] = item;
      }
      return prevPackages;
    });
  }

  useEffect(() => {
    calculateTotalOrder();
    return () => {
      _isMounted.current = false;
    }
  }, [values.extraPackages, values.orderPackage, extraPackagePrice]);

  const calculateTotalOrder = () => {
    const totalPrice = Number(values.orderPackage?.price) + (Number(values.extraPackages) * Number(extraPackagePrice));
    setFieldValue('totalPrice', totalPrice);
  };

  const setPackagesIfNecessary = () => {
    if (!values.orderPackage) {
      return;
    }

    let index = findIndex(packages, e => e.id === values.orderPackage.id);
    if (index !== -1) {
      values.orderPackage.selected = true;
      listRef?.current?.handleCheck(index);
      updatePackage(values.orderPackage);
    }
  }

  return (
    <Grid item xs={12} md={4}>
      <Box mb={2}>
        <Grid container
          alignItems="center">
          <Grid item xs={10}>
            <Typography color="primary" variant="h6">
              Package type
            </Typography>
          </Grid>
          {/* <Grid item xs={2} className="textRight">
                <Link href="#" onClick={handleSeeAllPackages} color="secondary" variant="body1" className="strong">
                  See all
                </Link>
              </Grid> */}
        </Grid>

        <Box my={1}>
          <div className={classes.scrollDiv}>
            <Listable
              service={PackageService}
              method="list"
              onLoad={handleLoadList}
              checkable
              dense
              onToggle={handleTogglePackages}
              ref={listRef}
              startIcon={<PackageIcon color={theme.palette.secondary.main} />}>
              {packages.map((item, index) => (
                <Grid container direction="row"
                  justifycontent="flex-start"
                  alignItems="center"
                  key={index}>
                  <Grid item xs={12}>
                    <Typography color="textSecondary" variant="body1" component="span" noWrap>
                      {item.name}
                    </Typography>
                    <br />
                    <Typography color="primary" variant="body1" component="span" noWrap>
                      Price: {NumberUtils.format(item.price, '£')}
                    </Typography>
                  </Grid>
                </Grid>
              ))}
            </Listable>
          </div>
        </Box>

        {(formik.touched.orderPackage && formik.errors.orderPackage) &&
          <p className="MuiFormHelperText-root MuiFormHelperText-contained Mui-error Mui-required">
            Required
          </p>
        }

        {formik.values.orderPackage && <ExtraPackage formik={formik} extraPackagePrice={extraPackagePrice} />}

      </Box>
    </Grid>
  );
});
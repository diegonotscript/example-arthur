import React, { useState } from 'react';
import { Box, Paper } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import Button from 'app/components/button/Button';
import IconButton from 'app/components/button/IconButton';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import NumberUtils from 'utils/NumberUtils';
import { FormField } from 'app/components/form/Form';
import { grey } from '@material-ui/core/colors';

export default function ExtraPackage(props) {
  const { formik, extraPackagePrice } = props;

  const [show, setShow] = useState(true);
  const [addExtra, setAddExtra] = useState(false);

  const handleYes = () => {
    setAddExtra(true);
  }

  const handleNo = () => {
    setShow(false);
  }

  const handleIncrease = (e) => {
    handleChange(e, formik.values.extraPackages + 1);
  }

  const handleDecrease = (e) => {
    handleChange(e, formik.values.extraPackages - 1);
  }

  const handleChange = (e, value) => {
    if (value < 0) {
      value = 0;
    }
    formik.setFieldValue('extraPackages', value, true);
  }

  if (!show) {
    return (<></>);
  }

  return (
    <Paper variant="outlined"
      elevation={2}
      style={{ backgroundColor: grey[50] }}>
      <Box p={3}>
        {(!addExtra && !formik.values.extraPackages) ?
          <>
            <Typography color="primary" variant="h6">
              Do you want to add extra packages for this item?
            </Typography>
            <Typography color="textSecondary" variant="body1" paragraph>
              Extra packages are used when your item needs to be delivered in multiple boxes.
            </Typography>
            <div className="flex-horizontal">
              <Button
                fullWidth
                variant="outlined"
                color="primary"
                onClick={handleYes}>
                Yes
              </Button>
              <Button
                fullWidth
                variant="outlined"
                color="primary"
                onClick={handleNo}>
                No
              </Button>
            </div>
          </>
          :
          <>
            <Typography color="primary" variant="h6">
              Add extra package(s)
            </Typography>
            <Typography color="textSecondary" variant="body1">
              The current price for each extra package added is {NumberUtils.format(extraPackagePrice, '£')}.
            </Typography>

            <div className="flex-horizontal">
              <FormField
                onChange={handleChange}
                type="number"
                min="0"
                name="extraPackages"
                placeholder="0"
                formik={formik}
                style={{ maxWidth: '100px', backgroundColor: '#fff' }}
              />
              <IconButton
                variant="contained"
                color="secondary"
                onClick={handleDecrease}
                disabled={formik.values.extraPackages <= 0}
                tooltip="Decrease">
                <RemoveIcon />
              </IconButton>
              <IconButton
                variant="contained"
                color="secondary"
                tooltip="Increase"
                onClick={handleIncrease}>
                <AddIcon />
              </IconButton>
            </div>

            <Typography color="secondary" variant="body1" component="span">
              {NumberUtils.format(formik.values.extraPackages * extraPackagePrice, '£')}
            </Typography>
            &nbsp;
            <Typography color="textSecondary" variant="body1" component="span">
              for {formik.values.extraPackages} extra package(s)
            </Typography>
          </>
        }
      </Box>
    </Paper>
  );
}
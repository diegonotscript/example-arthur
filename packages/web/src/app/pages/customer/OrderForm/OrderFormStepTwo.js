import React, { forwardRef } from 'react';
import { Grid, Typography, Box } from '@material-ui/core';
import { FormField } from 'app/components/form/Form';
import UploadImage from 'app/components/upload-image/UploadImage';

export default forwardRef((props, ref) => {
  const { formik } = props;

  return (
    <Grid item xs={12} md={4}>
      <Box mb={2}>
        <Typography color="primary" variant="h6">
          Product description
        </Typography>

        <FormField
          required
          name="description"
          multiline
          rows={4}
          formik={formik}
        />
      </Box>

      <Box mb={2}>
        <Typography color="primary" variant="h6">
          Product Image
        </Typography>

        <FormField
          required
          type="switch"
          name="sendProductImage"
          formik={formik}
          label="Would you like to send a image of your item?"
          subLabel="Not obligatory"
        />
        {formik.values.sendProductImage &&
          <UploadImage
            name="productImageUrl"
            helpText="The product image will help us to better manage your order."
            formik={formik} />
        }
      </Box>
    </Grid>
  );
});
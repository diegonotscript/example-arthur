import React, { useState, useEffect, useRef, forwardRef } from 'react';
import { useHistory, useParams, withRouter } from 'react-router-dom';
import { Grid, useTheme, useMediaQuery } from '@material-ui/core';
import { object, string, number } from 'yup';
import { Formik, Form } from 'formik';
import Button from 'app/components/button/Button';
import { cloneDeep } from 'lodash';
import UserService from 'services/api/user/UserService';
import OrderService from 'services/api/order/OrderService';
import Notification from 'app/components/notification/Notification';
import EmptyState from 'app/components/empty-state/EmptyState';
import OrderFormStepOne from './OrderFormStepOne';
import OrderFormFooterMobile from './OrderFormFooterMobile';
import OrderFormFooter from './OrderFormFooter';
import OrderFormStepTwo from './OrderFormStepTwo';
import OrderFormStepThree from './OrderFormStepThree';

const STEP_VALIDATIONS = {
  0: object().shape({
    salesNumber: string().required('Required'),
    shippingName: string().required('Required'),
    shippingPhone: string().required('Required'),
    shippingEmail: string().email('Invalid email address').required('Required'),
    shippingAddress: string().required('Required'),
    collectionAddress: object().required('Required').nullable(),
  }),
  2: object().shape({
    orderPackage: object().required('Required').nullable(),
    extraPackages: number()
  })
};

const VALIDATION = object().shape({
  salesNumber: string().required('Required'),
  shippingName: string().required('Required'),
  shippingPhone: string().required('Required'),
  shippingEmail: string().email('Invalid email address').required('Required'),
  shippingAddress: string().required('Required'),
  collectionAddress: object().required('Required').nullable(),
  orderPackage: object().required('Required').nullable(),
  extraPackages: number()
});

const initialValues = {
  id: '',
  salesNumber: '',
  shippingName: '',
  shippingPhone: '',
  shippingEmail: '',
  shippingAddress: '',
  collectionAddress: undefined,
  productDescription: '',
  productImageUrl: undefined,
  sendProductImage: false,
  orderPackage: undefined,
  extraPackages: 0,
  totalPrice: 0
};

export default withRouter(function OrderForm() {
  const childRef = useRef();
  const history = useHistory();
  const { notify } = Notification();
  const _isMounted = useRef(true);
  const formikRef = useRef();
  const [loading, setLoading] = useState(false);
  const [notFound, setNotFound] = useState(false);
  let { idOrder } = useParams();
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));
  const steps = [0, 1, 2];
  const [activeStep, setActiveStep] = useState(0);

  function isFirstStep() {
    return activeStep === steps[0];
  }

  function isLastStep() {
    return activeStep === steps[2];
  }

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSubmit = (values, { setSubmitting }) => {
    const payload = cloneDeep(values);

    if (isLastStep()) {
      UserService().me()
        .then(response => {
          payload.customer = response.data;
        })
        .then(() => {
          return OrderService().save(payload)
            .then((response) => {
              const data = response.data || {};
              const id = data.id;

              if (payload.id) {
                notify('Your order was updated', 'success');
              } else {
                notify('Your order was created', 'success');
              }

              history.push('/orders/label/' + id);
            })
            .catch(err => {
              setSubmitting(false);
              handleError(err);
            });
        })
        .catch(err => {
          setSubmitting(false);
          handleError(err);
        });
    } else {
      handleNext();
      setSubmitting(false);
    }

  };

  const handleError = (err) => {
    notify(err.message, 'error');
  };

  const loadOrder = () => {
    setLoading(true);
    setNotFound(false);
    OrderService().getById(idOrder)
      .then(response => {
        const data = response.data || initialValues;
        if (formikRef.current) {
          const fields = Object.keys(initialValues);
          fields.forEach(field => {
            if (data[field]) {
              if (field === 'productImageUrl') {
                formikRef.current.setFieldValue('sendProductImage', true);
              }
              formikRef.current.setFieldValue(field, data[field]);
            }
          });
        }
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
        handleError(err);
        if (err.message.match(`Order entity ${idOrder} not found`)) {
          setNotFound(true);
        }
      });
  }

  useEffect(() => {
    loadOrder();
    return () => {
      _isMounted.current = false;
    }
  }, [idOrder]);

  if (!loading && notFound) {
    return <EmptyState message="Order not found"
      button={<Button
        type="button"
        variant="contained"
        color="secondary"
        onClick={() => history.push('/orders')}
      >
        Go to my orders
      </Button>}
    />
  }

  return (
    <Formik initialValues={initialValues}
      validationSchema={mobile ? STEP_VALIDATIONS[activeStep] : VALIDATION}
      onSubmit={handleSubmit}
      innerRef={formikRef}>
      {(formik) => (
        <Form noValidate>
          {!loading && <OrderContent formik={formik} ref={childRef} mobile={mobile} isFirstStep={isFirstStep()} isLastStep={isLastStep()} handleBack={handleBack} activeStep={activeStep} />}
        </Form>
      )}
    </Formik>
  );
});

const OrderContent = forwardRef((props, ref) => {
  const { formik, mobile, isFirstStep, isLastStep, handleBack, activeStep } = props;

  return (
    <>
      <Grid container
        direction="row"
        justifycontent="flex-start"
        spacing={6}
      >
        {(activeStep === 0 || !mobile) && <OrderFormStepOne formik={formik} />}
        {(activeStep === 1 || !mobile) && <OrderFormStepTwo formik={formik} />}
        {(activeStep === 2 || !mobile) && <OrderFormStepThree formik={formik} />}

      </Grid>

      {mobile ?
        <OrderFormFooterMobile formik={formik} isFirstStep={isFirstStep} isLastStep={isLastStep} handleBack={handleBack} />
        :
        <OrderFormFooter formik={formik} />
      }
    </>
  );
});
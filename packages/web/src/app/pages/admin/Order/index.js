import React, { useEffect, useRef, useState } from 'react';
import { Grid, Typography, Box, useTheme, useMediaQuery, Avatar, Tooltip, Chip } from '@material-ui/core';
import { Filters } from 'app/components/search-input/SearchInput';
import Notification from 'app/components/notification/Notification';
import { Table } from 'app/components/table/Table';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import OrderService from 'services/api/order/OrderService';
import IconButton from 'app/components/button/IconButton';
import DateUtils from 'utils/DateUtils';
import Button from 'app/components/button/Button';
import { Form, Formik } from 'formik';
import { FormField } from 'app/components/form/Form';
import { InputAdornment } from '@material-ui/core';
import { SearchOutlined } from '@material-ui/icons';
import NavTab from 'app/components/nav-tab/NavTab';
import Skeleton from '@material-ui/lab/Skeleton';
import { useHistory } from 'react-router-dom';

const INITIAL_VALUES = {
  id: '',
  dateFrom: '',
  dateTo: ''
};

export default function Order() {
  const _isMounted = useRef(true);
  const theme = useTheme();
  const { notify } = Notification();
  const history = useHistory();

  const mobile = useMediaQuery(theme.breakpoints.down('sm'));

  const [tabValue, setTabValue] = useState(0);
  const [tabOptions, setTabOptions] = useState([
    { label: 'All', total: 0 },
    { label: 'Today\'s orders', total: 0 },
    { label: 'Collection process', total: 0 },
    { label: 'Shipping process', total: 0 },
    { label: 'Waiting Xpeedt', total: 0 },
    { label: 'Failures', total: 0 },
  ]);
  const [loadingTotal, setLoadingTotal] = useState(true);
  const [filters, setFilters] = useState('');

  const tableRef = useRef();
  const filtersRef = useRef();

  const handleChangeTab = (e, newValue) => {
    setTabValue(newValue);
    tableRef.current.refresh(joinFilters(filters, getTabFilter(newValue)));
  };

  const joinFilters = (filter1, filter2) => {
    if (filter1 && filter2) {
      return [filter1, filter2].join(' and ');
    }

    if (filter1) {
      return filter1;
    }

    if (filter2) {
      return filter2;
    }
  }

  const createFormikFilters = (valuesFormik = null) => {

    let filters = [];

    if (valuesFormik) {
      if (valuesFormik.id) {
        filters.push(`order.id = ${valuesFormik.id}`);
      }
      if (valuesFormik.dateFrom) {
        filters.push(`order.createdAt >= '${valuesFormik.dateFrom}'`);
      }
      if (valuesFormik.dateTo) {
        filters.push(`order.createdAt <= '${valuesFormik.dateTo}'`);
      }
    }

    return filters.join(' and ');
  };

  const getTabFilter = (newValue) => {

    if (newValue === 1) {
      return `order.createdAt = '${DateUtils.now()}'`;
    }

    if (newValue === 2) {
      return 'orderStatus.collectionStatus = true';
    }

    if (newValue === 3) {
      return 'orderStatus.shipmentStatus = true';
    }

    if (newValue === 4) {
      return 'orderStatus.internalStatus = true';
    }

    return null;
  };

  const handleSubmit = (values) => {
    let where = createFormikFilters(values);
    setFilters(where);
    tableRef.current.refresh(joinFilters(where, getTabFilter(tabValue)));
  };

  const handleLoadList = () => {
  };

  const handleEdit = (item) => {
    history.push('/orders/' + item.id);
  };

  const columns = [
    {
      field: 'order.id', headerName: 'Number', width: 110, align: 'center', renderCell: (params) => {
        return params.row.id;
      }
    },
    {
      field: 'order.createdAt', headerName: 'Date', renderCell: (params) => {
        return DateUtils.formatDateDefault(params.row.createdAt);
      }
    },
    {
      field: 'customer.name', headerName: 'Customer', flex: 0.5, renderCell: (params) => {
        return <strong>{params.row.customer?.name}</strong>;
      }
    },
    {
      field: 'collectionAddress.address', headerName: 'Collection Address', hide: mobile, flex: 0.5, renderCell: (params) => {
        return <strong>{params.row.collectionAddress?.fullAddress}</strong>;
      }
    },
    {
      field: 'orderPackage.name', headerName: 'Package', flex: 0.5, align: 'left', renderCell: (params) => {
        return <>
          <div className="ellipsis">{params.row.orderPackage.name}</div>
          {params.row.extraPackages > 0 &&
            <Tooltip title="Extra packages">
              <Chip label={`+${params.row.extraPackages}`} color="default" style={{ marginLeft: 8 }} />
            </Tooltip>
          }
        </>;
      }
    },
    {
      field: 'driver.name', headerName: 'Driver', hide: mobile, align: 'center', renderCell: (params) => {
        return <>
          {params.row.driver &&
            <>
              <Avatar className="smallAvatar" style={{ marginRight: 8 }}>
                {params.row.driver?.name.charAt(0)}
              </Avatar>
              {params.row.driver?.name}
            </>
          }
        </>;
      }
    },
    {
      field: 'orderStatus.description', headerName: 'Status', hide: mobile, flex: 0.5, align: 'center', renderCell: (params) => {
        return params.row.orderStatus && params.row.orderStatus.description;
      }
    },
    {
      field: 'order.updatedAt', headerName: 'Last update', hide: mobile, minWidth: 120, renderCell: (params) => {
        return DateUtils.formatDateHourDefault(params.row.updatedAt);
      }
    },
    {
      field: '', headerName: '', width: 40, sortable: false, renderCell: (params) => {
        return <>
          <IconButton color="primary"
            aria-label="edit item"
            component="span"
            size="xs"
            tooltip="Edit"
            onClick={() => handleEdit(params.row)}>
            <EditOutlinedIcon />
          </IconButton>
        </>;
      }
    },
  ];

  useEffect(() => {
    const abortController = new AbortController();
    setLoadingTotal(true);
    OrderService()
      .getTotalByStatus()
      .then((response) => {
        setTabOptions((prevTabOptions) => {
          prevTabOptions[0].total = response.waitingCollection || 0;
          prevTabOptions[1].total = response.inProcess || 0;
          return prevTabOptions;
        });
      })
      .catch(err => {
        notify(err.message, 'error');
      })
      .finally(() => {
        setLoadingTotal(false);
      });
    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  return (
    <>
      <Grid container
        direction="column"
      >
        <Grid item xs>
          <Typography color="primary" variant="h6">
            Orders
          </Typography>
        </Grid>

        <Grid item xs style={{ marginTop: theme.spacing(1) }}>
          <Formik
            initialValues={INITIAL_VALUES}
            onSubmit={handleSubmit}>
            {(formik) => (
              <Form noValidate>
                <Filters ref={filtersRef} formik={formik}>
                  <Grid container
                    direction="row"
                    justifycontent="flex-start"
                    alignItems="center"
                    spacing={2}
                  >
                    <Grid item xs={12} md={6}>
                      <FormField
                        required
                        type="number"
                        name="id"
                        label="Number"
                        formik={formik}
                        size="small"
                        InputProps={{
                          startAdornment:
                            <InputAdornment position="start">
                              <SearchOutlined />
                            </InputAdornment>
                        }}
                      />
                    </Grid>
                    <Grid item xs={6} md={3}>
                      <FormField
                        required
                        type="date"
                        name="dateFrom"
                        label="From"
                        size="small"
                        formik={formik}
                        InputProps={{
                          startAdornment:
                            <></>
                        }}
                      />
                    </Grid>
                    <Grid item xs={6} md={3}>
                      <FormField
                        required
                        type="date"
                        name="dateTo"
                        label="To"
                        size="small"
                        formik={formik}
                        InputProps={{
                          startAdornment:
                            <></>
                        }}
                      />
                    </Grid>
                  </Grid>
                </Filters>
              </Form>
            )}
          </Formik>
        </Grid>

        {/* TABS */}
        <Grid item xs>
          {loadingTotal && <Skeleton variant="rect" width="360px" height="48px" />}
          {!loadingTotal &&
            <NavTab
              className="filter-status-order"
              value={tabValue}
              onChange={handleChangeTab}
              options={tabOptions}
            />}
        </Grid>

        {/* TABLE */}
        <Grid item xs>
          <Box mt={2}>
            <Table
              columns={columns}
              service={OrderService}
              method="list"
              onLoad={handleLoadList}
              ref={tableRef}
              filtersRef={filtersRef}
            />
          </Box>
        </Grid>

      </Grid>
    </>
  );
}
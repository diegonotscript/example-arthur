import React, { useEffect, useRef, useState } from 'react';
import { Box, Paper } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import Button from 'app/components/button/Button';
import IconButton from 'app/components/button/IconButton';
import NumberUtils from 'utils/NumberUtils';
import { FormField } from 'app/components/form/Form';
import { grey } from '@material-ui/core/colors';
import ConfigurationService from 'services/api/configuration/ConfigurationService';
import Notification from 'app/components/notification/Notification';
import { Form, Formik } from 'formik';
import CheckIcon from '@material-ui/icons/Check';
import { object, string } from 'yup';
import Loader from 'app/components/loader/Loader';

const VALIDATION = object().shape({
  extraPackagePrice: string().required('Required'),
});

const INITIAL_VALUES = {
  extraPackagePrice: 0.00
};

export default function ExtraPackagePrice() {
  const [value, setValue] = useState(0);
  const [loading, setLoading] = useState(false);
  const { notify } = Notification();
  const formikRef = useRef();

  const handleResetValue = () => {
    setValue(0);
    if (formikRef.current) {
      formikRef.current.setFieldValue('extraPackagePrice', 0);
    }
  }

  const handleSubmitExtraPackages = (values, { setSubmitting }) => {
    ConfigurationService()
      .save(values)
      .then(() => {
        setSubmitting(false);
        setValue(values.extraPackagePrice);
        notify('The extra package price was updated', 'success');
      })
      .catch(err => {
        notify(err.message, 'error');
        setSubmitting(false);
      });
  }

  useEffect(() => {
    setLoading(true);
    ConfigurationService()
      .getUnique()
      .then((response) => {
        const extraPackagePrice = response?.data?.extraPackagePrice || 0;
        setValue(extraPackagePrice);
        if (formikRef.current) {
          formikRef.current.setFieldValue('extraPackagePrice', extraPackagePrice);
        }
      })
      .catch(err => {
        notify(err.message, 'error');
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return (
    <Formik initialValues={INITIAL_VALUES}
      validationSchema={VALIDATION}
      onSubmit={handleSubmitExtraPackages}
      innerRef={formikRef}>
      {(formik) => (
        <Form noValidate>
          <Paper variant="outlined"
            elevation={2}
            style={{ backgroundColor: grey[50] }}>
            <Box p={3}>
              <Typography color="primary" variant="h6">
                Set price of extra packages
              </Typography>
              <Typography color="textSecondary" variant="body1" paragraph>
                Extra packages are used when an item needs to be delivered in multiple boxes
              </Typography>

              {loading && <Loader />}

              {(!loading && value <= 0) &&
                <>
                  <Typography color="error" variant="body1">
                    You have not set a extra package price
                  </Typography>
                  <div className="flex-horizontal">
                    <FormField
                      type="currency"
                      name="extraPackagePrice"
                      placeholder="0"
                      formik={formik}
                      style={{ maxWidth: '150px', backgroundColor: '#fff' }}
                    />
                    <IconButton
                      type="submit"
                      variant="contained"
                      color="secondary"
                      tooltip="Save"
                      disabled={formik.values.extraPackagePrice <= 0}>
                      <CheckIcon />
                    </IconButton>
                  </div>
                </>
              }

              {(!loading && value > 0) &&
                <>
                  <div style={{ marginBottom: '8px' }}>
                    <Typography color="primary" variant="body1" component="span">
                      You set
                    </Typography>
                    &nbsp;
                    <Typography color="secondary" variant="body1" component="span" className="strong">
                      {NumberUtils.format(value, '£')}
                    </Typography>
                    &nbsp;
                    <Typography color="primary" variant="body1" component="span">
                      for each extra package
                    </Typography>
                  </div>

                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={handleResetValue}>
                    Reset price
                  </Button>
                </>
              }

            </Box>
          </Paper>
        </Form>
      )}
    </Formik>
  );
}
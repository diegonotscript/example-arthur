import React, { useRef, useState } from 'react';
import { Grid, Typography, Box, useTheme } from '@material-ui/core';
import { SearchInput } from 'app/components/search-input/SearchInput';
import Notification from 'app/components/notification/Notification';
import { Table } from 'app/components/table/Table';
import Button from 'app/components/button/Button';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import PackageService from 'services/api/package/PackageService';
import PackageForm from '../PackageForm';
import NumberUtils from 'utils/NumberUtils';
import IconButton from 'app/components/button/IconButton';
import ExtraPackagePrice from './ExtraPackagePrice';

export default function Package() {
  const theme = useTheme();
  const { notify } = Notification();

  const [openForm, setOpenForm] = useState(false);
  const [itemToEdit, setItemToEdit] = useState({});
  const tableRef = useRef();

  const handleLoadList = () => {
  };

  const handleClickOpenForm = () => {
    setOpenForm(true);
  };

  const handleCloseForm = (e, reload) => {
    setOpenForm(false);
    setItemToEdit({});
    if (reload) {
      tableRef.current.refresh();
    }
  };

  const handleEdit = (item) => {
    setItemToEdit(item);
    handleClickOpenForm();
  };

  const handleRemove = (item) => {
    item.loading = true;
    PackageService()
      .remove(item.id)
      .then(() => {
        tableRef.current.refresh();
        notify('The package ' + item.name + ' was removed', 'success');
      })
      .catch(err => {
        item.loading = false;
        notify(err.message, 'error');
      });
  };

  const columns = [
    { field: 'id', headerName: 'Id', disableColumnMenu: true, width: 80, align: 'center' },
    { field: 'name', headerName: 'Name', disableColumnMenu: true, flex: 1 },
    {
      field: 'price', headerName: 'Price (£)', disableColumnMenu: true, flex: 1, minWidth: 110, renderCell: (params) => {
        return NumberUtils.format(params.row.price, '');
      }
    },
    {
      field: '', headerName: '', width: 80, sortable: false, disableColumnMenu: true, renderCell: (params) => {
        return <>
          <IconButton color="primary"
            aria-label="edit item"
            size="xs"
            tooltip="Edit"
            onClick={() => handleEdit(params.row)}>
            <EditOutlinedIcon />
          </IconButton>
          <IconButton color="primary"
            aria-label="remove item"
            size="xs"
            tooltip="Remove"
            loading={params.row.loading}
            onClick={() => handleRemove(params.row)}>
            <DeleteOutlineOutlinedIcon />
          </IconButton>
        </>;
      }
    },
  ];

  return (
    <>
      <PackageForm
        open={openForm}
        onClose={handleCloseForm}
        itemToEdit={itemToEdit} />

      <Grid container
        direction="column"
      >
        <Grid item xs>
          <Typography color="primary" variant="h6">
            Packages
          </Typography>
          <Typography color="textSecondary" variant="body2">
            Add, edit or remove a package
          </Typography>
        </Grid>

        <Grid item xs style={{ marginTop: theme.spacing(4) }}>
          <Grid container
            direction="row"
            justifycontent="flex-end"
            alignItems="center"
            spacing={2}
          >
            <Grid item xs={12} md={6}>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleClickOpenForm}
              >
                Add package
              </Button>
            </Grid>
            <Grid item xs={12} md={6}>
              <SearchInput
                placeholder="Name"
                name="searchInput"
                fullWidth={true} />
            </Grid>
          </Grid>
        </Grid>

        {/* TABLE */}
        <Grid item xs>
          <Box mt={2}>
            <Table
              columns={columns}
              service={PackageService}
              method="list"
              onLoad={handleLoadList}
              ref={tableRef}
            />
          </Box>
        </Grid>

        <Grid item xs
          style={{ marginTop: theme.spacing(4) }}>
          <ExtraPackagePrice />
        </Grid>

      </Grid>
    </>
  );
}
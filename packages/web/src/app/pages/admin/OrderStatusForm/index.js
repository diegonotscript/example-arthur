import React, { forwardRef, useEffect, useImperativeHandle, useRef } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { DialogContent } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import OrderStatusService from 'services/api/order-status/OrderStatusService';
import Notification from 'app/components/notification/Notification';
import { FormField } from 'app/components/form/Form';
import { object, string, boolean } from 'yup';
import { Formik, Form } from 'formik';
import { DialogActions } from '@material-ui/core';
import Button from 'app/components/button/Button';

const VALIDATION = object().shape({
  description: string().required('Required'),
  collectionStatus: boolean(),
  shipmentStatus: boolean(),
  internalStatus: boolean(),
});

const INITIAL_VALUES = {
  id: '',
  description: '',
  collectionStatus: false,
  shipmentStatus: false,
  internalStatus: false,
};

export default function OrderStatusForm(props) {
  const { onClose, open, itemToEdit } = props;

  const childRef = useRef();
  const { notify } = Notification();

  const handleSubmit = (values, { setSubmitting }) => {
    OrderStatusService()
      .save(values)
      .then(() => {
        handleClose(null, true);

        if (values.id) {
          notify('The status was updated', 'success');
          return;
        }

        notify('A new status was added', 'success');
      })
      .catch(err => {
        notify(err.message, 'error');
        setSubmitting(false);
      })
      .finally(() => {
        setSubmitting(false);
      });
  };

  const handleClose = (e, reload) => {
    childRef.current?.handleClose(e, reload);
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="order-status-dialog"
      open={open}
      maxWidth="xs"
      fullWidth={true}
      scroll="paper">
      <Formik initialValues={INITIAL_VALUES}
        validationSchema={VALIDATION}
        onSubmit={handleSubmit}>
        {(formik) => (
          <Form noValidate>
            <OrderStatusFormContent formik={formik} ref={childRef} onClose={onClose} open={open} itemToEdit={itemToEdit} />
          </Form>
        )}
      </Formik>
    </Dialog>
  );
};

const OrderStatusFormContent = forwardRef((props, ref) => {
  const _isMounted = useRef(true);

  const { onClose, formik, itemToEdit } = props;

  useEffect(() => {
    if (!itemToEdit.id) {
      return;
    }

    const fields = ['id', 'description', 'collectionStatus', 'shipmentStatus', 'internalStatus'];
    fields.forEach(field => {
      formik.setFieldValue(field, itemToEdit[field]);
    });

    return () => {
      _isMounted.current = false;
    }
  }, [itemToEdit]);

  const handleClose = (e, reload) => {
    formik.resetForm();
    onClose(e, reload);
  };

  useImperativeHandle(ref, () => ({
    handleClose(e, reload) {
      handleClose(e, reload);
    },
  }));

  return (
    <>
      <DialogTitle id="form-dialog-title">
        {formik.values?.id ? 'Editing' : 'New'} status

        <IconButton aria-label="close" className="MuiDialogClose" onClick={handleClose}>
          <CloseIcon />
        </IconButton>

      </DialogTitle>
      <DialogContent dividers={true}>
        <FormField
          required
          name="description"
          label="Description"
          formik={formik}
        />

        <FormField
          required
          fullWidth
          type="checkbox"
          name="collectionStatus"
          formik={formik}
          label="Collection status"
        />
        <br />

        <FormField
          required
          type="checkbox"
          name="shipmentStatus"
          formik={formik}
          label="Shipment status"
        />
        <br />

        <FormField
          required
          type="checkbox"
          name="internalStatus"
          formik={formik}
          label="Internal status"
        />
        <br />

      </DialogContent>
      <DialogActions>
        <Button
          color="secondary"
          variant="contained"
          type="submit"
          loading={formik.isSubmitting}>
          Save
        </Button>
      </DialogActions>
    </>
  );
});

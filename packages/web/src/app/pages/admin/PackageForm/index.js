import React, { forwardRef, useEffect, useImperativeHandle, useRef } from 'react';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { DialogContent } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import PackageService from 'services/api/package/PackageService';
import Notification from 'app/components/notification/Notification';
import { FormField } from 'app/components/form/Form';
import { object, string, number } from 'yup';
import { Formik, Form } from 'formik';
import { DialogActions } from '@material-ui/core';
import Button from 'app/components/button/Button';

const VALIDATION = object().shape({
  name: string().required('Required'),
  price: number().required('Required'),
});

const INITIAL_VALUES = {
  id: '',
  name: '',
  price: ''
};

export default function PackageFormDialog(props) {
  const { onClose, open, itemToEdit } = props;

  const childRef = useRef();
  const { notify } = Notification();

  const handleSubmit = (values, { setSubmitting }) => {
    PackageService()
      .save(values)
      .then(() => {
        handleClose(null, true);
        setSubmitting(false);

        if (values.id) {
          notify('The package was updated', 'success');
          return;
        }

        notify('A new package was added', 'success');
      })
      .catch(err => {
        notify(err.message, 'error');
        setSubmitting(false);
      });
  };

  const handleClose = (e, reload) => {
    childRef.current?.handleClose(e, reload);
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="collection-address-dialog"
      open={open}
      maxWidth="xs"
      fullWidth={true}
      scroll="paper">
      <Formik initialValues={INITIAL_VALUES}
        validationSchema={VALIDATION}
        onSubmit={handleSubmit}>
        {(formik) => (
          <Form noValidate>
            <PackageFormDialogContent formik={formik} ref={childRef} onClose={onClose} open={open} itemToEdit={itemToEdit} />
          </Form>
        )}
      </Formik>
    </Dialog>
  );
};

const PackageFormDialogContent = forwardRef((props, ref) => {
  const _isMounted = useRef(true);

  const { onClose, formik, itemToEdit } = props;

  useEffect(() => {
    if (!itemToEdit.id) {
      return;
    }

    const fields = ['id', 'name', 'price'];
    fields.forEach(field => {
      formik.setFieldValue(field, itemToEdit[field]);
    });

    return () => {
      _isMounted.current = false;
    }
  }, [itemToEdit]);

  const handleClose = (e, reload) => {
    formik.resetForm();
    onClose(e, reload);
  };

  useImperativeHandle(ref, () => ({
    handleClose(e, reload) {
      handleClose(e, reload);
    },
  }));

  return (
    <>
      <DialogTitle id="form-dialog-title">
        {formik.values?.id ? 'Editing' : 'New'} package

        <IconButton aria-label="close" className="MuiDialogClose" onClick={handleClose}>
          <CloseIcon />
        </IconButton>

      </DialogTitle>
      <DialogContent dividers={true}>
        <FormField
          required
          name="name"
          label="Name"
          formik={formik}
        />

        <FormField
          required
          name="price"
          label="Price"
          type="number"
          formik={formik}
        />
      </DialogContent>
      <DialogActions>
        <Button
          color="secondary"
          variant="contained"
          type="submit"
          loading={formik.isSubmitting}>
          Save
        </Button>
      </DialogActions>
    </>
  );
});

PackageFormDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

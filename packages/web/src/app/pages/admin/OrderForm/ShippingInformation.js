import { Avatar, Typography, useMediaQuery, useTheme } from "@material-ui/core";
import { EditOutlined, PrintOutlined, DeleteOutlineOutlined, Warning, EmailOutlined } from "@material-ui/icons";
import Button from "app/components/button/Button";
import IconButton from "app/components/button/IconButton";
import { Table } from "app/components/table/Table";
import { useRef } from "react";

export default function ShippingInformation(props) {
  const tableRef = useRef();
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));
  const order = props.order;
  const loading = props.loading;

  const handleEdit = (item) => {

  };

  const columns = [
    {
      field: 'id', headerName: 'Number', sortable: false
    },
    {
      field: 'name', headerName: 'Package', sortable: false
    },
    {
      field: 'price', headerName: 'Price', sortable: false
    },
    {
      field: 'shippingAddress', headerName: 'Shipping to', sortable: false
    },
    {
      field: 'postCode', headerName: 'Postcode', sortable: false
    },
    {
      field: 'city', headerName: 'Town', sortable: false
    },
    {
      field: 'address', headerName: 'Address', sortable: false, flex: 1
    },
    {
      field: 'driver', headerName: 'Driver', sortable: false, flex: 1, align: 'center', renderCell: (params) => {
        return <>
          {params.row.driver ?
            <div className="flex-horizontal">
              <Avatar alt={`driver-avatar-${params.row.driver.name}`} src="https://material-ui.com/static/images/avatar/1.jpg" className="small">
              </Avatar>
              <div className="ellipsis">
                <Typography>
                  {params.row.driver.name}
                </Typography>
              </div>
              <IconButton color="secondary"
                aria-label="change driver"
                size="xs"
                tooltip="Change driver"
                onClick={() => handleEdit(params.row)}>
                <EditOutlined />
              </IconButton>
            </div>
            :
            <Button color="secondary"
              aria-label="add driver"
              size="small"
              tooltip="Add driver"
              variant="outlined"
              onClick={() => handleEdit(params.row)}>
              Add
            </Button>
          }
        </>;
      }
    },
    {
      field: 'orderStatus', headerName: 'Status', sortable: false, flex: 1, align: 'center', renderCell: (params) => {
        return <>
          <div className="flex-horizontal">
            <div className="ellipsis">
              <Typography title={params.row.orderStatus ? params.row.orderStatus.description : 'Waiting Xpeedt'}>
                {params.row.orderStatus ? params.row.orderStatus.description : 'Waiting Xpeedt'}
              </Typography>
            </div>
            <IconButton color="secondary"
              aria-label="change status"
              size="xs"
              tooltip="Change status">
              <EditOutlined />
            </IconButton>
          </div>
        </>
      }
    },
    {
      field: '', headerName: '', width: 140, sortable: false, align: 'right', renderCell: (params) => {
        return <>
          {params.row.hasFailures && <IconButton color="primary"
            aria-label="warning"
            size="xs"
            tooltip="Failures">
            <Warning color="error" />
          </IconButton>}
          <IconButton color="primary"
            aria-label="send message"
            size="xs"
            tooltip="Send message">
            <EmailOutlined />
          </IconButton>
          <IconButton color="primary"
            aria-label="print label"
            size="xs"
            tooltip="Print label">
            <PrintOutlined />
          </IconButton>
          <IconButton color="primary"
            aria-label="edit item"
            size="xs"
            tooltip="Edit">
            <EditOutlined />
          </IconButton>
          <IconButton color="primary"
            aria-label="remove item"
            size="xs"
            tooltip="Remove">
            <DeleteOutlineOutlined />
          </IconButton>
        </>;
      }
    },
  ];

  const rows = [
    { id: 1, driver: { name: 'Gabriel popo' }, orderStatus: { description: 'On its way' }, hasFailures: true },
  ];

  return (
    <Table
      hasLoadedItems
      columns={columns}
      rows={rows}
      loading={loading}
    />
  );
};
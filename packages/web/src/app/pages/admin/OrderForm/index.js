import React, { useEffect, useRef, useState } from 'react';
import { Grid, Typography, Container, Box, useTheme, makeStyles, Chip, Avatar, CircularProgress, Fab, Tooltip } from '@material-ui/core';
import OrderService from 'services/api/order/OrderService';
import { useHistory, useParams } from 'react-router-dom';
import Notification from 'app/components/notification/Notification';
import NumberUtils from 'utils/NumberUtils';
import Button from 'app/components/button/Button';
import Skeleton from '@material-ui/lab/Skeleton';
import DateUtils from 'utils/DateUtils';
import useButtonStyles from 'app/styles/themes/button/ButtonTheme';
import ShippingInformation from './ShippingInformation';
import CollectionInformation from './CollectionInformation';
import History from './History';
import { SaveOutlined } from '@material-ui/icons';
import { Form, Formik } from "formik";
import { object } from 'yup';
import { cloneDeep } from 'lodash-es';
import ConfirmModal from 'app/components/confirm-modal/ConfirmModal';

const VALIDATION = object().shape({
  collectionAddress: object().required('Required'),
  orderStatus: object().nullable(),
  driver: object().nullable(),
});

const INITIAL_VALUES = {
  id: '',
  salesNumber: '',
  shippingName: '',
  shippingPhone: '',
  shippingEmail: '',
  shippingAddress: '',
  collectionAddress: {},
  customer: {},
  productDescription: '',
  sendProductImage: false,
  orderPackage: undefined,
  extraPackages: 0,
  totalPrice: 0,
  orderStatus: {},
  driver: {}
};

const useStyles = makeStyles(theme => ({
  box: {
    background: '#0e1f3808',
    border: '1px solid rgba(224, 224, 224, 1)',
    borderRadius: theme.spacing(0.5)
  },
  packageTitle: {
    marginBottom: theme.spacing(2),
    display: 'inline-block'
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  }
}));

export default function OrderForm() {
  const _isMounted = useRef(true);
  const classes = useStyles();
  const btnClasses = useButtonStyles();

  let { idOrder } = useParams();
  const { notify } = Notification();
  const theme = useTheme();
  const history = useHistory();

  const [order, setOrder] = useState({});
  const [loadingOrder, setLoadingOrder] = useState(true);
  const [loadingRemove, setLoadingRemove] = useState(false);
  const [openConfirmRemove, setOpenConfirmRemove] = useState(false);
  const formikRef = useRef();

  useEffect(() => {
    const abortController = new AbortController();

    setLoadingOrder(true);
    OrderService()
      .getById(idOrder)
      .then((response) => {
        const data = response.data;
        setOrder(data);

        if (formikRef.current) {
          const fields = Object.keys(INITIAL_VALUES);
          fields.forEach(field => {
            if (data[field]) {
              formikRef.current.setFieldValue(field, data[field]);
            }
          });
        }
      })
      .catch(err => {
        notify(err.message, 'error');
      })
      .finally(() => {
        setLoadingOrder(false);
      });

    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  const handleClickPrintLabel = (e, idOrder) => {
    history.push('/orders/label/' + idOrder);
  };

  const handleSubmit = (values, { setSubmitting }) => {
    const payload = cloneDeep(values);

    OrderService().save(payload)
      .then(() => {
        notify('The order was updated', 'success');
      })
      .catch(err => {
        setSubmitting(false);
        handleError(err);
      });
  };

  const handleConfirmRemove = () => {
    setOpenConfirmRemove(true);
  };

  const handleCancelRemove = () => {
    setOpenConfirmRemove(false);
  };

  const onRemove = () => {
    setLoadingRemove(true);
    OrderService().remove(idOrder)
      .then(() => {
        notify('The order was deleted', 'success');
        history.push('/orders');
      })
      .catch(err => {
        setLoadingRemove(false);
        handleError(err);
      });
  };

  const handleError = (err) => {
    notify(err.message, 'error');
  };

  return (
    <Container>

      <Formik initialValues={INITIAL_VALUES}
        validationSchema={VALIDATION}
        onSubmit={handleSubmit}
        innerRef={formikRef}>
        {(formik) => (
          <Form noValidate>

            <Grid container
              direction="column"
              justifycontent="flex-start"
            >
              <Grid item xs>
                <Box p={2} mt={2} className={classes.box} mb={4}>
                  <Grid item xs={12}>
                    <Typography color="primary" variant="h6" className={classes.packageTitle}>
                      Order
                    </Typography>
                    <Chip className="chip-reverse"
                      avatar={<Avatar>
                        {loadingOrder && <CircularProgress color="inherit" size={16} />}
                        {(!loadingOrder && order.id) ? order.id : ""}
                      </Avatar>}
                      color="default"
                      label="Number"
                      style={{ marginTop: '-4px', marginLeft: '8px' }} />
                  </Grid>

                  {loadingOrder && <Skeleton variant="rect" width="100%" height="24px" />}

                  {(!loadingOrder && order.id) &&
                    <>
                      <Grid container direction="row"
                        justifycontent="flex-start"
                        alignItems="center"
                        spacing={2}
                      >
                        <Grid item zeroMinWidth>
                          <Typography color="primary" variant="body1" component="span">
                            Date:
                          </Typography>
                          &nbsp;
                          <Typography color="textSecondary" variant="body1" component="span" noWrap>
                            {DateUtils.formatDateHourDefault(order.createdAt)}
                          </Typography>
                        </Grid>
                        <Grid item zeroMinWidth>
                          <Typography color="primary" variant="body1" component="span">
                            Sales number:
                          </Typography>
                          &nbsp;
                          <Typography color="textSecondary" variant="body1" component="span" noWrap>
                            {order.salesNumber}
                          </Typography>
                        </Grid>
                        <Grid item zeroMinWidth>
                          <Typography color="primary" variant="body1" component="span">
                            Customer:
                          </Typography>
                          &nbsp;
                          <Typography color="textSecondary" variant="body1" component="span" noWrap>
                            {order.customer?.name}
                          </Typography>
                        </Grid>
                        <Grid item zeroMinWidth>
                          <Typography color="primary" variant="body1" component="span">
                            Total price:
                          </Typography>
                          &nbsp;
                          <Typography color="secondary" variant="body1" component="span" noWrap>
                            {NumberUtils.format(order.totalPrice, '£')}
                          </Typography>
                        </Grid>
                        <Grid item zeroMinWidth>
                          <Typography color="primary" variant="body1" component="span">
                            Payment method:
                          </Typography>
                          &nbsp;
                          <Typography color="textSecondary" variant="body1" component="span" noWrap>
                            {order.paymentMethod}
                          </Typography>
                        </Grid>
                      </Grid>
                    </>
                  }
                </Box>
              </Grid>

              <Grid item xs>
                <Typography color="primary" variant="h6">
                  Collection information
                </Typography>
              </Grid>

              <Grid item xs className="textCenter">
                <Box mt={2} mb={4}>
                  {loadingOrder && <CircularProgress size={24} color="secondary" />}
                  {!loadingOrder &&
                    <CollectionInformation formik={formik} />
                  }
                </Box>
              </Grid>

              <Grid item xs>
                <Grid container direction="row"
                  justifycontent="flex-start"
                  alignItems="center"
                  spacing={2}
                >
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="h6">
                      Shipping information
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span" style={{ marginLeft: '32px' }}>
                      Actual price:
                    </Typography>
                    &nbsp;
                    <Typography color="secondary" variant="body1" component="span" noWrap>
                      {NumberUtils.format(11.50, '£')}
                    </Typography>
                  </Grid>
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="body1" component="span">
                      Customer credit:
                    </Typography>
                    &nbsp;
                    <Typography color="secondary" variant="body1" component="span" noWrap>
                      {NumberUtils.format(5.50, '£')}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs>
                <Box mt={2} mb={4}>
                  {loadingOrder && <CircularProgress size={24} color="secondary" />}
                  {!loadingOrder &&
                    <ShippingInformation order={order} loading={loadingOrder} />
                  }
                </Box>
              </Grid>

              <Grid item xs>
                <Grid container direction="row"
                  justifycontent="flex-start"
                  alignItems="center"
                  spacing={2}
                >
                  <Grid item zeroMinWidth>
                    <Typography color="primary" variant="h6">
                      Tracking history
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs>
                <Box mt={2} mb={4}>
                  <History order={order} loading={loadingOrder} />
                </Box>
              </Grid>

              <Grid item xs>
                <Box mt={2} mb={4}>
                  <Button
                    className={btnClasses.danger}
                    aria-label="delete order"
                    tooltip="Delete order"
                    variant="outlined"
                    onClick={handleConfirmRemove}
                    loading={loadingRemove}
                  >
                    Delete order
                  </Button>
                </Box>
              </Grid>

              <Tooltip title="Save">
                <Fab color="secondary" aria-label="save"
                  className={classes.fab}
                  type="submit">
                  <SaveOutlined />
                </Fab>
              </Tooltip>

            </Grid>
          </Form>
        )}
      </Formik>

      <ConfirmModal
        open={openConfirmRemove}
        onCancel={handleCancelRemove}
        onConfirm={onRemove}
        title="Confirm remove"
      >
        <Typography>
          You really want to remove the collection order?
        </Typography>
      </ConfirmModal>

    </Container>
  );
}
import { Avatar, Grid, InputAdornment, Link, List, ListItem, ListItemAvatar, ListItemText, useMediaQuery, useTheme } from "@material-ui/core";
import { RoomOutlined } from "@material-ui/icons";
import { useEffect, useRef, useState } from "react";
import { FormField } from "app/components/form/Form";
import OrderStatusService from "services/api/order-status/OrderStatusService";
import UserService from "services/api/user/UserService";

export default function CollectionInformation(props) {
  const theme = useTheme();
  const formik = props.formik;
  const _isMounted = useRef(true);

  const [drivers, setDrivers] = useState([]);
  const [orderStatuses, setOrderStatuses] = useState([]);

  useEffect(() => {
    const abortController = new AbortController();

    loadOrderStatus();
    loadDrivers();

    return () => {
      _isMounted.current = false;
      abortController.abort();
    }
  }, []);

  const loadOrderStatus = () => {
    OrderStatusService().list()
      .then(response => {
        if (_isMounted.current) {
          setOrderStatuses(response.data.content);
        }
      });
  }

  const loadDrivers = () => {
    UserService().drivers()
      .then(response => {
        if (_isMounted.current) {
          setDrivers(response.data.content);
        }
      });
  }

  return (
    <>
      <Grid container direction="row"
        justifycontent="flex-start"
        alignItems="center"
        spacing={2}
      >
        <Grid item xs={12} sm={6}>
          <FormField
            required
            name="collectionAddress.postCode"
            label="Post code"
            formik={formik}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormField
            required
            name="collectionAddress.address"
            label="Address"
            formik={formik}
            InputProps={{
              startAdornment:
                <InputAdornment position="start">
                  <RoomOutlined />
                </InputAdornment>
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormField
            required
            name="collectionAddress.city"
            label="Town"
            formik={formik}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormField
            required
            name="collectionAddress.country"
            label="Country"
            formik={formik}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormField
            type="autoComplete"
            options={drivers}
            getOptionLabel="name"
            loading={false}
            name="driver"
            label="Driver"
            formik={formik}
            noOptionsText={<Link href="#" color="primary" variant="body1">
              Add driver
            </Link>}
            renderOption={(option) => (
              <List style={{ padding: 0 }}>
                <ListItem style={{ padding: 0 }}>
                  <ListItemAvatar>
                    <Avatar>
                      {option.name.charAt(0)}
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={option.name} secondary={option.active ? 'Active' : 'Inactive'} />
                </ListItem>
              </List>
            )}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormField
            type="autoComplete"
            options={orderStatuses}
            getOptionLabel="description"
            loading={false}
            name="orderStatus"
            label="Status"
            formik={formik}
            noOptionsText={<Link href="#" color="primary" variant="body1">
              Add status
            </Link>}
          />
        </Grid>
      </Grid>
    </>
  );
};

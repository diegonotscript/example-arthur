import { Box, Grid, Typography, useMediaQuery, useTheme } from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import { CheckCircle } from "@material-ui/icons";
import { Table } from "app/components/table/Table";
import { useRef } from "react"

export default function History(props) {
  const tableRef = useRef();
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));
  const order = props.order;
  const loading = props.loading;

  const history = [
    { id: 1, date: '2021-05-14', hour: '00:00', sent: true },
  ];

  const columns = [
    {
      field: 'date', headerName: 'Date', sortable: false, flex: 1
    },
    {
      field: 'hour', headerName: 'Hour', sortable: false, flex: 1
    },
    {
      field: 'address', headerName: 'Address', sortable: false, flex: 2
    },
    {
      field: 'event', headerName: 'Event', sortable: false, flex: 1, renderCell: (params) => {
        return params.row.sent && <div className="flex-horizontal">
          <CheckCircle style={{ color: green[500] }} />
          <Typography>Sent</Typography>
        </div>;
      }
    },
  ];

  return (
    <Grid item xs>
      <Box mt={2} mb={4}>
        <Table
          hasLoadedItems
          columns={columns}
          rows={history}
          loading={loading}
        />
      </Box>
    </Grid>
  );
};
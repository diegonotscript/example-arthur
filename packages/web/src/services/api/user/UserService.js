import Service from "services/Service";

export default function UserService() {
  const service = Service('api/users');

  const me = () => {
    return service
      .request('@me')
      .get();
  };

  const update = (params) => {
    return service
      .request()
      .put(params);
  };

  const drivers = () => {
    return service
      .request('drivers')
      .get();
  };

  return {
    me: me,
    update: update,
    drivers: drivers,
  };

};
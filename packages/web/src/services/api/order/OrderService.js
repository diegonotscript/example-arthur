import Service from "services/Service";

export default function OrderService() {
  const service = Service('api/orders');

  const list = (params) => {
    return service
      .request()
      .getBy(params);
  };

  const save = (params) => {
    if (params.id) {
      return service
        .request(params.id)
        .put(params);
    }

    return service
      .request()
      .post(params);
  };

  const getById = (id) => {
    return service
      .request(id)
      .get();
  };

  const remove = (id) => {
    return service
      .request(id)
      .del();
  };

  const getPackageById = (idOrder, idPackage) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          data: {
            id: idPackage,
            name: 'Package',
            price: 11,
            order: {
              id: idOrder,
              paymentMethod: 'VISA - 4566',
              date: '2021-07-30',
              collectionAddress: {
                id: 3,
                address: "123123",
                postCode: "1231231",
                city: "Criciúma",
                country: "Brasil",
                isDefault: true
              }
            },
            history: [
              { id: 1, date: '', hour: '' }
            ]
          }
        });
      }, 1000);
    });
    return service
      .request(idOrder, 'packages', idPackage)
      .get();
  };

  const getTotalByStatus = (params) => {
    return service
      .request('total-by-status')
      .get(params);
  };

  const getTotalCart = () => {
    return service
      .request('total-cart')
      .get();
  };

  return {
    list: list,
    save: save,
    remove: remove,
    getById: getById,
    getTotalByStatus: getTotalByStatus,
    getPackageById: getPackageById,
    getTotalCart: getTotalCart,
  };

};
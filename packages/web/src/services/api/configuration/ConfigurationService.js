import Service from "services/Service";

export default function ConfigurationService() {
  const service = Service('api/configuration');

  const getUnique = () => {
    return service
      .request()
      .get();
  };

  const save = (params) => {
    if (params.id) {
      return service
        .request(params.id)
        .put(params);
    }

    return service
      .request()
      .post(params);
  };

  const remove = (id) => {
    return service
      .request(id)
      .del();
  };

  return {
    getUnique: getUnique,
    save: save,
    remove: remove,
  };

};
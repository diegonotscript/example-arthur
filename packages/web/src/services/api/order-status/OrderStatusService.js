import Service from "services/Service";

export default function OrderStatusService() {
  const service = Service('api/orders-status');

  const list = (params) => {
    return service
      .request()
      .get();
  };

  const save = (params) => {
    if (params.id) {
      return service
        .request(params.id)
        .put(params);
    }

    return service
      .request()
      .post(params);
  };

  const remove = (id) => {
    return service
      .request(id)
      .del();
  };

  const getById = (id) => {
    return service
      .request(id)
      .get();
  };

  return {
    list: list,
    save: save,
    remove: remove,
    getById: getById,
  };

};
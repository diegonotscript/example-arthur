import Service from "services/Service";

export default function AddressService() {
  const service = Service('api/address');

  const list = (params) => {
    return service
      .request()
      .get(params);
  };

  const save = (params) => {
    if (params.id) {
      return service
        .request(params.id)
        .put(params);
    }

    return service
      .request()
      .post(params);
  };

  const remove = (id) => {
    return service
      .request(id)
      .del();
  };

  const getById = (id) => {
    return service
      .request(id)
      .get();
  };

  const getDefault = () => {
    return service
      .request('default')
      .get();
  };

  const setDefault = (params) => {
    return service
      .request('default')
      .put(params);
  };

  return {
    list: list,
    save: save,
    remove: remove,
    getById: getById,
    getDefault: getDefault,
    setDefault: setDefault,
  };

};
import Service from "services/Service";

export default function PackageService() {
  const service = Service('api/packages');

  const list = (params) => {
    return service
      .request()
      .get();
  };

  const save = (params) => {
    if (params.id) {
      return service
        .request(params.id)
        .put(params);
    }

    return service
      .request()
      .post(params);
  };

  const remove = (id) => {
    return service
      .request(id)
      .del();
  };

  const getById = (id) => {
    return service
      .request(id)
      .get();
  };

  const track = (idPackage, idTrack) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          data: {
            id: idTrack,
            package: {
              id: idPackage,
              price: 11,
            }
          }
        });
      }, 1000);
    });
    return service
      .request(idPackage, 'track', idTrack)
      .get();
  };

  return {
    list: list,
    save: save,
    remove: remove,
    getById: getById,
    track: track,
  };

};
import Service from "services/Service";

export default function AuthService() {
  const service = Service('auth');

  const login = (params) => {
    return service
      .request('login')
      .post(params);
  };

  const signup = (params) => {
    return service
      .request('signup')
      .post(params);
  };

  const existsEmail = (params) => {
    return service
      .request('exists-email')
      .post(params);
  };

  return {
    login: login,
    signup: signup,
    existsEmail: existsEmail,
  };

};
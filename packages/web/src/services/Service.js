import axios from 'axios';
import Storage from 'config/Storage';
import AuthService from 'services/auth/AuthService';

const HOST_API = process.env.REACT_APP_HOST_API;
const INSTANCE = axios.create({
  baseURL: HOST_API,
  headers: { 'Content-Type': 'application/json;charset=utf-8', 'Access-Control-Allow-Origin': '*' }
});

// Set the AUTH token for any request
INSTANCE.interceptors.request.use(async config => {
  const token = Storage.getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

INSTANCE.interceptors.response.use((response) => {
  // Return a successful response back to the calling service
  return response;
}, (error) => {
  console.log('INSTANCE.interceptors', error.response.data);

  const data = error.response.data;

  if (!data) {
    return new Promise((resolve, reject) => {
      reject();
    });
  }

  if (data.code === 'invalid_token' && data.description === 'jwt expired') {
    Storage.removeToken();
    window.location.reload();

    /* const user = Storage.getUser();

    AuthService()
      .login(user)
      .then((response) => {
        const token = response.data.token;
        Storage.setToken(token);
        Storage.setUser(user);
      })
      .then(async () => {
        const token = Storage.getToken();
        const config = error.config;
        config.headers['Authorization'] = `Bearer ${token}`;
        
                return new Promise((resolve, reject) => {
                  axios.request(config).then(response => {
                    resolve(response);
                  }).catch((error) => {
                    reject(error);
                  })
                });
      }); */
  }

  return new Promise((resolve, reject) => {

    if (error.response.data.error) {
      reject(error.response.data.error);
    }

    if (error.response.data) {
      reject(error.response.data);
    }

    if (error.message) {
      reject(error.message);
    }

    if (error.response.status !== 401) {
      reject(error);
    }

  });

});

class Request {

  constructor(apiPath) {
    this.apiPath = apiPath;
  }

  catchError(err) {
    if (!err.response) {
      throw {
        message: err,
      };
    }

    console.log('catchError', err.message);
    let error = err.message;
    if (err.response && err.message) {
      if (err.message) {
        error = err.message;
      } else if (err.message.error) {
        error = err.message.error;
      }
    }

    if (error) {
      throw {
        status: err.response.status,
        statusText: err.response.statusText,
        message: error,
      };
    }
  }

  get() {
    return INSTANCE.get(this.apiPath).catch(this.catchError);
  }

  getBy(params) {
    return INSTANCE.get(this.apiPath, { params: params }).catch(this.catchError);
  }

  post(params) {
    return INSTANCE.post(this.apiPath, params).catch(this.catchError);
  }

  put(params) {
    return INSTANCE.put(this.apiPath, params).catch(this.catchError);
  }

  del() {
    return INSTANCE.delete(this.apiPath).catch(this.catchError);
  }

  opt() {
    return INSTANCE.options(this.apiPath).catch(this.catchError);
  }

}

export default function Service(api) {

  const createUrl = (requestPaths) => {
    return (api ? api + "/" : "") + requestPaths.join("/");
  }

  const request = (...args) => {
    return new Request(createUrl(args));
  }

  return {
    request: request,
  };

}
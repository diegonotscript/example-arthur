class Storage {

  /* MÉTODOS PADRÕES */
  static _getItem(chave) {
    let item = localStorage.getItem(chave);
    if (typeof item === 'boolean') {
      return item;
    }
    return JSON.parse(localStorage.getItem(chave));
  };
  static _setItem(chave, item) {
    localStorage.setItem(chave, JSON.stringify(item));
  };
  static _removeItem(chave) {
    localStorage.removeItem(chave);
  };

  /* INCLUIR MÉTODOS DE GET E/OU SET ABAIXO */
  static getToken() {
    return this._getItem('token');
  };
  static setToken(item) {
    this._setItem('token', item);
  };
  static removeToken() {
    this._removeItem('token');
  };
  static getUser() {
    return this._getItem('user');
  };
  static setUser(item) {
    this._setItem('user', item);
  };
  static removeUser() {
    this._removeItem('user');
  };

  static getHasBeenWelcomed() {
    return this._getItem('welcome');
  };
  static setHasBeenWelcomed() {
    this._setItem('welcome', true);
  };
}

export default Storage;
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Login from 'app/pages/login/Login';
import { AuthConsumer } from './AuthProvider';

import Order from 'app/pages/customer/Order';
import OrderForm from 'app/pages/customer/OrderForm';
import OrderLabel from 'app/pages/customer/OrderLabel';
import OrderPackage from 'app/pages/customer/OrderPackage';
import TrackingPackage from 'app/pages/customer/TrackingPackage';
import Cart from 'app/pages/customer/Cart';

import OrderStatus from 'app/pages/admin/OrderStatus';
import OrderAdmin from 'app/pages/admin/Order';
import OrderFormAdmin from 'app/pages/admin/OrderForm';
import PackageAdmin from 'app/pages/admin/Package';
import Settings from 'app/pages/customer/Settings';

const ADMIN_ROUTES = [
  { path: '/orders', component: OrderAdmin },
  { path: '/packages', component: PackageAdmin },
  { path: '/order-status', component: OrderStatus },
  { path: '/orders/:idOrder', component: OrderFormAdmin },
  { path: '/orders/label/:idOrder', component: OrderLabel },
];

const CUSTOMER_ROUTES = [
  { path: '/orders', component: Order },
  { path: '/orders/new', component: OrderForm },
  { path: '/orders/:idOrder', component: OrderForm },
  { path: '/orders/label/:idOrder', component: OrderLabel },
  { path: '/cart', component: Cart },
  { path: '/settings', component: Settings },
];

const PUBLIC_ROUTES = ['/', '/login', '/sign-up', '/recover-password', '/logout'];

function Routes(props) {

  const isAuthenticated = props.isAuthenticated;
  const isAdmin = props.isAdmin;

  const CustomerAuthRoutes = ({ component: Component, ...rest }) => {
    return (
      <Route {...rest} render={componentProps => (
        (isAuthenticated) ?
          <Component {...componentProps} />
          : <Redirect to="/logout" />
      )} />
    );
  };

  const AdminAuthRoutes = ({ component: Component, ...rest }) => {
    return (
      <Route {...rest} render={componentProps => (
        (isAuthenticated) ?
          <Component {...componentProps} />
          : <Redirect to="/logout" />
      )} />
    );
  };

  const PublicRoutes = ({ component: Component, ...rest }) => {
    return (
      <Route {...rest} render={componentProps => (
        (!isAuthenticated) ?
          <Component {...componentProps} />
          : <Redirect to="/orders" />
      )} />
    );
  };

  return (
    <Switch>

      {PUBLIC_ROUTES.map(path => (
        <PublicRoutes
          isAuthenticated={isAuthenticated}
          key={path}
          exact
          path={path}
          component={Login}
        />
      ))}

      {isAdmin && ADMIN_ROUTES.map((route, index) => (
        <AdminAuthRoutes
          isAuthenticated={isAuthenticated}
          key={index}
          path={route.path}
          exact
          component={route.component} />
      ))}

      {!isAdmin && CUSTOMER_ROUTES.map((route, index) => (
        <CustomerAuthRoutes
          isAuthenticated={isAuthenticated}
          key={index}
          path={route.path}
          exact
          component={route.component} />
      ))}
    </Switch>
  )
}

const RoutesConsumer = () => (
  <AuthConsumer>
    {
      (context) => (
        <Routes
          isAuthenticated={context.isAuthenticated}
          isAdmin={context.authenticatedUser.isAdmin} />
      )
    }
  </AuthConsumer>
);

export default RoutesConsumer;
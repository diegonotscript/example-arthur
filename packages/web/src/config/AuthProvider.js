import React, { useState } from 'react';
import Storage from './Storage';
import AuthService from 'services/auth/AuthService';
import UserService from 'services/api/user/UserService';

export const AuthContext = React.createContext();
export const AuthConsumer = AuthContext.Consumer;
const Provider = AuthContext.Provider;

const AuthProvider = (props) => {
  const [isAuthenticated, setIsAuthenticated] = useState(!!Storage.getToken());
  const [authenticatedUser, setAuthenticatedUser] = useState(Storage.getUser() || { isAdmin: false });

  const startSession = (params) => {
    const user = { email: params.email, password: params.password };
    return AuthService()
      .login(user)
      .then((response) => {
        const token = response.data.token;
        setToken(token);
        return UserService().me()
          .then(response => {
            setUser(response.data);
          });
      });
  };

  const setUser = (user) => {
    Storage.setUser(user);
    setAuthenticatedUser(user);
  };

  const removeUser = (user) => {
    Storage.removeUser(user);
    setAuthenticatedUser({ isAdmin: false });
  };

  const setToken = (token) => {
    Storage.setToken(token);
    setIsAuthenticated(true);
  };

  const removeToken = () => {
    Storage.removeToken();
    setIsAuthenticated(false);
  };

  const finishSession = () => {
    return new Promise((resolve, reject) => {
      removeUser();
      removeToken();
      resolve();
    });
  };

  const getHasBeenWelcomed = () => {
    Storage.getHasBeenWelcomed();
  };

  const setHasBeenWelcomed = () => {
    Storage.setHasBeenWelcomed();
  };

  // divide com os filhos
  const context = {
    isAuthenticated: isAuthenticated,
    startSession: startSession,
    finishSession: finishSession,
    getHasBeenWelcomed: getHasBeenWelcomed,
    setHasBeenWelcomed: setHasBeenWelcomed,
    setUser: setUser,
    authenticatedUser: authenticatedUser//{ isAdmin: false }
  };

  return (
    <Provider value={context}>
      {props.children}
    </Provider>
  );
};

export default AuthProvider;
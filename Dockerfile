FROM node as base
CMD ["set","NODE_OPTIONS=","--max_old_space_size=","4096"]
RUN npm install -g @foal/cli
RUN npm install -g lerna
RUN npm install pm2 -g 
WORKDIR /usr/app/
VOLUME /usr/app/
COPY lerna.json package.json .env ./
COPY packages/backend/ ./packages/backend/
COPY packages/web/  ./packages/web/
COPY nginx/ ./nginx/

RUN yarn install --force
RUN lerna bootstrap
RUN yarn build
RUN pm2 --name @xpeedt/backend start npm -- start
EXPOSE 3001
# EXPOSE 3001
# FROM nginx:alpine
# COPY  --from=base /usr/app/packages/web/build /usr/share/nginx/html
# COPY --from=base /usr/app/nginx/default.conf /etc/nginx/conf.d/default.conf

# EXPOSE 80
# CMD ["nginx","-g","daemon off;"]







